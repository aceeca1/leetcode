class Solution {
    vector<pair<int, int>> getSkyline(vector<int>* a, int z) {
        vector<pair<int, int>> ans;
        if (z == 1) {
            ans.emplace_back(a[0][0], a[0][2]);
            ans.emplace_back(a[0][1], 0);
            return move(ans);
        }
        int z0 = z >> 1;
        auto ans1 = getSkyline(a, z0), ans2 = getSkyline(a + z0, z - z0);
        int i = 0, j = 0, h1 = 0, h2 = 0, h = 0, p = 0;
        while (i < ans1.size() || j < ans2.size()) {
            if (i < ans1.size() && (
                j >= ans2.size() || ans1[i].first < ans2[j].first)) {
                h = h1 = ans1[i].second;
                if (h2 > h) h = h2;
                if (h != p)
                    if (!ans.empty() && ans.back().first == ans1[i].first)
                        ans.back().second = p = h;
                    else ans.emplace_back(ans1[i].first, p = h);
                ++i;
            } else {
                h = h2 = ans2[j].second;
                if (h1 > h) h = h1;
                if (h != p)
                    if (!ans.empty() && ans.back().first == ans2[j].first)
                        ans.back().second = p = h;
                    else ans.emplace_back(ans2[j].first, p = h);
                ++j;
            }
        }
        return move(ans);
    }

public:
    vector<pair<int, int>> getSkyline(vector<vector<int>>& buildings) {
        if (buildings.empty()) return {};
        return getSkyline(&buildings[0], buildings.size());
    }
};
