class Solution {
public:
    string shortestPalindrome(string s) {
        vector<int> n(s.size() + 1);
        n[0] = -1;
        for (int i = 1; i <= s.size(); ++i) {
            int j = n[i - 1];
            while (j >= 0 && s[j] != s[i - 1]) j = n[j];
            n[i] = j + 1;
        }
        int k = 0;
        for (int i = s.size() - 1; i >= 0; --i) {
            while (k >= 0 && s[k] != s[i]) k = n[k];
            ++k;
        }
        auto v = s.substr(k, s.size() - k);
        reverse(v.begin(), v.end());
        return v += s;
    }
};
