class Solution {
public:
    int maxDepth(TreeNode* root) {
        if (!root) return 0;
        int ans0 = maxDepth(root->left);
        int ans1 = maxDepth(root->right);
        if (ans1 > ans0) ans0 = ans1;
        return ans0 + 1;
    }
};
