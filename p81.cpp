#include <algorithm>
using namespace std;

class Solution {
public:
    bool search(vector<int>& nums, int target) {
        if (nums.empty()) return false;
        while (nums.size() > 1 && nums.back() == nums[0]) nums.pop_back();
        int k = upper_bound(nums.begin(), nums.end(), nums[0],
            [](int a1, int a2){return a1 > a2;}
        ) - nums.begin();
        int p;
        if (target >= nums[0]) p = lower_bound(
            nums.begin(), nums.begin() + k, target) - nums.begin();
        else p = lower_bound(
            nums.begin() + k, nums.end(), target) - nums.begin();
        return p < nums.size() && nums[p] == target;
    }
};
