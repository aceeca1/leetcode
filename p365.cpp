class Solution {
public:
    bool canMeasureWater(int x, int y, int z) {
        if (!x && !y) return !z;
        if (z > x + y) return false;
        while (y) {
            x %= y;
            swap(x, y);
        }
        return !(z % x);
    }
};
