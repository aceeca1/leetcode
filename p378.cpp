#include <vector>
using namespace std;

class Solution {
    vector<vector<int>> m;

    int rank(int u) {
        int ans = 0, k = 0;
        for (int i = m.size() - 1; i >= 0; --i) {
            while (k < m[0].size() && m[i][k] < u) ++k;
            ans += k;
        }
        return ans;
    }

public:
    int kthSmallest(vector<vector<int>>& matrix, int k) {
        m = move(matrix);
        int s = m[0][0], t = m.back().back() + 1;
        while (s < t) {
            int mid = s + ((t - s) >> 1);
            if (rank(mid) >= k) t = mid;
            else s = mid + 1;
        }
        return s - 1;
    }
};
