class Solution {
    int n;
    vector<int> a;

    void put(int m) {
        if (m > n) return;
        if (m) a.emplace_back(m);
        m *= 10;
        for (int i = 0; i < 10; ++i)
            if (m + i) put(m + i);
    }

public:
    vector<int> lexicalOrder(int n_) {
        n = n_;
        put(0);
        return move(a);
    }
};
