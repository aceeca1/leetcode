#include <vector>
using namespace std;

class Solution {
public:
    void rotate(vector<vector<int>>& matrix) {
        auto& a = matrix;
        int n = a.size();
        for (int i0 = 0; i0 < n >> 1; ++i0)
            for (int j0 = 0; j0 < (n + 1) >> 1; ++j0) {
                int k = a[i0][j0];
                int i1 = n - 1 - j0, j1 = i0;
                a[i0][j0] = a[i1][j1];
                int i2 = n - 1 - j1, j2 = i1;
                a[i1][j1] = a[i2][j2];
                int i3 = n - 1 - j2, j3 = i2;
                a[i2][j2] = a[i3][j3];
                a[i3][j3] = k;
            }
    }
};
