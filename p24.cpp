class Solution {
public:
    ListNode* swapPairs(ListNode* head) {
        ListNode h(0), *p = &h;
        h.next = head;
        while (p->next && p->next->next) {
            auto p1 = p->next, p2 = p1->next;
            p->next = p2;
            p1->next = p2->next;
            p = p2->next = p1;
        }
        return h.next;
    }
};
