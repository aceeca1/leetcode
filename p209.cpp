class Solution {
public:
    int minSubArrayLen(int s, vector<int>& nums) {
        int p = 0, q = 0, ans = 0x7fffffff;
        for (;;) {
            while (q < nums.size() && s > 0) s -= nums[q++];
            if (s > 0) break;
            int k = q - p;
            if (k < ans) ans = k;
            s += nums[p++];
        }
        if (ans == 0x7fffffff) return 0;
        return ans;
    }
};
