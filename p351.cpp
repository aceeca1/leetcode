#include <map>
using namespace std;

class SummaryRanges {
    map<int, int> m;

public:
    SummaryRanges() {}

    void addNum(int val) {
        auto p = m.lower_bound(val);
        int s = val, t = val;
        if (p != m.begin()) {
            auto q = p; --q;
            if (q->second >= val) return;
            if (q->second + 1 == val) { s = q->first; m.erase(q); }
        }
        if (p != m.end()) {
            if (p->first <= val) return;
            if (p->first - 1 == val) { t = p->second; m.erase(p); }
        }
        m.emplace(s, t);
    }

    vector<Interval> getIntervals() {
        vector<Interval> ans;
        for (auto &i: m) ans.emplace_back(i.first, i.second);
        return move(ans);
    }
};
