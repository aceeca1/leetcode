#include <vector>
#include <algorithm>
using namespace std;

class Solution {
    struct Node {
        vector<int> ch;
        int a1 = 0, a2 = 0, a1c;
    };
    vector<Node> v;
    int minDepth = 0x7fffffff;
    vector<int> ans;

    int rootify(int c, int p) {
        auto r = remove(v[c].ch.begin(), v[c].ch.end(), p);
        v[c].ch.resize(r - v[c].ch.begin());
        for (int i: v[c].ch) {
            int k = rootify(i, c) + 1;
            if (k > v[c].a1) {
                v[c].a2 = v[c].a1;
                v[c].a1 = k;
                v[c].a1c = i;
            } else if (k > v[c].a2) v[c].a2 = k;
        }
        return v[c].a1;
    }

    void findDepth(int c, int p) {
        if (p != -1) {
            int ap = (v[p].a1c == c ? v[p].a2 : v[p].a1) + 1;
            if (ap > v[c].a1) {
                v[c].a2 = v[c].a1;
                v[c].a1 = ap;
                v[c].a1c = p;
            } else if (ap > v[c].a2) v[c].a2 = ap;
        }
        if (v[c].a1 < minDepth) minDepth = v[c].a1;
        for (int i: v[c].ch) findDepth(i, c);
    }

    void findRoot(int c) {
        if (v[c].a1 == minDepth) ans.emplace_back(c);
        for (int i: v[c].ch) findRoot(i);
    }

public:
    vector<int> findMinHeightTrees(int n, vector<pair<int, int>>& edges) {
        v.resize(n);
        for (auto& e: edges) {
            v[e.first].ch.emplace_back(e.second);
            v[e.second].ch.emplace_back(e.first);
        }
        rootify(0, -1);
        findDepth(0, -1);
        findRoot(0);
        return move(ans);
    }
};
