class Solution {
public:
    int computeArea(int A, int B, int C, int D, int E, int F, int G, int H) {
        int a1 = (C - A) * (D - B);
        int a2 = (G - E) * (H - F);
        if (E > A) A = E;
        if (F > B) B = F;
        if (G < C) C = G;
        if (H < D) D = H;
        int a3 = 0;
        if (C > A && D > B) a3 = (C - A) * (D - B);
        return a1 + a2 - a3;

    }
};
