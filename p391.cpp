#include <climits>
#include <vector>
#include <set>
#include <algorithm>
#include <cstdio>
using namespace std;

struct Segment {
    int p1, p2;
    bool operator<(const Segment& that) const {
        return p1 < that.p1;
    }
};

struct Event {
    int c, x; // c: 0 => Leave, 1 => Enter
    Segment s;
    bool operator<(const Event& that) const {
        return x < that.x || x == that.x && c < that.c;
    }
};

class Solution {
    vector<vector<int>> rect;

    bool checkArea() {
        int xMin = INT_MAX, xMax = INT_MIN;
        int yMin = INT_MAX, yMax = INT_MIN;
        int area = 0;
        for (auto& r: rect) {
            if (r[0] < xMin) xMin = r[0];
            if (r[1] < yMin) yMin = r[1];
            if (r[2] > xMax) xMax = r[2];
            if (r[3] > yMax) yMax = r[3];
            area += (r[2] - r[0]) * (r[3] - r[1]);
        }
        return area == (xMax - xMin) * (yMax - yMin);
    }

    bool sweepLine() {
        vector<Event> e;
        for (auto& r: rect) {
            e.emplace_back(Event{1, r[0], {r[1], r[3]}});
            e.emplace_back(Event{0, r[2], {r[1], r[3]}});
        }
        sort(e.begin(), e.end());
        set<Segment> s;
        for (auto& ei: e) {
            switch (ei.c) {
                case 0: s.erase(ei.s); break;
                case 1:
                    auto v = s.emplace(ei.s);
                    if (!v.second) return false;
                    auto k = v.first;
                    if (k != s.begin()) {
                        auto k1 = k; --k1;
                        if (k1->p2 > k->p1) return false;
                    }
                    auto k2 = k; ++k2;
                    if (k2 != s.end() && k2->p1 < k->p2) return false;
            }
        }
        return true;
    }

public:
    bool isRectangleCover(vector<vector<int>>& rectangles) {
        rect = move(rectangles);
        return checkArea() && sweepLine();
    }
};
