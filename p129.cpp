class Solution {
    int n = 0;

public:
    int sumNumbers(TreeNode* root) {
        if (!root) return 0;
        n = n * 10 + root->val;
        if (!root->left && !root->right) {
            int ans = n;
            n /= 10;
            return ans;
        }
        int ans = sumNumbers(root->left) + sumNumbers(root->right);
        n /= 10;
        return ans;
    }
};
