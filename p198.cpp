class Solution {
public:
    int rob(vector<int>& nums) {
        int a0 = 0, a1 = 0, a2;
        for (auto i: nums) {
            a2 = a1;
            a1 = a0;
            int k = a2 + i;
            if (k > a0) a0 = k;
        }
        return a0;
    }
};
