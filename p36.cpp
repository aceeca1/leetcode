class Solution {
public:
    bool check(int& b, int bdi) {
        if (bdi == '.') return true;
        int c = 1 << (bdi - '0');
        if (b & c) return false;
        b += c;
        return true;
    }

    bool isValidSudoku(vector<vector<char>>& board) {
        for (int i = 0; i < 9; ++i) {
            int b1 = 0, b2 = 0;
            for (int j = 0; j < 9; ++j) {
                if (!check(b1, board[i][j])) return false;
                if (!check(b2, board[j][i])) return false;
            }
        }
        for (int i1 = 0; i1 < 3; ++i1)
            for (int j1 = 0; j1 < 3; ++j1) {
                int b = 0;
                for (int i2 = 0; i2 < 3; ++i2)
                    for (int j2 = 0; j2 < 3; ++j2)
                        if (!check(b, board[i1 * 3 + i2][j1 * 3 + j2]))
                            return false;
            }
        return true;
    }
};
