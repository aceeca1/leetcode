class Solution {
    TreeNode* flattenTail(TreeNode* p) {
        auto r = p->right, tail = p;
        if (p->left) {
            auto lTail = flattenTail(p->left);
            tail->right = p->left;
            tail = lTail;
            p->left = nullptr;
        }
        if (r) {
            auto rTail = flattenTail(r);
            tail->right = r;
            tail = rTail;
        }
        tail->right = nullptr;
        return tail;
    }

public:
    void flatten(TreeNode* root) {
        if (!root) return;
        flattenTail(root);
    }
};
