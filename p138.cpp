#include <unordered_map>
using namespace std;

class Solution {
public:
    RandomListNode *copyRandomList(RandomListNode *head) {
        if (!head) return nullptr;
        unordered_map<RandomListNode*, RandomListNode*> a;
        RandomListNode h(0), *p = &h;
        for (auto i = head; i; i = i->next) {
            p = p->next = new RandomListNode(i->label);
            a[i] = p;
        }
        for (auto i = head; i; i = i->next)
            a[i]->random = a[i->random];
        return h.next;
    }
};
