class Solution {
public:
    vector<string> summaryRanges(vector<int>& nums) {
        if (nums.empty()) return {};
        vector<string> ans;
        int s = nums[0], t = nums[0];
        for (int i = 1; i <= nums.size(); ++i)
            if (i < nums.size() && nums[i] == t + 1) ++t;
            else {
                ans.emplace_back(to_string(s));
                if (s < t) {
                    ans.back() += "->";
                    ans.back() += to_string(t);
                }
                s = t = nums[i];
            }
        return move(ans);
    }
};
