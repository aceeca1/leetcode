class Solution {
    int ans = 0x80000000;

    int single(TreeNode* p) {
        if (!p) return 0x80000000;
        int a1 = single(p->left);
        int a2 = single(p->right);
        int n = p->val;
        if (a1 > 0) n += a1;
        if (a2 > 0) n += a2;
        if (n > ans) ans = n;
        if (a1 < 0) a1 = 0;
        if (a2 > a1) a1 = a2;
        return a1 + p->val;
    }

public:
    int maxPathSum(TreeNode* root) {
        single(root);
        return ans;
    }
};
