class Solution {
    vector<int> a;

public:
    bool isCont(int k) {
        if (k >= a.size()) return false;
        return (a[k] & 0xc0) == 0x80;
    }

    bool validUtf8(vector<int>& data) {
        a = move(data);
        for (int i = 0; i < a.size();) {
            if (!(a[i] & 0x80)) ++i;
            else if (!(a[i] & 0x40)) return false;
            else if (!(a[i] & 0x20)) {
                if (!isCont(i + 1)) return false;
                i += 2;
            } else if (!(a[i] & 0x10)) {
                if (!isCont(i + 1)) return false;
                if (!isCont(i + 2)) return false;
                i += 3;
            } else if (!(a[i] & 0x8)) {
                if (!isCont(i + 1)) return false;
                if (!isCont(i + 2)) return false;
                if (!isCont(i + 3)) return false;
                i += 4;
            } else return false;
        }
        return true;
    }
};
