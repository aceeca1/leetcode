#include <cstdint>
using namespace std;

class Solution {
public:
    int minPatches(vector<int>& nums, int n) {
        int64_t max = 1;
        int ans = 0;
        for (int i: nums) {
            while (i > max) {
                if (max > n) return ans;
                max <<= 1;
                ++ans;
            }
            if (max > n) return ans;
            max += i;
        }
        while (max <= n) {
            max <<= 1;
            ++ans;
        }
        return ans;
    }
};
