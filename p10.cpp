#include <vector>
#include <string>
#include <unordered_set>
using namespace std;

class Solution {
public:
    struct Node;
    struct Edge {
        char c;
        Node *next;
    };
    struct Node {
        vector<Edge> e;
    };

    void eps(unordered_set<Node*>& u, Node* ui) {
        for (auto& uiei: ui->e) {
            if (uiei.c) continue;
            auto r = u.emplace(uiei.next);
            if (r.second) eps(u, uiei.next);
        }
    }

    void eps(unordered_set<Node*>& u) {
        for (auto ui: u) eps(u, ui);
    }

    bool isMatch(string s, string p) {
        const char *pp = p.c_str();
        Node *n = new Node, *last = n;
        for (int i = 0; i < p.size();) {
            auto n0 = new Node;
            if (pp[i + 1] == '*') {
                last->e.emplace_back(Edge{0, n0});
                n0->e.emplace_back(Edge{pp[i], n0});
                i += 2;
            } else {
                last->e.emplace_back(Edge{pp[i], n0});
                ++i;
            }
            last = n0;
        }
        unordered_set<Node*> u{n};
        eps(u);
        for (int i = 0; i < s.size(); ++i) {
            unordered_set<Node*> u0;
            for (auto ui: u)
                for (auto uiei: ui->e)
                    if (uiei.c == s[i] || uiei.c == '.')
                        u0.emplace(uiei.next);
            u = move(u0);
            eps(u);
        }
        for (;;) {
            if (n->e.empty()) break;
            auto n0 = n->e.back().next;
            if (n0 == n) break;
            delete n;
            n = n0;
        }
        delete n;
        return u.count(last);
    }
};
