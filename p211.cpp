#include <stack>
using namespace std;

class WordDictionary {
    struct Node {
        Node* ch[26]{};
        int n = 0;
    };
    Node *root = new Node;

public:
    void addWord(string word) {
        auto p = root;
        for (char i: word) {
            auto& p1 = p->ch[i - 'a'];
            if (!p1) p1 = new Node;
            p = p1;
        }
        ++p->n;
    }

    bool search(string word) {
        stack<pair<Node*, int>> s;
        s.emplace(root, 0);
        while (!s.empty()) {
            auto& sH = s.top();
            Node *p = sH.first;
            int k = sH.second;
            s.pop();
            if (k == word.size()) {
                if (p->n) return true;
            } else if (word[k] == '.') {
                for (int i = 0; i < 26; ++i)
                    if (p->ch[i]) s.emplace(p->ch[i], k + 1);
            } else {
                auto p1 = p->ch[word[k] - 'a'];
                if (p1) s.emplace(p1, k + 1);
            }
        }
        return false;
    }

    ~WordDictionary() {
        stack<Node*> s;
        s.emplace(root);
        while (!s.empty()) {
            auto sH = s.top();
            s.pop();
            for (int i = 0; i < 26; ++i)
                if (sH->ch[i]) s.emplace(sH->ch[i]);
            delete sH;
        }
    }
};
