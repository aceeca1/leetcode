class Solution {
public:
    vector<vector<int>> generate(int numRows) {
        vector<vector<int>> ans;
        vector<int> a{1};
        for (int i = 1; i <= numRows; ++i) {
            ans.emplace_back(a);
            a.emplace_back(1);
            for (int j = a.size() - 2; j; --j) a[j] += a[j - 1];
        }
        return move(ans);
    }
};
