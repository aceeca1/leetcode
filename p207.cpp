#include <stack>
using namespace std;

class Solution {
public:
    bool canFinish(int numCourses, vector<pair<int, int>>& prerequisites) {
        vector<vector<int>> e(numCourses);
        for (auto& i: prerequisites) e[i.first].emplace_back(i.second);
        vector<int> v(numCourses);
        stack<pair<int, bool>> s;
        for (int i = 0; i < v.size(); ++i) {
            if (v[i]) continue;
            s.emplace(i, true);
            while (!s.empty()) {
                auto& sH = s.top();
                int k = sH.first;
                bool out = sH.second;
                s.pop();
                if (!out) {
                    v[k] = 2;
                    continue;
                }
                v[k] = 1;
                s.emplace(k, false);
                for (auto j: e[k]) switch (v[j]) {
                    case 0: s.emplace(j, true); break;
                    case 1: return false;
                }
            }
        }
        return true;
    }
};
