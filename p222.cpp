class Solution {
public:
    int countNodes(TreeNode* root) {
        if (!root) return 0;
        int len1 = 0, len2 = 0;
        for (auto i = root; i; i = i->left) ++len1;
        for (auto i = root; i; i = i->right) ++len2;
        if (len1 == len2) return (1 << len1) - 1;
        return countNodes(root->left) + countNodes(root->right) + 1;
    }
};
