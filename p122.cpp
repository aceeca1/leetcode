class Solution {
public:
    int maxProfit(vector<int>& prices) {
        if (prices.empty()) return 0;
        int ans = 0;
        for (int i = 1; i < prices.size(); ++i) {
            int k = prices[i] - prices[i - 1];
            if (k > 0) ans += k;
        }
        return ans;
    }
};
