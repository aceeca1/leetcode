#include <string>
#include <vector>
using namespace std;

class Solution {
    string str, pr;
    vector<string> ans;
    int m = 0, k = 0;

    void put() {
        if (k == 4) {
            ans.emplace_back(pr);
            return;
        }
        ++k;
        for (int i = 1; i <= 3; ++i) {
            int le = str.size() - m - i;
            if (le < 4 - k || (4 - k) * 3 < le) continue;
            if (i >= 2 && str[m] == '0') continue;
            if (i == 3) {
                if (str[m] >= '3') continue;
                if (str[m] == '2') {
                    if (str[m + 1] >= '6') continue;
                    if (str[m + 1] == '5' && str[m + 2] >= '6') continue;
                }
            }
            if (!pr.empty()) pr += '.';
            pr.append(str, m, i);
            m += i;
            put();
            m -= i;
            pr.resize(pr.size() - i);
            if (!pr.empty()) pr.pop_back();
        }
        --k;
    }

public:
    vector<string> restoreIpAddresses(string s) {
        str = move(s);
        put();
        return move(ans);
    }
};
