// Below is the interface for Iterator, which is already defined for you.
// **DO NOT** modify the interface for Iterator.
class Iterator {
    struct Data;
    Data* data;
public:
    Iterator(const vector<int>& nums);
    Iterator(const Iterator& iter);
    virtual ~Iterator();
    // Returns the next element in the iteration.
    int next();
    // Returns true if the iteration has more elements.
    bool hasNext() const;
};

class PeekingIterator : public Iterator {
    int buf;
    bool bufValid;
public:
    PeekingIterator(const vector<int>& nums) : Iterator(nums) {
        if (bufValid = Iterator::hasNext()) buf = Iterator::next();
    }

    int peek() { return buf; }

    int next() {
        auto ans = buf;
        if (bufValid = Iterator::hasNext()) buf = Iterator::next();
        return ans;
    }

    bool hasNext() const { return bufValid; }
};
