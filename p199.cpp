class Solution {
    vector<int> ans;
    int level = 0;

    void preOrder(TreeNode* p) {
        if (level >= ans.size()) ans.emplace_back();
        ans[level++] = p->val;
        if (p->left) preOrder(p->left);
        if (p->right) preOrder(p->right);
        --level;
    }

public:
    vector<int> rightSideView(TreeNode* root) {
        if (!root) return {};
        preOrder(root);
        return move(ans);
    }
};
