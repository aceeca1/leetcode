#include <utility>
using namespace std;

class Solution {
public:
    TreeNode* invertTree(TreeNode* root) {
        auto root0 = root;
        while (root) {
            auto p = root->left;
            if (!p) {
                swap(root->left, root->right);
                root = root->left;
                continue;
            }
            while (p->left && p->left != root) p = p->left;
            if (p->left) {
                p->left = nullptr;
                swap(root->left, root->right);
                root = root->left;
                continue;
            }
            p = root->left;
            while (p->right) p = p->right;
            p->right = root;
            root = root->left;
        }
        return root0;
    }
};
