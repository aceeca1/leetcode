class Solution {
    void reverse(ListNode*& p) {
        ListNode *q = nullptr;
        while (p) {
            auto p1 = p->next;
            p->next = q;
            q = p;
            p = p1;
        }
        p = q;
    }

public:
    bool isPalindrome(ListNode* head) {
        if (!head || !head->next) return true;
        int n = 0;
        for (auto i = head; i; i = i->next) ++n;
        int n0 = n >> 1;
        auto p = head;
        while (--n0) p = p->next;
        auto q = p->next;
        if (n & 1) {
            auto q1 = q->next;
            delete q;
            q = q1;
        }
        p->next = nullptr;
        reverse(q);
        bool b = true;
        for (auto i = head, j = q; i; i = i->next, j = j->next)
            if (i->val != j->val) { b = false; break; }
        p->next = q;
        return b;
    }
};
