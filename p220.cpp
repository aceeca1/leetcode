#include <cstdint>
#include <set>
using namespace std;

class Solution {
public:
    bool containsNearbyAlmostDuplicate(vector<int>& nums, int k, int t) {
        if (t < 0) return false;
        set<int> u;
        for (int i = 0; i < nums.size(); ++i) {
            if (i > k) u.erase(nums[i - k - 1]);
            auto p = u.emplace(nums[i]);
            if (!p.second) return true;
            auto q = p.first; ++q;
            if (q != u.end() && int64_t(*q) - nums[i] <= t) return true;
            q = p.first;
            if (q != u.begin() && int64_t(nums[i]) - *--q <= t) return true;
        }
        return false;
    }
};
