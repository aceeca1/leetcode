#include <unordered_set>
#include <unordered_map>
#include <string>
#include <vector>
#include <queue>
using namespace std;

class Solution {
    unordered_map<int, int> sToI;
    vector<string> iToS;
    vector<vector<int>> e;
    vector<int> d;

    int hash(string s) {
        int ans = 0;
        for (int i = 0; i < s.size(); ++i) ans = ans * 0xdeadbeef + s[i];
        return ans;
    }

public:
    int ladderLength(
        string beginWord, string endWord, unordered_set<string>& wordList) {
        if (beginWord == endWord) return 1;
        iToS.emplace_back(move(beginWord));
        sToI[hash(iToS.back())] = 0;
        iToS.emplace_back(move(endWord));
        sToI[hash(iToS.back())] = 1;
        for (auto& s: wordList) {
            bool su = sToI.emplace(hash(s), iToS.size()).second;
            if (!su) continue;
            iToS.emplace_back(move(s));
        }
        e.resize(iToS.size());
        for (int i = 0; i < e.size(); ++i) {
            string &s = iToS[i];
            int h = hash(s), p = 1;
            for (int j = s.size() - 1; j >= 0; --j) {
                int h1 = h - s[j] * p;
                for (char k = 'a'; k <= 'z'; ++k) {
                    if (k == s[j]) continue;
                    int h2 = h1 + k * p;
                    auto sH = sToI.find(h2);
                    if (sH != sToI.end()) e[i].emplace_back(sH->second);
                }
                p *= 0xdeadbeef;
            }
        }
        d.resize(e.size(), 0x7fffffff);
        d[0] = 1;
        queue<int> q;
        q.emplace(0);
        while (!q.empty()) {
            int h = q.front();
            if (h == 1) return d[1];
            q.pop();
            for (int i: e[h]) if (d[i] == 0x7fffffff) {
                d[i] = d[h] + 1;
                q.push(i);
            }
        }
        return 0;
    }
};
