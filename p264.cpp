#include <queue>
#include <algorithm>
#include <cstdint>
using namespace std;

class Solution {
public:
    int nthUglyNumber(int n) {
        queue<int64_t> a2, a3, a5;
        a2.emplace(2);
        a3.emplace(3);
        a5.emplace(5);
        int64_t k = 1;
        while (--n) {
            k = min(a2.front(), min(a3.front(), a5.front()));
            if (k == a2.front()) a2.pop();
            else if (k == a3.front()) a3.pop();
            else if (k == a5.front()) a5.pop();
            a5.emplace(k * 5);
            if (k % 5) {
                a3.emplace(k * 3);
                if (k % 3) a2.emplace(k << 1);
            }
        }
        return k;
    }
};
