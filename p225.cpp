#include <queue>
using namespace std;

class Stack {
    queue<int> q;

public:
    void push(int x) {
        q.emplace(x);
        for (int i = q.size() - 1; i; --i) {
            int qH = q.front();
            q.pop();
            q.emplace(qH);
        }
    }

    void pop() { q.pop(); }

    int top() { return q.front(); }

    bool empty() { return q.empty(); }
};
