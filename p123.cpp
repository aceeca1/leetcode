class Solution {
public:
    int maxProfit(vector<int>& prices) {
        if (prices.empty()) return 0;
        vector<int> a(prices.size());
        for (int i = 1; i <= 2; ++i) {
            int p = -prices[0], a0 = 0, a1;
            for (int j = 1; j < prices.size(); ++j) {
                a1 = a0;
                a0 = a[j];
                a[j] = a[j - 1];
                int k = p + prices[j];
                if (k > a[j]) a[j] = k;
                k = a1 - prices[j];
                if (k > p) p = k;
            }
        }
        return a.back();
    }
};
