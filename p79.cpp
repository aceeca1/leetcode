class Solution {
    vector<vector<char>> b;
    string w;
    int x, y, k = 0;

    void put() {
        if (++k == w.size()) throw true;
        b[x][y] = '.';
        if (x && b[x - 1][y] == w[k]) --x, put(), ++x;
        if (y && b[x][y - 1] == w[k]) --y, put(), ++y;
        if (x + 1 < b.size() && b[x + 1][y] == w[k]) ++x, put(), --x;
        if (y + 1 < b[0].size() && b[x][y + 1] == w[k]) ++y, put(), --y;
        b[x][y] = w[--k];
    }

public:
    bool exist(vector<vector<char>>& board, string word) {
        b = move(board);
        w = move(word);
        try {
            for (x = 0; x < b.size(); ++x)
                for (y = 0; y < b[0].size(); ++y)
                    if (b[x][y] == w[0]) put();
        } catch (bool succ) {
            return true;
        }
        return false;
    }
};
