#include <string>
using namespace std;

class Solution {
public:
    int lengthOfLastWord(string s) {
        while (s.back() == ' ') s.pop_back();
        return s.size() - s.find_last_of(' ') - 1;
    }
};
