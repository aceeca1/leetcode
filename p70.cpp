#include <cmath>
using namespace std;

class Solution {
public:
    int climbStairs(int n) {
        double a = sqrt(5);
        double b = 0.5 * (a + 1.0);
        return round(pow(b, n + 1) / a);
    }
};
