class Solution {
public:
    ListNode* insertionSortList(ListNode* head) {
        ListNode h(0);
        while (head) {
            auto head1 = head->next, p = &h;
            while (p->next && p->next->val < head->val) p = p->next;
            head->next = p->next;
            p->next = head;
            head = head1;
        }
        return h.next;
    }
};
