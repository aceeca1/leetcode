#include <cmath>
using namespace std;

class Solution {
public:
    int getSum(int a, int b) {
        double c = pow(2147483648.0, (1 / 2147483648.0));
        return round(log(pow(c, a) * pow(c, b)) / log(c));
    }
};
