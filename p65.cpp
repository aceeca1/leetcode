#include <cctype>
#include <cstring>
using namespace std;

class Solution {
public:
    bool isNumber(string s) {
        auto p = s.c_str();
        double d;
        int n1, n2;
        if (sscanf(p, "%f%n %n", &d, &n1, &n2) < 1) return false;
        if (p[n2] || strchr(p, 'x')) return false;
        if (p[n1 - 1] == '.') return !strchr(p, 'e');
        return isdigit(p[n1 - 1]);
    }
};
