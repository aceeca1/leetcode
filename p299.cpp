#include <string>
using namespace std;

class Solution {
public:
    string getHint(string secret, string guess) {
        int n = secret.size(), a = 0, b = 0;
        int c1[10]{}, c2[10]{};
        for (int i = 0; i < n; ++i)
            if (secret[i] == guess[i]) ++a;
            else {
                ++c1[secret[i] - '0'];
                ++c2[guess[i] - '0'];
            }
        for (int i = 0; i < 10; ++i)
            b += c2[i] < c1[i] ? c2[i] : c1[i];
        return ((to_string(a) += 'A') += to_string(b)) += 'B';
    }
};
