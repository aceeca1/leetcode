class Solution {
public:
    int maxProduct(vector<string>& words) {
        vector<int> a(words.size());
        for (int i = 0; i < words.size(); ++i)
            for (char j: words[i]) a[i] |= 1 << (j - 'a');
        int ans = 0;
        for (int i = 0; i < words.size(); ++i)
            for (int j = i + 1; j < words.size(); ++j) {
                if (a[i] & a[j]) continue;
                int k = words[i].size() *  words[j].size();
                if (k > ans) ans = k;
            }
        return ans;
    }
};
