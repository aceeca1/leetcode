class Solution {
public:
    ListNode *getIntersectionNode(ListNode *headA, ListNode *headB) {
        int n1 = 0;
        for (auto i = headA; i; i = i->next) ++n1;
        int n2 = 0;
        for (auto i = headB; i; i = i->next) ++n2;
        while (n1 > n2) { headA = headA->next; --n1; }
        while (n1 < n2) { headB = headB->next; --n2; }
        while (headA != headB) {
            headA = headA->next;
            headB = headB->next;
        }
        return headA;
    }
};
