#include <algorithm>
using namespace std;

class Solution {
public:
    int search(vector<int>& nums, int target) {
        int k = upper_bound(nums.begin(), nums.end(), nums[0],
            [](int a1, int a2){return a1 > a2;}
        ) - nums.begin();
        int p;
        if (target >= nums[0])
            p = lower_bound(
                nums.begin(), nums.begin() + k, target
            ) - nums.begin();
        else
            p = lower_bound(
                nums.begin() + k, nums.end(), target
            ) - nums.begin();
        if (p < nums.size() && nums[p] == target) return p;
        return -1;
    }
};
