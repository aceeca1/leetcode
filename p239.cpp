#include <deque>
#include <vector>
using namespace std;

class Solution {
public:
    vector<int> maxSlidingWindow(vector<int>& nums, int k) {
        deque<pair<int, int>> q;
        vector<int> ans;
        for (int i = 0; i < nums.size(); ++i) {
            if (i >= k && q.front().first == i - k) q.pop_front();
            while (!q.empty() && q.back().second < nums[i]) q.pop_back();
            q.emplace_back(i, nums[i]);
            if (i >= k - 1) ans.emplace_back(q.front().second);
        }
        return move(ans);
    }
};
