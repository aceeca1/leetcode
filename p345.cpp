class Solution {

public:
    string reverseVowels(string s) {
        bool vowel[256]{};
        for (char i: "AEIOUaeiou") vowel[i] = true;
        int i = 0, j = s.size() - 1;
        for (;;) {
            while (i < j && !vowel[s[i]]) ++i;
            while (i < j && !vowel[s[j]]) --j;
            if (i >= j) break;
            swap(s[i++], s[j--]);
        }
        return move(s);
    }
};
