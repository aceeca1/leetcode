#include <vector>
using namespace std;

class Solution {
public:
    int thirdMax(vector<int>& nums) {
        int num1st = INT_MIN;
        int num2nd = INT_MIN;
        int num3rd = INT_MIN;
        bool v1st = false, v2nd = false, v3rd = false;
        for (auto i: nums)
            if (!v1st || i > num1st) {
                num3rd = num2nd;  v3rd = v2nd;
                num2nd = num1st;  v2nd = v1st;
                num1st = i;       v1st = true;
            } else if (i == num1st);
            else if (!v2nd || i > num2nd) {
                num3rd = num2nd;  v3rd = v2nd;
                num2nd = i;       v2nd = true;
            } else if (i == num2nd);
            else if (!v3rd || i > num3rd) {
                num3rd = i;       v3rd = true;
            }
        return v3rd ? num3rd : num1st;
    }
};
