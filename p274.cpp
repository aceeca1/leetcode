class Solution {
public:
    int hIndex(vector<int>& citations) {
        vector<int> h(citations.size() + 1);
        for (int i: citations) {
            if (i > citations.size()) i = citations.size();
            ++h[i];
        }
        int ans = 0;
        for (int i = citations.size(); ; --i) {
            ans += h[i];
            if (ans >= i) return i;
        }
    }
};
