#include <unordered_map>
#include <algorithm>
using namespace std;

class Solution {
public:
    vector<int> topKFrequent(vector<int>& nums, int k) {
        unordered_map<int, int> a;
        for (int i: nums) ++a[i];
        vector<pair<int, int>> b(a.begin(), a.end());
        nth_element(b.begin(), b.begin() + k, b.end(),
            [&](const pair<int, int>& p1, const pair<int, int>& p2) {
                return p1.second > p2.second;
            });
        vector<int> ans(k);
        for (int i = 0; i < k; ++i) ans[i] = b[i].first;
        return move(ans);
    }
};
