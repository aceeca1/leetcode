#include <algorithm>
using namespace std;

class Solution {
public:
    int findPeakElement(vector<int>& nums) {
        int s = 0, t = nums.size() - 1;
        while (s < t) {
            int m = (s + t) >> 1;
            if (nums[m] < nums[m + 1]) s = m + 1;
            else t = m;
        }
        return s;
    }
};
