#include <cmath>
using namespace std;

class NumArray {
    vector<int> a;
    int m;

public:
    NumArray(vector<int> &nums) {
        m = 1 << (ilogb(nums.size() + 2) + 1);
        a.resize(m + m);
        for (int i = 0; i < nums.size(); ++i) a[m + 1 + i] = nums[i];
        for (int i = m - 1; i; --i) a[i] = a[i << 1] + a[(i << 1) + 1];
    }

    void update(int i, int val) {
        i += m + 1;
        val -= a[i];
        for (; i; i >>= 1) a[i] += val;
    }

    int sumRange(int i, int j) {
        int ans = 0;
        for (i += m, j += m + 2; (i ^ j) != 1; i >>= 1, j >>= 1) {
            if (!(i & 1)) ans += a[i + 1];
            if (j & 1) ans += a[j - 1];
        }
        return ans;
    }
};
