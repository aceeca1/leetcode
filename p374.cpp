int guess(int num);

class Solution {
public:
    int guessNumber(int n) {
        int l = 1, r = n;
        while (l < r) {
            int m = l + ((r - l) >> 1);
            switch (guess(m)) {
                case 0: return m;
                case -1: r = m - 1; break;
                case 1: l = m + 1;
            }
        }
        return l;
    }
};
