#include <vector>
#include <utility>
using namespace std;

class Solution {
public:
    int maximumGap(vector<int>& nums) {
        if (nums.size() < 2) return 0;
        int min = nums[0], max = nums[0];
        for (int i: nums) {
            if (i < min) min = i;
            if (i > max) max = i;
        }
        if (min == max) return 0;
        int k = (max - min + nums.size() - 2) / (nums.size() - 1);
        vector<pair<int, int>> b(
            (max - min) / k + 1, {0x7fffffff, 0x80000000});
        for (int i: nums) {
            int p = (i - min) / k;
            int &minP = b[p].first, &maxP = b[p].second;
            if (i < minP) minP = i;
            if (i > maxP) maxP = i;
        }
        int ans = 0;
        max = 0x80000000;
        for (int i = 0; i < b.size(); ++i) {
            if (b[i].first != 0x7fffffff) {
                int k = b[i].first - max;
                if (k > ans) ans = k;
            }
            if (b[i].second > max) max = b[i].second;
        }
        return ans;
    }
};
