#include <algorithm>
#include <string>
using namespace std;

class Solution {
public:
    string toHex(unsigned num) {
        string ans;
        while (num) {
            int k = num & 0xf;
            char c = k < 10 ? '0' + k : 'a' + (k - 10);
            ans += c;
            num >>= 4;
        }
        if (ans.empty()) ans += '0';
        reverse(ans.begin(), ans.end());
        return ans;
    }
};
