#include <algorithm>
#include <vector>
using namespace std;

class Solution {
public:
    int trap(vector<int>& height) {
        if (height.empty()) return 0;
        int maxH = 0, sumH = 0;
        for (int i = 0; i < height.size(); ++i) {
            if (height[i] > maxH) maxH = height[i];
            sumH -= height[i];
        }
        int k = 0;
        while (height[k] != maxH) ++k;
        int p = 0;
        for (int i = 0; i < k; ++i) {
            height[i] = p = max(p, height[i]);
            sumH += height[i];
        }
        p = 0;
        for (int i = height.size() - 1; i >= k; --i) {
            height[i] = p = max(p, height[i]);
            sumH += height[i];
        }
        return sumH;
    }
};
