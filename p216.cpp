class Solution {
    vector<vector<int>> ans;
    int ku, nu, m = 1;
    vector<int> pr;

    void put() {
        if (ku == 1) {
            if (m <= nu && nu <= 9) {
                ans.emplace_back(pr);
                ans.back().emplace_back(nu);
            }
            return;
        }
        --ku;
        int m0 = m;
        for (; m <= 9; ++m) {
            pr.emplace_back(m);
            nu -= m++;
            put();
            nu += --m;
            pr.pop_back();
        }
        m = m0;
        ++ku;
    }

public:
    vector<vector<int>> combinationSum3(int k, int n) {
        ku = k;
        nu = n;
        put();
        return move(ans);
    }
};
