#include <cstdint>
using namespace std;

class Solution {
public:
    int maxArea(vector<int>& height) {
        int p1 = 0, p2 = height.size() - 1;
        int64_t ans = 0;
        while (p1 < p2) {
            if (height[p1] < height[p2]) {
                auto area = int64_t(height[p1]) * (p2 - p1);
                if (area > ans) ans = area;
                ++p1;
            } else {
                auto area = int64_t(height[p2]) * (p2 - p1);
                if (area > ans) ans = area;
                --p2;
            }
        }
        return ans;
    }
};
