class NumMatrix {
    vector<vector<int>> a;

    int get(int x, int y) {
        if (x < 0 || y < 0) return 0;
        return a[x][y];
    }

public:
    NumMatrix(vector<vector<int>> &matrix) {
        a = move(matrix);
        for (int i = 0; i < a.size(); ++i)
            for (int j = 0; j < a[0].size(); ++j) {
                int a1 = get(i - 1, j - 1);
                int a2 = get(i - 1, j);
                int a3 = get(i, j - 1);
                int a4 = get(i, j);
                a[i][j] = a2 + a3 - a1 + a4;
            }
    }

    int sumRegion(int row1, int col1, int row2, int col2) {
        int a1 = get(row1 - 1, col1 - 1);
        int a2 = get(row1 - 1, col2);
        int a3 = get(row2, col1 - 1);
        int a4 = get(row2, col2);
        return a4 - a2 - a3 + a1;
    }
};
