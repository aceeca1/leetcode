#include <cstdint>
using namespace std;

class Solution {
public:
    bool isPerfectSquare(int num) {
        int s = 0, t = num + 1;
        while (s < t) {
            int m = (s + t) >> 1;
            if (int64_t(m) * m >= num) t = m;
            else s = m + 1;
        }
        return int64_t(s) * s == num;
    }
};
