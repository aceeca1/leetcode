class Solution {
    TreeNode* buildTree(int* in, int* post, int z) {
        if (z == 0) return nullptr;
        int k = 0;
        while (in[k] != post[z - 1]) ++k;
        TreeNode *ans = new TreeNode(post[z - 1]);
        ans->left = buildTree(in, post, k);
        ans->right = buildTree(in + k + 1, post + k, z - k - 1);
        return ans;
    }

public:
    TreeNode* buildTree(vector<int>& inorder, vector<int>& postorder) {
        return buildTree(&inorder[0], &postorder[0], inorder.size());
    }
};
