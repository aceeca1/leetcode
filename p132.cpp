class Solution {
public:
    int minCut(string s) {
        if (s.empty()) return 0;
        vector<int> pa(s.size() + 1), b(s.size() + 1);
        b.back() = -1;
        for (int i = s.size() - 1; i >= 0; --i) {
            int a0, a1;
            for (int j = i; j <= s.size(); ++j) {
                a1 = a0;
                a0 = pa[j];
                pa[j] = i == j || i + 1 == j || a1 && s[i] == s[j - 1];
            }
            b[i] = 0x7fffffff;
            for (int j = i + 1; j <= s.size(); ++j)
                if (pa[j] && b[j] < b[i]) b[i] = b[j];
            ++b[i];
        }
        return b[0];
    }
};
