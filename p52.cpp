#include <vector>
#include <string>
using namespace std;

class Solution {
    vector<int> a;
    int v = 0, m, m1 = -1, m2 = -1, z, k = 0;

    void put() {
        if (k == z) {
            ++v;
            return;
        }
        int m0 = m & (m1 >> k) & (m2 >> (z - 1 - k));
        for (;;) {
            int i = m0 & -m0;
            if (!i) break;
            m0 ^= i;
            a[k] = i;
            m ^= i, m1 ^= i << k, m2 ^= i << (z - 1 - k);
            ++k, put(), --k;
            m ^= i, m1 ^= i << k, m2 ^= i << (z - 1 - k);
        }
    }

public:
    int totalNQueens(int n) {
        a.resize(z = n);
        m = (1 << n) - 1;
        put();
        return v;
    }
};
