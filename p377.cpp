class Solution {
public:
    int combinationSum4(vector<int>& nums, int target) {
        vector<int> a(target + 1);
        a[0] = 1;
        for (int i = 1; i <= target; ++i)
            for (int j: nums) if (j <= i) a[i] += a[i - j];
        return a[target];
    }
};
