#include <cstdio>
#include <utility>
#include <unordered_map>
using namespace std;

class LRUCache {
    struct Node;
    struct Node {
        Node *next;
        pair<int, int> v;
    };
    Node h{nullptr, {}}, *t = &h;
    unordered_map<int, Node*> m;
    int cap;

    void moveToEnd(Node*& q) {
        auto q1 = q->next;
        if (!q1->next) return;
        q->next = q1->next;
        if (q->next) m[q->next->v.first] = q;
        q = t;
        t = t->next = q1;
        t->next = nullptr;
    }

public:
    LRUCache(int capacity) {
        cap = capacity;
    }

    int get(int key) {
        auto p = m.find(key);
        if (p == m.end()) return -1;
        moveToEnd(p->second);
        return t->v.second;
    }

    void set(int key, int value) {
        auto p = m.emplace(key, t);
        if (p.second) {
            if (!cap--) {
                m.erase(h.next->v.first);
                auto h1 = h.next;
                h.next = h1->next;
                if (h.next) m[h.next->v.first] = &h;
                delete h1;
                ++cap;
            }
            t = t->next = new Node{nullptr, {key, value}};
        } else {
            moveToEnd(p.first->second);
            t->v.second = value;
        }
    }

    ~LRUCache() {
        for (t = h.next; t;) {
            auto t1 = t->next;
            delete t;
            t = t1;
        }
    }
};

int main() {
    LRUCache n(2);
    n.set(2, 1);
    n.set(1, 1);
    n.get(2);
    n.set(4, 1);
    n.get(1);
    n.get(2);
    return 0;
}
