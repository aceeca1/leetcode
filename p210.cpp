#include <queue>
using namespace std;

class Solution {
public:
    vector<int> findOrder(int numCourses, vector<pair<int, int>>& prerequisites) {
        vector<vector<int>> e(numCourses);
        vector<int> in(numCourses);
        for (auto& i: prerequisites) {
            e[i.second].emplace_back(i.first);
            ++in[i.first];
        }
        queue<int> q;
        for (int i = 0; i < numCourses; ++i)
            if (!in[i]) q.emplace(i);
        vector<int> ans;
        while (!q.empty()) {
            int qH = q.front();
            q.pop();
            ans.emplace_back(qH);
            for (int i: e[qH]) if (!--in[i]) q.emplace(i);
        }
        if (ans.size() < numCourses) ans.clear();
        return move(ans);
    }
};
