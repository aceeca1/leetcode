#include <vector>
#include <random>
using namespace std;

class Solution {
    vector<ListNode*> v;
    default_random_engine rand;
    uniform_int_distribution<int> uni;

public:
    Solution(ListNode* head) {
        while (head) {
            v.emplace_back(head);
            head = head->next;
        }
        uni = uniform_int_distribution<int>(0, v.size() - 1);
    }

    int getRandom() {
        return v[uni(rand)]->val;
    }
};
