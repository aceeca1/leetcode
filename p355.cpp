#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <deque>
#include <queue>
using namespace std;

class Twitter {
    unordered_map<int, int> uToI;
    vector<int> iToU;
    vector<unordered_set<int>> fo;
    vector<deque<pair<int, int>>> tw;
    int now = 0;

    int getUToI(int userId) {
        auto u = uToI.emplace(userId, iToU.size());
        if (u.second) {
            iToU.emplace_back(userId);
            fo.emplace_back();
            fo.back().emplace(fo.size() - 1);
            tw.emplace_back();
        }
        return u.first->second;
    }

public:
    Twitter() {}

    void postTweet(int userId, int tweetId) {
        int k = getUToI(userId);
        if (tw[k].size() == 10) tw[k].pop_front();
        tw[k].emplace_back(tweetId, now++);
    }

    struct FoIt {
        int u, p, h;
        bool operator<(const FoIt& that) const { return h < that.h; }
    };

    vector<int> getNewsFeed(int userId) {
        priority_queue<FoIt> pq;
        for (int i: fo[getUToI(userId)])
            if (!tw[i].empty())
                pq.emplace(FoIt{i, tw[i].size() - 1, tw[i].back().second});
        vector<int> ans;
        for (int i = 0; i < 10; ++i) {
            if (pq.empty()) break;
            auto pqH = pq.top();
            pq.pop();
            ans.emplace_back(tw[pqH.u][pqH.p--].first);
            if (pqH.p >= 0) {
                pqH.h = tw[pqH.u][pqH.p].second;
                pq.emplace(pqH);
            }
        }
        return move(ans);
    }

    void follow(int followerId, int followeeId) {
        int u1 = getUToI(followerId), u2 = getUToI(followeeId);
        fo[u1].emplace(u2);
    }

    void unfollow(int followerId, int followeeId) {
        if (followerId == followeeId) return;
        int u1 = getUToI(followerId), u2 = getUToI(followeeId);
        fo[u1].erase(u2);
    }
};
