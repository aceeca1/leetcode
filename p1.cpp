#include <unordered_map>
#include <vector>
using namespace std;

class Solution {
public:
    vector<int> twoSum(vector<int>& nums, int target) {
        unordered_map<int, int> a;
        for (int i = 0; i < nums.size(); ++i) {
            auto k = a.find(target - nums[i]);
            if (k != a.end()) return vector<int>{k->second, i};
            a[nums[i]] = i;
        }
        return vector<int>{};
    }
};
