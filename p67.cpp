class Solution {
public:
    string addBinary(string a, string b) {
        if (a.size() > b.size()) swap(a, b);
        int carry = 0;
        for (int i = 0; i < b.size(); ++i) {
            int i1 = a.size() - 1 - i, i2 = b.size() - 1 - i;
            if (i1 >= 0) carry += a[i1] - '0';
            carry += b[i2] - '0';
            b[i2] = '0' + (carry & 1);
            carry >>= 1;
        }
        if (carry) b.insert(b.begin(), '1');
        return move(b);
    }
};
