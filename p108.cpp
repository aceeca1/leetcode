class Solution {
    TreeNode *sortedArrayToBST(int* a, int z) {
        if (!z) return nullptr;
        int k = z >> 1;
        auto p = new TreeNode(a[k]);
        p->left = sortedArrayToBST(a, k);
        p->right = sortedArrayToBST(a + k + 1, z - k - 1);
        return p;
    }

public:
    TreeNode* sortedArrayToBST(vector<int>& nums) {
        return sortedArrayToBST(&nums[0], nums.size());
    }
};
