#include <algorithm>
#include <vector>
#include <string>
using namespace std;

class Solution {
    struct Node {
        Node *ch[26]{};
        int n = 0;
    };
    Node *root = new Node;

    vector<vector<char>> b;
    int x, y;
    vector<string> ans;
    string pr;

    bool remove(Node* p, int q) {
        if (q < pr.size() && !remove(p->ch[pr[q] - 'a'], q + 1)) return false;
        if (p->n) return false;
        for (int i = 0; i < 26; ++i) if (p->ch[i]) return false;
        p->n = -1;
        return true;
    }

    void put(Node *k) {
        k = k->ch[b[x][y] - 'a'];
        if (!k || k->n == -1) return;
        pr += b[x][y];
        if (k->n) {
            ans.emplace_back(pr);
            --k->n;
            remove(root, 0);
        }
        b[x][y] = '.';
        if (x && b[x - 1][y] != '.') --x, put(k), ++x;
        if (y && b[x][y - 1] != '.') --y, put(k), ++y;
        if (x + 1 < b.size() && b[x + 1][y] != '.') ++x, put(k), --x;
        if (y + 1 < b[0].size() && b[x][y + 1] != '.') ++y, put(k), --y;
        b[x][y] = pr.back();
        pr.pop_back();
    }

    void dispose(Node *p) {
        for (int i = 0; i < 26; ++i)
            if (p->ch[i]) dispose(p->ch[i]);
        delete p;
    }

public:
    ~Solution() { dispose(root); }

    vector<string> findWords(vector<vector<char>>& board, vector<string>& words) {
        b = move(board);
        for (auto& w: words) {
            auto p = root;
            for (char i: w) {
                auto &p1 = p->ch[i - 'a'];
                if (!p1) p1 = new Node;
                p = p1;
            }
            p->n = 1;
        }
        for (x = 0; x < b.size(); ++x)
            for (y = 0; y < b[0].size(); ++y)
                put(root);
        return move(ans);
    }
};
