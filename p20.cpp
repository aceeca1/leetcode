#include <stack>
using namespace std;

class Solution {
public:
    bool isValid(string s) {
        stack<char> st;
        for (char si: s)
            switch (si) {
                case '(': case '[': case '{':
                    st.emplace(si); break;
                case ')':
                    if (st.empty() || st.top() != '(') return false;
                    st.pop(); break;
                case ']':
                    if (st.empty() || st.top() != '[') return false;
                    st.pop(); break;
                case '}':
                    if (st.empty() || st.top() != '{') return false;
                    st.pop();
            }
        return st.empty();
    }
};
