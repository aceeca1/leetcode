class TrieNode {
public:
    TrieNode *ch[26]{};
    int n = 0;
    TrieNode() {}
};

class Trie {
public:
    Trie() { root = new TrieNode(); }

    void insert(string word) {
        auto p = root;
        for (char i: word) {
            auto& p1 = p->ch[i - 'a'];
            if (!p1) p1 = new TrieNode;
            p = p1;
        }
        ++p->n;
    }

    bool search(string word) {
        auto p = root;
        for (char i: word) {
            auto& p1 = p->ch[i - 'a'];
            if (!p1) return false;
            p = p1;
        }
        return p->n;
    }

    bool startsWith(string prefix) {
        auto p = root;
        for (char i: prefix) {
            auto& p1 = p->ch[i - 'a'];
            if (!p1) return false;
            p = p1;
        }
        return true;
    }

    void dispose(TrieNode *p) {
        for (int i = 0; i < 26; ++i)
            if (p->ch[i]) dispose(p->ch[i]);
        delete p;
    }

    ~Trie() { dispose(root); }
private:
    TrieNode* root;
};
