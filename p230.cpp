class Solution {
    int ku;

    void inOrder(TreeNode* p) {
        if (!p) return;
        inOrder(p->left);
        if (!--ku) throw p->val;
        inOrder(p->right);
    }

public:
    int kthSmallest(TreeNode* root, int k) {
        ku = k;
        try { inOrder(root); }
        catch (int n) { return n; }
        return 0;
    }
};
