class Solution {
public:
    vector<string> fullJustify(vector<string>& words, int maxWidth) {
        int p = 0;
        vector<string> ans;
        while (p < words.size()) {
            int q = p + 1, w = words[p].size();
            while (q < words.size()) {
                int w0 = w + 1 + words[q].size();
                if (w0 > maxWidth) break;
                w = w0;
                ++q;
            }
            if (q == p + 1) {
                ans.emplace_back(move(words[p]));
                string &s = ans.back();
                if (maxWidth > w) s.append(maxWidth - w, ' ');
            } else {
                ans.emplace_back();
                string &s = ans.back();
                int u = maxWidth - w, v = q - 1 - p;
                if (q == words.size()) {
                    for (int i = 0; i <= v; ++i) {
                        s += words[p + i];
                        if (i < v) s += ' ';
                    }
                    s.append(u, ' ');
                } else {
                    int t = u / v;
                    u -= t * v;
                    for (int i = 0; i <= v; ++i) {
                        s += words[p + i];
                        if (q == words.size()) s += ' ';
                        else if (i < u) s.append(t + 2, ' ');
                        else if (i < v) s.append(t + 1, ' ');
                    }
                }
            }
            p = q;
        }
        return move(ans);
    }
};
