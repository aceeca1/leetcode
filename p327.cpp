#include <vector>
#include <algorithm>
#include <cmath>
#include <cstdint>
using namespace std;

class Solution {
    vector<int64_t> a, b;
    int m;

    int findLe(int64_t n) {
       int k = upper_bound(a.begin(), a.end(), n) - a.begin() + m, ans = 0;
       for (; k > 1; k >>= 1)
            if (k & 1) ans += b[k - 1];
       return ans;
    }

public:
    int countRangeSum(vector<int>& nums, int lower, int upper) {
        vector<int64_t> sums(nums.size() + 1);
        for (int i = 0; i < nums.size(); ++i)
            sums[i + 1] = sums[i] + nums[i];
        a.assign(sums.begin(), sums.end());
        sort(a.begin(), a.end());
        a.resize(unique(a.begin(), a.end()) - a.begin());
        for (int i = 0; i < sums.size(); ++i)
            sums[i] = lower_bound(a.begin(), a.end(), sums[i]) - a.begin();
        m = 1 << (ilogb(a.size() + 2) + 1);
        b.resize(m + m);
        int ans = 0;
        for (int i = sums.size() - 1; i; --i) {
            for (int k = sums[i] + m; k; k >>= 1) ++b[k];
            int u1 = findLe(a[sums[i - 1]] + lower - 1);
            int u2 = findLe(a[sums[i - 1]] + upper);
            ans += u2 - u1;
        }
        return ans;
    }
};
