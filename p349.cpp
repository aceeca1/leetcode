#include <unordered_set>
#include <vector>
using namespace std;

class Solution {
public:
    vector<int> intersection(vector<int>& nums1, vector<int>& nums2) {
        unordered_set<int> u1(nums1.begin(), nums1.end());
        unordered_set<int> u2(nums2.begin(), nums2.end());
        nums1.clear();
        for (int i: u1) if (u2.count(i)) nums1.emplace_back(i);
        return move(nums1);
    }
};
