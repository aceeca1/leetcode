#include <stack>
using namespace std;

class Solution {
public:
    vector<int> inorderTraversal(TreeNode* root) {
        if (!root) return vector<int>();
        stack<TreeNode*> s;
        TreeNode *c = root;
        vector<int> ans;
        for (;;) {
            for (; c; c = c->left) s.emplace(c);
            if (s.empty()) break;
            c = s.top();
            s.pop();
            ans.emplace_back(c->val);
            c = c->right;
        }
        return move(ans);
    }
};
