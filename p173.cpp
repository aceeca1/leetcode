class BSTIterator {
    TreeNode *rt;

public:
    BSTIterator(TreeNode *root) { rt = root; }

    bool hasNext() { return rt; }

    int next() {
        for (;;) {
            auto p = rt->left;
            if (!p) {
                int ans = rt->val;
                rt = rt->right;
                return ans;
            }
            while (p->right && p->right != rt) p = p->right;
            if (p->right) {
                int ans = rt->val;
                p->right = nullptr;
                rt = rt->right;
                return ans;
            }
            p->right = rt;
            rt = rt->left;
        }
    }
};
