#include <vector>
#include <string>
#include <cstring>
using namespace std;

class Solution {
public:
    int lengthLongestPath(string input) {
        input.c_str();
        char *p = strtok(&input[0], "\n");
        int maxLen = 0;
        vector<int> len;
        for (; p; p = strtok(nullptr, "\n")) {
            int tabs = 0;
            while (p[0] == '\t') { ++p; ++tabs; }
            if (tabs >= len.size()) len.emplace_back();
            if (tabs) len[tabs] = len[tabs - 1] + 1 + strlen(p);
            else len[tabs] = strlen(p);
            if (strchr(p, '.') && len[tabs] > maxLen) maxLen = len[tabs];
        }
        return maxLen;
    }
};
