#include <algorithm>
#include <vector>
using namespace std;

class Solution {
public:
    vector<vector<int>> combine(int n, int k) {
        vector<vector<int>> ans;
        vector<int> a(n);
        for (int i = 0; i < k; ++i) a[i] = 1;
        for (;;) {
            ans.emplace_back();
            vector<int>& b = ans.back();
            for (int i = 0; i < n; ++i) if (a[i]) b.emplace_back(i + 1);
            if (!prev_permutation(a.begin(), a.end())) break;
        }
        return move(ans);
    }
};
