class Solution {
public:
    ListNode* mergeTwoLists(ListNode* l1, ListNode* l2) {
        ListNode h(0), *p = &h;
        while (l1 || l2)
            if (l1 && (!l2 || l1->val < l2->val)) {
                p = p->next = l1;
                l1 = l1->next;
            } else {
                p = p->next = l2;
                l2 = l2->next;
            }
        return h.next;
    }
};
