class Solution {
public:
    int maxProfit(vector<int>& prices) {
        if (prices.empty()) return 0;
        int min = prices[0], ans = 0;
        for (int i = 1; i < prices.size(); ++i)
            if (prices[i] < min) min = prices[i];
            else {
                int v = prices[i] - min;
                if (v > ans) ans = v;
            }
        return ans;
    }
};
