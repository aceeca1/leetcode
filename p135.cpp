class Solution {
public:
    int candy(vector<int>& ratings) {
        vector<int> a(ratings.size(), 1);
        for (int i = 1; i < a.size(); ++i)
            if (ratings[i] > ratings[i - 1]) a[i] = a[i - 1] + 1;
        int ans = a.back();
        for (int i = a.size() - 2; i >= 0; --i) {
            if (ratings[i] > ratings[i + 1]) {
                int k = a[i + 1] + 1;
                if (k > a[i]) a[i] = k;
            }
            ans += a[i];
        }
        return ans;
    }
};
