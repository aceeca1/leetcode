#include <algorithm>
using namespace std;

class Solution {
    vector<vector<int>> ans;
    int level = 0;

    void preOrder(TreeNode* p) {
        if (level >= ans.size()) ans.emplace_back();
        ans[level++].emplace_back(p->val);
        if (p->left) preOrder(p->left);
        if (p->right) preOrder(p->right);
        --level;
    }

public:
    vector<vector<int>> zigzagLevelOrder(TreeNode* root) {
        if (!root) return vector<vector<int>>();
        preOrder(root);
        for (int i = 1; i < ans.size(); i += 2)
            reverse(ans[i].begin(), ans[i].end());
        return move(ans);
    }
};
