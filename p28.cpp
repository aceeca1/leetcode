#include <string>
#include <vector>
using namespace std;

class Solution {
public:
    int strStr(string haystack, string needle) {
        if (needle.empty()) return 0;
        vector<int> n(needle.size() + 1);
        n[0] = -1;
        for (int i = 1; i <= needle.size(); ++i) {
            int j = n[i - 1];
            while (j >= 0 && needle[j] != needle[i - 1]) j = n[j];
            n[i] = j + 1;
        }
        int k = 0;
        for (int i = 0; i < haystack.size(); ++i) {
            while (k >= 0 && needle[k] != haystack[i]) k = n[k];
            ++k;
            if (k == needle.size()) return i - needle.size() + 1;
        }
        return -1;
    }
};
