#include <vector>
#include <algorithm>
#include <random>
using namespace std;

class Solution {
    vector<int> a;
    default_random_engine rand;

public:
    Solution(vector<int> nums): a(nums) {}

    vector<int> reset() { return a; }

    vector<int> shuffle() {
        vector<int> ans(a);
        std::shuffle(ans.begin(), ans.end(), rand);
        return ans;
    }
};
