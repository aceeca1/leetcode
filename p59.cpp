class Solution {
public:
    vector<vector<int>> generateMatrix(int n) {
        vector<vector<int>> a(n, vector<int>(n));
        int c = 0;
        int pS = 0, pT = n - 1, qS = 0, qT = n - 1;
        for (;;) {
            for (int i = qS; i <= qT; ++i) a[pS][i] = ++c;
            if (++pS > pT) break;
            for (int i = pS; i <= pT; ++i) a[i][qT] = ++c;
            if (qS > --qT) break;
            for (int i = qT; i >= qS; --i) a[pT][i] = ++c;
            if (pS > --pT) break;
            for (int i = pT; i >= pS; --i) a[i][qS] = ++c;
            if (++qS > qT) break;
        }
        return move(a);
    }
};
