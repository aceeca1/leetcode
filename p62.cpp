#include <cstdint>
#include <algorithm>
using namespace std;

class Solution {
public:
    int uniquePaths(int m, int n) {
        --m, --n;
        if (m < n) swap(m, n);
        m += n;
        uint64_t ans = 1;
        for (int i = 1; i <= n; ++i) {
            ans *= m + 1 - i;
            ans /= i;
        }
        return ans;
    }
};
