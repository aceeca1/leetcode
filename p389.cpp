class Solution {
public:
    char findTheDifference(string s, string t) {
        char a[26]{};
        for (int i = 0; s[i]; ++i) --a[s[i] - 'a'];
        for (int i = 0; t[i]; ++i) ++a[t[i] - 'a'];
        for (int i = 0; i < 26; ++i) if (a[i]) return 'a' + i;
    }
};
