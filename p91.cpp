class Solution {
public:
    int numDecodings(string s) {
        if (s.empty()) return 0;
        int a, a1 = s[0] != '0', a2 = 1;
        for (int i = 1; i < s.size(); ++i) {
            if (s[i] != '0') a = a1; else a = 0;
            if (s[i - 1] == '1' ||
                s[i - 1] == '2' && '0' <= s[i] && s[i] <= '6') a += a2;
            a2 = a1;
            a1 = a;
        }
        return a1;
    }
};
