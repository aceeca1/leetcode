class Solution {
    vector<TreeNode*> lookup(TreeNode* root, TreeNode* p) {
        vector<TreeNode*> ans;
        for (;;) {
            ans.emplace_back(root);
            if (root == p) return ans;
            if (p->val < root->val) root = root->left;
            else root = root->right;
        }
    }

public:
    TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
        auto pA = lookup(root, p);
        auto qA = lookup(root, q);
        while (pA.size() > qA.size()) pA.pop_back();
        while (pA.size() < qA.size()) qA.pop_back();
        while (pA.back() != qA.back()) {
            pA.pop_back();
            qA.pop_back();
        }
        return pA.back();
    }
};
