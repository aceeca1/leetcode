class Solution {
public:
    string simplifyPath(string path) {
        path += '/';
        string ans;
        for (int i = 1; i < path.size();) {
            if (path[i] == '/') { ++i; continue; }
            if (path[i] == '.')
                if (path[i + 1] == '/') { i += 2; continue; }
                if (path[i + 1] == '.' && path[i + 2] == '/') {
                    if (ans.size()) {
                        while (ans.back() != '/') ans.pop_back();
                        ans.pop_back();
                    }
                    i += 3;
                    continue;
                }
            ans += '/';
            while (path[i] != '/') ans += path[i++];
            ++i;
        }
        if (ans.empty()) ans += '/';
        return ans;
    }
};
