#include <algorithm>
using namespace std;

class Solution {
    int robLine(int* nums, int z) {
        int a0 = 0, a1 = 0, a2;
        for (int i = 0; i < z; ++i) {
            a2 = a1;
            a1 = a0;
            int k = a2 + nums[i];
            if (k > a0) a0 = k;
        }
        return a0;
    }

public:
    int rob(vector<int>& nums) {
        if (nums.empty()) return 0;
        if (nums.size() == 1) return nums[0];
        int ans1 = robLine(&nums[0], nums.size() - 1);
        int ans2 = robLine(&nums[1], nums.size() - 1);
        return max(ans1, ans2);
    }
};
