class Solution {
public:
    string longestCommonPrefix(vector<string>& strs) {
        if (strs.empty()) return "";
        string ans;
        for (int i = 0; i < strs[0].size(); ++i) {
            char c = strs[0][i];
            if (any_of(strs.begin(), strs.end(), [&](const string& s){
                return s[i] != c;
            })) break;
            ans += c;
        }
        return ans;
    }
};
