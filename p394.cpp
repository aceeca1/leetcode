#include <cctype>
#include <string>
using namespace std;

class Solution {
    string s;
    int pos;

    string decode() {
        string ret;
        while (pos < s.size() && s[pos] != ']')
            if (isdigit(s[pos])) {
                int pos1 = s.find('[', pos + 1);
                int times = stoi(s.substr(pos, pos1 - pos));
                pos = pos1 + 1;
                string content = decode();
                for (int i = 0; i < times; ++i) ret += content;
                ++pos;
            } else ret += s[pos++];
        return ret;
    }

public:
    string decodeString(string str) {
        s = move(str);
        return decode();
    }
};
