#include <cstdint>
using namespace std;

class Solution {
public:
    vector<int> getRow(int rowIndex) {
        vector<int> ans(rowIndex + 1);
        ans[0] = 1;
        for (int i = 1; i <= rowIndex; ++i)
            ans[i] = (uint64_t)ans[i - 1] * (rowIndex + 1 - i) / i;
        return move(ans);
    }
};
