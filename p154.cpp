#include <algorithm>
using namespace std;

class Solution {
public:
    int findMin(vector<int>& nums) {
        while (nums.size() > 1 && nums.back() == nums[0]) nums.pop_back();
        auto p = upper_bound(nums.begin(), nums.end(), nums[0],
            [&](int a1, int a2) { return a1 > a2; });
        if (p == nums.end()) return nums[0];
        return *p;
    }
};
