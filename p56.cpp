#include <algorithm>
using namespace std;

class Solution {
public:
    vector<Interval> merge(vector<Interval>& intervals) {
        if (intervals.empty()) return move(intervals);
        sort(intervals.begin(), intervals.end(),
            [&](const Interval& i1, const Interval& i2) {
                return i1.start < i2.start;
            });
        int z = 0;
        for (int i = 1; i < intervals.size(); ++i)
            if (intervals[i].start > intervals[z].end)
                swap(intervals[++z], intervals[i]);
            else if (intervals[i].end > intervals[z].end)
                intervals[z].end = intervals[i].end;
        intervals.resize(z + 1);
        return move(intervals);
    }
};
