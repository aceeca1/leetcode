class Solution {
public:
    int removeDuplicates(vector<int>& nums) {
        if (nums.empty()) return 0;
        int c = nums[0], r = 1, z = 1;
        for (int i = 1; i < nums.size(); ++i)
            if (nums[i] != c) {
                c = nums[z++] = nums[i];
                r = 1;
            } else if (r < 2) {
                nums[z++] = nums[i];
                ++r;
            }
        return z;
    }
};
