class Solution {
    void reverse(ListNode* h) {
        auto i = h->next;
        h->next = nullptr;
        while (i) {
            auto i1 = i->next;
            i->next = h->next;
            h->next = i;
            i = i1;
        }
    }

    void interleave(ListNode* h1, ListNode* h2) {
        while (h2) {
            auto h1n = h1->next, h2n = h2->next;
            h1->next = h2;
            h2->next = h1n;
            h1 = h1n;
            h2 = h2n;
        }
    }

public:
    void reorderList(ListNode* head) {
        if (!head) return;
        int n = 0;
        for (auto i = head; i; i = i->next) ++n;
        n = (n - 1) >> 1;
        auto p = head;
        while (n--) p = p->next;
        reverse(p);
        auto q = p->next;
        p->next = nullptr;
        interleave(head, q);
    }
};
