class Solution {
public:
    bool searchMatrix(vector<vector<int>>& matrix, int target) {
        int x = matrix.size() - 1, y = 0;
        while (x >= 0 && y < matrix[0].size())
            if (matrix[x][y] < target) ++y;
            else if (matrix[x][y] > target) --x;
            else return true;
        return false;
    }
};
