class Solution {
public:
    int minPathSum(vector<vector<int>>& grid) {
        for (int i = 0; i < grid.size(); ++i)
            for (int j = 0; j < grid[0].size(); ++j) {
                if (!i && !j) continue;
                int k = 0x7fffffff;
                if (i && grid[i - 1][j] < k) k = grid[i - 1][j];
                if (j && grid[i][j - 1] < k) k = grid[i][j - 1];
                grid[i][j] += k;
            }
        return grid.back().back();
    }
};
