class Solution {
public:
    int longestConsecutive(vector<int>& nums) {
        unordered_set<int> a;
        for (int ni: nums) a.emplace(ni);
        int ans = 0;
        while (!a.empty()) {
            int p = *a.begin(), s = p, t = p;
            a.erase(a.begin());
            while (a.count(s - 1)) a.erase(--s);
            while (a.count(t + 1)) a.erase(++t);
            p = t - s + 1;
            if (p > ans) ans = p;
        }
        return ans;
    }
};
