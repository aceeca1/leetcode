#include <string>
using namespace std;

class Solution {
public:
    bool canConstruct(string ransomNote, string magazine) {
        int a[26]{};
        for (char i: magazine) ++a[i - 'a'];
        for (char i: ransomNote)
            if (--a[i - 'a'] < 0) return false;
        return true;
    }
};
