#include <algorithm>
using namespace std;

class Solution {
    int dfs(TreeNode* p) {
        if (!p) return 0;
        int a1 = dfs(p->left);
        int a2 = dfs(p->right);
        if (a1 > a2) swap(a1, a2);
        if (a2 > a1 + 1) throw false;
        return a2 + 1;
    }

public:
    bool isBalanced(TreeNode* root) {
        try { dfs(root); }
        catch (bool e) { return false; }
        return true;
    }
};
