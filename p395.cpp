#include <vector>
#include <string>
using namespace std;

class Solution {
    static vector<string> split(string s, char c) {
        vector<string> ret;
        ret.emplace_back();
        for (int i = 0; i < s.size(); ++i)
            if (s[i] == c) ret.emplace_back();
            else ret.back() += s[i];
        return ret;
    }

public:
    int longestSubstring(string s, int k) {
        int a[256]{};
        for (char i: s) ++a[i];
        for (int i = 0; i < 256; ++i)
            if (a[i] && a[i] < k) {
                int b = 0;
                for (auto& j: split(s, i)) {
                    int bu = longestSubstring(j, k);
                    if (bu > b) b = bu;
                }
                return b;
            }
        return s.size();
    }
};
