#include <algorithm>
#include <utility>
#include <vector>
using namespace std;

class Solution {
    vector<int> c, pf;
    vector<vector<int>> r;
    int k = 0, n;

    void put() {
        if (!n) r.emplace_back(pf);
        if (k >= c.size() || n < c[k]) return;
        ++k, put(), --k;
        while (n >= c[k]) {
            pf.emplace_back(c[k]);
            n -= c[k];
            ++k, put(), --k;
        }
        while (pf.size() && pf.back() == c[k]) {
            pf.pop_back();
            n += c[k];
        }
    }


public:
    vector<vector<int>> combinationSum(vector<int>& candidates, int target) {
        c = move(candidates);
        sort(c.begin(), c.end());
        c.resize(unique(c.begin(), c.end()) - c.begin());
        n = target;
        put();
        return move(r);
    }
};
