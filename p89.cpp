#include <vector>
using namespace std;

class Solution {
public:
    vector<int> grayCode(int n) {
        vector<int> ans{0};
        for (int i = 0; i < n; ++i) {
            for (int j = ans.size() - 1; j >= 0; --j)
                ans.emplace_back(ans[j] + (1 << i));
        }
        return move(ans);
    }
};
