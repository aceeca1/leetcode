#include <vector>
#include <unordered_map>
#include <random>
using namespace std;

class RandomizedSet {
    vector<int> v;
    unordered_map<int, int> u;
    default_random_engine rand;

public:
    bool insert(int val) {
        auto e = u.emplace(val, v.size());
        if (e.second) {
            v.emplace_back(val);
            return true;
        } else return false;
    }

    bool remove(int val) {
        auto e = u.find(val);
        if (e == u.end()) return false;
        int k = e->second;
        v[k] = v.back();
        u[v[k]] = k;
        u.erase(e);
        v.pop_back();
        return true;
    }

    int getRandom() {
        uniform_int_distribution<int> uni(0, v.size() - 1);
        return v[uni(rand)];
    }
};
