#include <algorithm>
#include <string>
using namespace std;

class Solution {
public:
    bool wordPattern(string pattern, string str) {
        if (pattern.empty()) return str.empty();
        string m[26];
        int k = 0;
        for (int i: pattern) {
            if (k >= str.size()) return false;
            int k1 = k;
            while (k1 < str.size() && str[k1] != ' ') ++k1;
            auto s = str.substr(k, k1 - k), &u = m[i - 'a'];
            if (u.empty()) u = s;
            else if (s != u) return false;
            k = k1 + 1;
        }
        if (k != str.size() + 1) return false;
        sort(m, m + 26);
        k = 0;
        while (m[k].empty()) ++k;
        for (++k; k < 26; ++k) if (m[k] == m[k - 1]) return false;
        return true;
    }
};
