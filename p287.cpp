#include <algorithm>
using namespace std;

class Solution {
public:
    int findDuplicate(vector<int>& nums) {
        int s = 1, t = nums.size();
        while (s < t) {
            int m = (s + t) >> 1;
            int c = count_if(nums.begin(), nums.end(),
                [&](int k) { return k <= m; });
            if (c > m) t = m; else s = m + 1;
        }
        return s;
    }
};
