#include <memory>
using namespace std;

class Solution {
public:
    bool isIsomorphic(string s, string t) {
        char tr[256]{};
        for (int i = 0; i < s.size(); ++i)
            if (!tr[s[i]]) tr[s[i]] = t[i];
            else if (tr[s[i]] != t[i]) return false;
        memset(tr, 0, sizeof(tr));
        for (int i = 0; i < s.size(); ++i)
            if (!tr[t[i]]) tr[t[i]] = s[i];
            else if (tr[t[i]] != s[i]) return false;
        return true;
    }
};
