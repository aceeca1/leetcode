class Solution {
public:
    int calculateMinimumHP(vector<vector<int>>& dungeon) {
        for (int i = dungeon.size() - 1; i >= 0; --i)
            for (int j = dungeon[0].size() - 1; j >= 0; --j) {
                int ans = 0x7fffffff;
                if (j < dungeon[0].size() - 1) {
                    int k = dungeon[i][j + 1];
                    if (k < ans) ans = k;
                }
                if (i < dungeon.size() - 1) {
                    int k = dungeon[i + 1][j];
                    if (k < ans) ans = k;
                }
                if (ans == 0x7fffffff) ans = 1;
                ans -= dungeon[i][j];
                if (ans < 1) ans = 1;
                dungeon[i][j] = ans;
            }
        return dungeon[0][0];
    }
};
