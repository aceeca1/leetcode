class Solution {
public:
    void gameOfLife(vector<vector<int>>& board) {
        for (int i = 0; i < board.size(); ++i)
            for (int j = 0; j < board[0].size(); ++j) {
                int near = 0;
                bool iT = i < board.size() - 1;
                bool jT = j < board[0].size() - 1;
                if (i) {
                    if (j) near += board[i - 1][j - 1] & 1;
                    if (jT) near += board[i - 1][j + 1] & 1;
                    near += board[i - 1][j] & 1;
                }
                if (iT) {
                    if (j) near += board[i + 1][j - 1] & 1;
                    if (jT) near += board[i + 1][j + 1] & 1;
                    near += board[i + 1][j] & 1;
                }
                if (j) near += board[i][j - 1] & 1;
                if (jT) near += board[i][j + 1] & 1;
                if (near == 3 || near == 2 && board[i][j]) board[i][j] += 2;
            }
        for (int i = 0; i < board.size(); ++i)
            for (int j = 0; j < board[0].size(); ++j)
                board[i][j] >>= 1;
    }
};
