class Solution {
public:
    int countNumbersWithUniqueDigits(int n) {
        int ans = 1, prod = 9;
        for (int i = 1; i <= n; ++i) {
            ans += prod;
            prod *= 10 - i;
        }
        return ans;
    }
};
