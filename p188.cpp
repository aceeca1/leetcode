class Solution {
    int maxProfit(vector<int>& prices) {
        if (prices.empty()) return 0;
        int ans = 0;
        for (int i = 1; i < prices.size(); ++i) {
            int k = prices[i] - prices[i - 1];
            if (k > 0) ans += k;
        }
        return ans;
    }

public:
    int maxProfit(int k, vector<int>& prices) {
        if (k > prices.size() >> 1) return maxProfit(prices);
        if (prices.empty()) return 0;
        vector<int> a(prices.size());
        for (int i = 1; i <= k; ++i) {
            int p = -prices[0], a0 = 0, a1;
            for (int j = 1; j < prices.size(); ++j) {
                a1 = a0;
                a0 = a[j];
                a[j] = a[j - 1];
                int q = p + prices[j];
                if (q > a[j]) a[j] = q;
                q = a1 - prices[j];
                if (q > p) p = q;
            }
        }
        return a.back();
    }
};
