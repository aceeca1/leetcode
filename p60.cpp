#include <algorithm>
#include <string>
using namespace std;

class Solution {
public:
    string getPermutation(int n, int k) {
        string s(n, 0), ans(n, 0);
        for (int i = 0; i < n; ++i) s[i] = '1' + i;
        --k;
        for (int i = 2; i <= n; ++i) {
            ans[n - i] = k % i;
            k /= i;
        }
        for (int i = 0; i < n; ++i) {
            int k = ans[i];
            ans[i] = s[k];
            s.erase(k, 1);
        }
        return move(ans);
    }
};
