class Solution {
    TreeNode *p1 = nullptr, *q, *q1 = nullptr;

    void visit(TreeNode* p) {
        if (p1 && p->val < p1->val)
            if (q1) {
                swap(p->val, q1->val);
                q = nullptr;
            } else {
                q = p;
                q1 = p1;
            }
        p1 = p;
    }

public:
    void recoverTree(TreeNode* root) {
        while (root) {
            TreeNode *p = root->left;
            if (!p) {
                visit(root);
                root = root->right;
                continue;
            }
            while (p->right && p->right != root) p = p->right;
            if (p->right) {
                p->right = nullptr;
                visit(root);
                root = root->right;
                continue;
            }
            p->right = root;
            root = root->left;
        }
        if (q) swap(q->val, q1->val);
    }
};
