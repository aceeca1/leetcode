#include <vector>
using namespace std;

class Solution {
public:
    int numberOfArithmeticSlices(vector<int>& a) {
        int b = INT_MIN, c = 0, ans = 0;
        for (int i = 1; i < a.size(); ++i) {
            int diff = a[i] - a[i - 1];
            if (diff == b) ++c;
            else {
                ans += c * (c - 1) >> 1;
                b = diff;
                c = 1;
            }
        }
        ans += c * (c - 1) >> 1;
        return ans;
    }
};
