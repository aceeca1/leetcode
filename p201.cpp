class Solution {
public:
    int rangeBitwiseAnd(int m, int n) {
        if (m == n) return m;
        int mn = m & n;
        int h = 0, ans = -1;
        while ((m ^ n) != 1) {
            if (!(m & 1)) ans &= (m + 1) << h;
            if (n & 1) ans &= (n - 1) << h;
            ++h;
            m >>= 1;
            n >>= 1;
        }
        return ans & mn;
    }
};
