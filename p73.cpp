#include <algorithm>
using namespace std;

class Solution {
public:
    void setZeroes(vector<vector<int>>& matrix) {
        int a0 = any_of(matrix[0].begin(), matrix[0].end(),
            [&](int m0i) { return !m0i; });
        int b0 = any_of(matrix.begin(), matrix.end(),
            [&](const vector<int>& mi) { return !mi[0]; });
        for (int i = 1; i < matrix.size(); ++i)
            for (int j = 1; j < matrix[0].size(); ++j) {
                if (matrix[i][j]) continue;
                matrix[i][0] = 0;
                matrix[0][j] = 0;
            }
        for (int i = 1; i < matrix.size(); ++i)
            for (int j = 1; j < matrix[0].size(); ++j) {
                if (!matrix[i][0]) matrix[i][j] = 0;
                if (!matrix[0][j]) matrix[i][j] = 0;
            }
        if (a0) for (int i = 0; i < matrix[0].size(); ++i) matrix[0][i] = 0;
        if (b0) for (int i = 0; i < matrix.size(); ++i) matrix[i][0] = 0;
    }
};
