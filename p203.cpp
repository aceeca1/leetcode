class Solution {
public:
    ListNode* removeElements(ListNode* head, int val) {
        ListNode h(0), *p = &h;
        for (auto i = head; i; ) if (i->val != val) {
            p = p->next = i;
            i = i->next;
        } else {
            auto i1 = i->next;
            delete i;
            i = i1;
        }
        p->next = nullptr;
        return h.next;
    }
};
