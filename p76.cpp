#include <string>
using namespace std;

class Solution {
public:
    string minWindow(string s, string t) {
        int m[256]{}, k = 0;
        for (char ti: t) if (!m[ti]--) ++k;
        int q = 0;
        for (; q < s.size() && k; ++q) if (++m[s[q]] == 0) --k;
        if (k) return "";
        int p = 0, mV = 0x7fffffff, pMV, qMV;
        for (;;) {
            while (m[s[p]]) { --m[s[p]]; ++p; }
            int v = q - p;
            if (v < mV) { mV = v; pMV = p; qMV = q; }
            char c = s[p++];
            while (q < s.size() && s[q] != c) ++m[s[q++]];
            if (q++ == s.size()) return s.substr(pMV, qMV - pMV);
        }
    }
};
