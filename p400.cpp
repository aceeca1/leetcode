#include <cstdint>
using namespace std;

class Solution {
public:
    int findNthDigit(int n) {
        --n;
        int64_t a = 1;
        int64_t b = 1;
        for (;;) {
            int64_t c = a * b * 9L;
            if (n < c) {
                int64_t n1 = n / a;
                int64_t n2 = n % a;
                auto s = to_string(b + n1);
                return s[n2] - '0';
            }
            n -= c;
            b *= 10L;
            ++a;
        }
    }
};
