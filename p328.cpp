class Solution {
public:
    ListNode* oddEvenList(ListNode* head) {
        ListNode h1(0), h2(0), *p1 = &h1, *p2 = &h2;
        int d = 0;
        for (auto i = head; i; i = i->next) {
            if (d) p2 = p2->next = i;
            else p1 = p1->next = i;
            d = !d;
        }
        p1->next = h2.next;
        p2->next = nullptr;
        return h1.next;
    }
};
