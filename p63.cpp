class Solution {
public:
    int uniquePathsWithObstacles(vector<vector<int>>& obstacleGrid) {
        auto &a = obstacleGrid;
        for (int i = 0; i < a.size(); ++i)
            for (int j = 0; j < a[0].size(); ++j)
                if (a[i][j]) a[i][j] = 0;
                else if (!i && !j) a[i][j] = 1;
                else {
                    int k = 0;
                    if (i) k += a[i - 1][j];
                    if (j) k += a[i][j - 1];
                    a[i][j] = k;
                }
        return a.back().back();
    }
};
