class Solution {
public:
    ListNode* rotateRight(ListNode* head, int k) {
        if (!head) return nullptr;
        int len = 0;
        for (auto p = head; p; p = p->next) ++len;
        k %= len;
        auto p = head, q = head;
        while (k--) q = q->next;
        while (q->next) {
            p = p->next;
            q = q->next;
        }
        q->next = head;
        head = p->next;
        p->next = nullptr;
        return head;
    }
};
