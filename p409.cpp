#include <string>
#include <vector>
using namespace std;

class Solution {
public:
    int longestPalindrome(string s) {
        vector<int> a(256);
        for (int i = 0; i < s.size(); ++i) ++a[s[i]];
        bool odd = false;
        int ans = 0;
        for (int i = 0; i < 256; ++i) {
            if (a[i] & 1) odd = true;
            ans += a[i] & ~1;
        }
        return ans + odd;
    }
};
