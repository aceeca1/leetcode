class Solution {
    TreeNode* buildTree(int* pre, int* in, int z) {
        if (z == 0) return nullptr;
        int k = 0;
        while (in[k] != pre[0]) ++k;
        TreeNode *ans = new TreeNode(pre[0]);
        ans->left = buildTree(pre + 1, in, k);
        ans->right = buildTree(pre + k + 1, in + k + 1, z - k - 1);
        return ans;
    }

public:
    TreeNode* buildTree(vector<int>& preorder, vector<int>& inorder) {
        return buildTree(&preorder[0], &inorder[0], preorder.size());
    }
};
