#include <stack>
using namespace std;

class MinStack {
    stack<pair<int, int>> s;

public:
    MinStack() {}

    void push(int x) {
        int min = x;
        if (!s.empty() && s.top().second < min) min = s.top().second;
        s.emplace(x, min);
    }

    void pop() { s.pop(); }

    int top() { return s.top().first; }

    int getMin() { return s.top().second; }
};
