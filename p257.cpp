class Solution {
    vector<string> ans;
    string pr;

    void preOrder(TreeNode* p) {
        if (!p) return;
        if (!pr.empty()) pr += "->";
        pr += to_string(p->val);
        if (!p->left && !p->right)
            ans.emplace_back(pr);
        else {
            preOrder(p->left);
            preOrder(p->right);
        }
        while (!pr.empty() && pr.back() != '>') pr.pop_back();
        if (!pr.empty()) {
            pr.pop_back();
            pr.pop_back();
        }
    }

public:
    vector<string> binaryTreePaths(TreeNode* root) {
        preOrder(root);
        return move(ans);
    }
};
