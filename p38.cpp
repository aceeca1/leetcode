#include <string>
using namespace std;

class Solution {
public:
    string countAndSay(int n) {
        string s{"1"};
        for (int i = 2; i <= n; ++i) {
            string r;
            char c = s[0];
            int rep = 1;
            for (int i = 1; i <= s.size(); ++i)
                if (i < s.size() && s[i] == c) ++rep;
                else {
                    char p[10];
                    sprintf(p, "%d", rep);
                    r += p;
                    r += c;
                    c = s[i];
                    rep = 1;
                }
            s = r;
        }
        return s;
    }
};
