class Solution {
    int bc[64]{};

public:
    vector<string> readBinaryWatch(int num) {
        for (int i = 1; i < 63; ++i)
            if (i & 1) bc[i] = bc[i >> 1] + 1;
            else bc[i] = bc[i >> 1];
        vector<string> ret;
        for (int i = 0; i < 12; ++i)
            for (int j = 0; j < 60; ++j)
                if (bc[i] + bc[j] == num) {
                    char c[10];
                    sprintf(c, "%d:%02d", i, j);
                    ret.emplace_back(c);
                }
        return ret;
    }
};
