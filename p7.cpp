#include <cstdint>
using namespace std;

class Solution {
public:
    int reverse(int x) {
        if (x == 0x80000000) return 0;
        if (x < 0) return -reverse(-x);
        int64_t r = 0;
        while (x) {
            r = r * 10 + x % 10;
            x /= 10;
        }
        if (r > 0x7fffffff) return 0;
        return r;
    }
};
