#include <queue>
#include <vector>
using namespace std;

class Solution {
    struct Node {
        int i1, i2, v;
        bool operator<(const Node& that) const { return v > that.v; }
        void inc(const vector<int>& nums1, const vector<int>& nums2) {
            ++i1;
            if (i1 >= nums1.size()) v = 0x7fffffff;
            else v = nums1[i1] + nums2[i2];
        }
    };

public:
    vector<pair<int, int>> kSmallestPairs(vector<int>& nums1, vector<int>& nums2, int k) {
        if (nums1.empty() || nums2.empty()) return {};
        priority_queue<Node> pq;
        for (int i = 0; i < nums2.size(); ++i)
            pq.emplace(Node{0, i, nums1[0] + nums2[i]});
        vector<pair<int, int>> ans;
        while (k--) {
            if (pq.empty()) break;
            auto pqH = pq.top();
            if (pqH.v == 0x7fffffff) break;
            pq.pop();
            ans.emplace_back(nums1[pqH.i1], nums2[pqH.i2]);
            pqH.inc(nums1, nums2);
            pq.emplace(move(pqH));
        }
        return move(ans);
    }
};
