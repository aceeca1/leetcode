class Solution {
    vector<vector<int>> e;
    vector<int> a;
    vector<string> ans;
    string str, pr;
    int m = 0;

    void put() {
        if (m == str.size()) {
            ans.emplace_back(pr);
            return;
        }
        if (!pr.empty()) pr += ' ';
        int k = m;
        for (int i: e[m]) {
            if (!a[i]) continue;
            while (k < i) pr += str[k++];
            m = i;
            put();
        }
        while (!pr.empty() && pr.back() != ' ') {
            pr.pop_back();
            --m;
        }
        if (!pr.empty()) pr.pop_back();
    }

    int hash(string s) {
        int ans = 0;
        for (int i = 0; i < s.size(); ++i) ans = ans * 0xdeadbeef + s[i];
        return ans;
    }

public:
    vector<string> wordBreak(string s, unordered_set<string>& wordDict) {
        e.resize(s.size() + 1);
        unordered_set<int> w;
        for (auto& i: wordDict) w.emplace(hash(i));
        for (int i = 0; i < s.size(); ++i) {
            int h = 0;
            for (int j = i + 1; j <= s.size(); ++j) {
                h = h * 0xdeadbeef + s[j - 1];
                if (w.count(h)) e[i].emplace_back(j);
            }
        }
        a.resize(s.size() + 1);
        a[s.size()] = true;
        for (int i = s.size() - 1; i >= 0; --i)
            for (auto j: e[i]) a[i] |= a[j];
        str = move(s);
        put();
        return move(ans);
    }
};
