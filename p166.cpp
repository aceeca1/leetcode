#include <string>
#include <cstdint>
using namespace std;

class Solution {
    int64_t num, denom;
    string ans;

    void f(int64_t& n, bool b = false) {
        n *= 10;
        int64_t a = n / denom;
        if (b) ans += '0' + a;
        n -= a * denom;
    }

public:
    string fractionToDecimal(int numerator, int denominator) {
        num = numerator;
        denom = denominator;
        if (denom < 0) { num = -num; denom = -denom; }
        if (num < 0) { ans += '-'; num = -num; }
        int64_t a = num / denom;
        ans += to_string(a);
        num -= a * denom;
        if (!num) return move(ans);
        ans += '.';
        int n1 = 0, n2 = 1;
        int64_t x1 = num, x2 = num; f(x2);
        while (x2 && x1 != x2) {
            if (n1 + n1 < n2) { n1 = n2; x1 = x2; }
            ++n2; f(x2);
        }
        n2 -= n1;
        if (x2) {
            int64_t x1 = num, x2 = num;
            while (n2--) f(x2);
            while (x1 != x2) {
                f(x1, true);
                f(x2);
            }
            ans += '(';
            f(x1, true);
            while (x1 != x2) f(x1, true);
            ans += ')';
            return move(ans);
        }
        x1 = num;
        while (x1) f(x1, true);
        return move(ans);
    }
};
