#include <algorithm>
using namespace std;

class Solution {
public:
    void reverseWords(string &s) {
        int p = 0;
        for (int i = 0; i <= s.size(); ++i)
            if (i < s.size() && s[i] != ' ') s[p++] = s[i];
            else if (p && s[p - 1] != ' ') s[p++] = ' ';
        if (!p) { s.clear(); return; }
        s.resize(p - 1);
        p = 0;
        for (int i = 0; i <= s.size(); ++i) {
            if (i < s.size() && s[i] != ' ') continue;
            reverse(s.begin() + p, s.begin() + i);
            p = i + 1;
        }
        reverse(s.begin(), s.end());
    }
};
