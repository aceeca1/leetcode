class Solution {
public:
    int maxSubArray(int* a, int z) {
        if (!z) return 0x80000000;
        if (z == 1) return a[0];
        int z0 = z >> 1;
        int r1 = maxSubArray(a, z0);
        int r2 = maxSubArray(a + z0 + 1, z - z0 - 1);
        int r = r1 > r2 ? r1 : r2;
        int a1 = 0, s = 0;
        for (int i = z0 - 1; i >= 0; --i) {
            s += a[i];
            if (s > a1) a1 = s;
        }
        int a2 = 0; s = 0;
        for (int i = z0 + 1; i < z; ++i) {
            s += a[i];
            if (s > a2) a2 = s;
        }
        int r3 = a1 + a[z0] + a2;
        if (r3 > r) r = r3;
        return r;
    }

    int maxSubArray(vector<int>& nums) {
        return maxSubArray(&nums[0], nums.size());
    }
};
