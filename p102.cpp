class Solution {
    vector<vector<int>> ans;
    int level = 0;

    void preOrder(TreeNode* p) {
        if (level >= ans.size()) ans.emplace_back();
        ans[level++].emplace_back(p->val);
        if (p->left) preOrder(p->left);
        if (p->right) preOrder(p->right);
        --level;
    }

public:
    vector<vector<int>> levelOrder(TreeNode* root) {
        if (!root) return vector<vector<int>>();
        preOrder(root);
        return move(ans);
    }
};
