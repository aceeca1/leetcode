#include <vector>
#include <string>
using namespace std;

class Solution {
    vector<vector<string>> v;
    vector<int> a;
    int m, m1 = -1, m2 = -1, z, k = 0;

    void put() {
        if (k == z) {
            v.emplace_back(z, string(z, '.'));
            auto& vb = v.back();
            for (int i = 0; i < z; ++i) {
                int j = 0;
                while (!((a[i] >> j) & 1)) ++j;
                vb[i][j] = 'Q';
            }
            return;
        }
        int m0 = m & (m1 >> k) & (m2 >> (z - 1 - k));
        for (;;) {
            int i = m0 & -m0;
            if (!i) break;
            m0 ^= i;
            a[k] = i;
            m ^= i, m1 ^= i << k, m2 ^= i << (z - 1 - k);
            ++k, put(), --k;
            m ^= i, m1 ^= i << k, m2 ^= i << (z - 1 - k);
        }
    }

public:
    vector<vector<string>> solveNQueens(int n) {
        a.resize(z = n);
        m = (1 << n) - 1;
        put();
        return move(v);
    }
};
