#include <cmath>
using namespace std;

class Solution {
public:
    double myPow(double x, int n) {
        if (x < 0) return n & 1 ? -myPow(-x, n) : myPow(-x, n);
        return exp(log(x) * n);
    }
};
