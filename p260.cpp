class Solution {
public:
    vector<int> singleNumber(vector<int>& nums) {
        int a = 0;
        for (int i: nums) a ^= i;
        a &= -a;
        int a1 = 0, a2 = 0;
        for (int i: nums)
            if (i & a) a1 ^= i;
            else a2 ^= i;
        return vector<int>{a1, a2};
    }
};
