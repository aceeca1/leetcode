#include <stack>
using namespace std;

class Solution {
public:
    UndirectedGraphNode *cloneGraph(UndirectedGraphNode *node) {
        if (!node) return nullptr;
        unordered_map<UndirectedGraphNode*, UndirectedGraphNode*> a;
        stack<UndirectedGraphNode*> s;
        a[node] = new UndirectedGraphNode(node->label);
        s.emplace(node);
        while (!s.empty()) {
            auto p = s.top(), q = a[p];
            s.pop();
            for (auto i: p->neighbors) {
                auto ai = a.emplace(i, nullptr);
                if (ai.second) {
                    ai.first->second = new UndirectedGraphNode(i->label);
                    s.emplace(i);
                }
                q->neighbors.emplace_back(ai.first->second);
            }
        }
        return a[node];
    }
};
