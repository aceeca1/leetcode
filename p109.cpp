class Solution {
    ListNode* n;

    TreeNode *sortedListToBST(int z) {
        if (!z) return nullptr;
        int k = z >> 1;
        auto p = new TreeNode(0);
        p->left = sortedListToBST(k);
        p->val = n->val;
        n = n->next;
        p->right = sortedListToBST(z - k - 1);
        return p;
    }

public:
    TreeNode* sortedListToBST(ListNode* head) {
        n = head;
        int z = 0;
        while (head) { ++z; head = head->next; }
        return sortedListToBST(z);
    }
};
