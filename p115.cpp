class Solution {
public:
    int numDistinct(string s, string t) {
        vector<int> a(t.size() + 1);
        a[0] = 1;
        for (int i = 1; i <= s.size(); ++i)
            for (int j = t.size(); j; --j)
                if (s[i - 1] == t[j - 1]) a[j] += a[j - 1];
        return a.back();
    }
};
