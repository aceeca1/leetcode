#include <vector>
#include <cstdint>
#include <string>
using namespace std;

class Solution {
public:
    vector<int> findSubstring(string s, vector<string>& words) {
        int n = words.size(), m = words[0].size();
        size_t h = 0;
        for (auto& wi: words) h += hash<string>()(wi);
        vector<int> ans;
        for (int st = 0; st < m; ++st) {
            if (st + n * m > s.size()) break;
            int p = st, q = st;
            size_t pqh = 0;
            for (int i = 0; i < n; ++i) {
                pqh += hash<string>()(s.substr(q, m));
                q += m;
            }
            for (;;) {
                if (pqh == h) ans.emplace_back(p);
                pqh -= hash<string>()(s.substr(p, m));
                p += m;
                if (q + m > s.size()) break;
                pqh += hash<string>()(s.substr(q, m));
                q += m;
            }
        }
        return ans;
    }
};
