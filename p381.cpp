#include <vector>
#include <unordered_set>
#include <unordered_map>
#include <random>
using namespace std;

class RandomizedCollection {
    vector<int> v;
    unordered_map<int, unordered_set<int>> u;
    default_random_engine rand;

public:
    bool insert(int val) {
        auto &uv = u[val];
        uv.emplace(v.size());
        v.emplace_back(val);
        return uv.size() == 1;
    }

    bool remove(int val) {
        auto e = u.find(val);
        if (e == u.end()) return false;
        int k = *e->second.begin();
        v[k] = v.back();
        u[v[k]].erase(v.size() - 1);
        u[v[k]].emplace(k);
        e->second.erase(k);
        if (e->second.empty()) u.erase(e);
        v.pop_back();
        return true;
    }

    int getRandom() {
        uniform_int_distribution<int> uni(0, v.size() - 1);
        return v[uni(rand)];
    }
};
