class Solution {
public:
    int maximalSquare(vector<vector<char>>& matrix) {
        if (matrix.empty() || matrix[0].empty()) return 0;
        vector<int> a0(matrix[0].size()), a1(a0.size());
        int ans = 0;
        for (int i = 0; i < matrix.size(); ++i) {
            a0[0] = matrix[i][0] == '1';
            if (a0[0] > ans) ans = a0[0];
            for (int j = 1; j < matrix[0].size(); ++j) {
                if (matrix[i][j] == '0') { a0[j] = 0; continue; }
                int v = a1[j], v1 = a1[j - 1], v2 = a0[j - 1];
                if (v1 < v) v = v1;
                if (v2 < v) v = v2;
                a0[j] = ++v;
                if (v > ans) ans = v;
            }
            swap(a0, a1);
        }
        return ans * ans;
    }
};
