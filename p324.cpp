#include <algorithm>
#include <vector>
#include <iterator>
using namespace std;

class Solution {
    struct WeirdIter: public iterator<random_access_iterator_tag, int> {
        int *v, n, m, k;
        WeirdIter(int* v_, int n_, int m_, int k_):
            v(v_), n(n_), m(m_), k(k_) {}
        bool operator==(const WeirdIter& that) const { return k == that.k; }
        int operator-(const WeirdIter& that) const { return k - that.k; }
        WeirdIter operator+(int i) const { return WeirdIter{v, n, m, k + i}; }
        bool operator<=(const WeirdIter& that) const { return k <= that.k; }
        bool operator<(const WeirdIter& that) const { return k < that.k; }
        WeirdIter& operator++() { ++k; return *this;}
        WeirdIter operator-(int i) const { return WeirdIter{v, n, m, k - i}; }
        bool operator!=(const WeirdIter& that) const { return k != that.k; }
        WeirdIter& operator--() { --k; return *this;}
        int& operator*() const {
            return v[k >= m ? ((n - 1 - k) << 1) + 1 : (m - 1 - k) << 1];
        }
    };

public:
    void wiggleSort(vector<int>& nums) {
        int n = nums.size(), m = (n + 1) >> 1;
        WeirdIter a{&nums[0], n, m, 0};
        WeirdIter b{&nums[0], n, m, m};
        WeirdIter c{&nums[0], n, m, n};
        nth_element(a, b, c);
        auto bZ = b - 1;
        for (auto i = bZ; a <= i; --i)
            if (*i == *b) { swap(*i, *bZ); --bZ; }
        bZ = b + 1;
        for (auto i = bZ; i < c; ++i)
            if (*i == *b) { swap(*i, *bZ); ++bZ; }
    }
};
