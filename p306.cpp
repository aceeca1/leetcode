#include <cstdint>
#include <cinttypes>
#include <cstdio>
#include <string>
#include <vector>
#include <utility>
#include <cstring>
using namespace std;

#ifndef SCNu64
#define SCNu64 "llu"
#endif

#ifndef PRIu64
#define PRIu64 "llu"
#endif

class Solution {
    struct BigInt {
        static constexpr uint64_t M = 1000000000000000000;
        vector<uint64_t> v;

        long long scanLL(const char* s, int z) {
            long long ans = 0;
            for (int i = 0; i < z; ++i) ans = ans * 10 + (s[i] - '0');
            return ans;
        }

        BigInt(const char* s, int z) {
            for (int i = z; i > 0; i -= 18) {
                int p = i - 18;
                if (p < 0) p = 0;
                v.emplace_back(scanLL(s + p, i - p));
            }
        }

        operator string() {
            string ans = to_string(v.back());
            char s[20];
            for (int i = v.size() - 2; i >= 0; --i) {
                sprintf(s, "%018" PRIu64, v[i]);
                ans += s;
            }
            return ans;
        }

        BigInt& operator+=(const BigInt& that) {
            if (that.v.size() > v.size()) v.resize(that.v.size());
            uint64_t carry = 0;
            for (int i = 0; i < v.size(); ++i) {
                carry += v[i];
                if (i < that.v.size()) carry += that.v[i];
                v[i] = carry % M;
                carry /= M;
            }
            if (carry) v.emplace_back(carry);
            return *this;
        }
    };

    const char *numc;
    int z;

    bool succ(int p1, int p2) {
        if (numc[0] == '0' && p1 > 1) return false;
        if (numc[p1] == '0' && p2 - p1 > 1) return false;
        BigInt a1(numc, p1), a2(numc + p1, p2 - p1);
        for (int k = p2; k < z; ) {
            swap(a1, a2);
            a2 += a1;
            string s2(a2);
            if (strncmp(numc + k, s2.c_str(), s2.size())) return false;
            k += s2.size();
        }
        return true;
    }

public:
    bool isAdditiveNumber(string num) {
        numc = num.c_str();
        z = num.size();
        for (int i = 1; i < z; ++i)
            for (int j = i + 1; j < z; ++j)
                if (succ(i, j)) return true;
        return false;
    }
};
