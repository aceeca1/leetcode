#include <unordered_map>
using namespace std;

class Solution {
public:
    int maxPoints(vector<Point>& points) {
        int ans = 0;
        for (auto& i: points) {
            unordered_map<double, int> a;
            int same = 0;
            int vSame = 0;
            for (auto& j: points) {
                if (i.x == j.x && i.y == j.y) {
                    ++same;
                    continue;
                }
                double dy = j.y - i.y, dx = j.x - i.x;
                if (dx == 0.0) {
                    ++vSame;
                    continue;
                }
                ++a[dy / dx];
            }
            for (auto& j: a) {
                int k = j.second + same;
                if (k > ans) ans = k;
            }
            vSame += same;
            if (vSame > ans) ans = vSame;
        }
        return ans;
    }
};
