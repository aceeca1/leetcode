#include <algorithm>
#include <vector>
using namespace std;

class Solution {
public:
    vector<int> plusOne(vector<int>& digits) {
        int i = digits.size() - 1;
        while (i >= 0 && digits[i] == 9) --i;
        if (i == -1) {
            digits[0] = 1;
            fill(digits.begin() + 1, digits.end(), 0);
            digits.emplace_back(0);
            return move(digits);
        }
        ++digits[i];
        fill(digits.begin() + i + 1, digits.end(), 0);
        return move(digits);
    }
};
