class Solution {
    int c = 0xdeadbeef;

public:
    bool isValidBST(TreeNode* root) {
        if (!root) return true;
        if (!isValidBST(root->left)) return false;
        if (c != 0xdeadbeef && c >= root->val) return false;
        c = root->val;
        return isValidBST(root->right);
    }
};
