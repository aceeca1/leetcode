#include <vector>
using namespace std;

class Solution {
public:
    bool canJump(vector<int>& nums) {
        int s = 0, t = 0;
        while (nums.size() - 1 > t) {
            int t0 = t;
            for (int i = s; i <= t; ++i) {
                int v = i + nums[i];
                if (v > t0) t0 = v;
            }
            if (t0 == t) return false;
            s = t + 1;
            t = t0;
        }
        return true;
    }
};
