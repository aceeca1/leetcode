class Solution {
public:
    ListNode* mergeTwoLists(ListNode* l1, ListNode* l2) {
        ListNode h(0), *p = &h;
        while (l1 || l2)
            if (l1 && (!l2 || l1->val < l2->val)) {
                p = p->next = l1;
                l1 = l1->next;
            } else {
                p = p->next = l2;
                l2 = l2->next;
            }
        return h.next;
    }

    ListNode* mergeKLists(vector<ListNode*>& lists) {
        if (lists.empty()) return nullptr;
        while (lists.size() > 1) {
            int j = 0;
            for (int i = 0; i < lists.size() - 1; i += 2)
                lists[j++] = mergeTwoLists(lists[i], lists[i + 1]);
            if (lists.size() & 1) lists[j++] = lists.back();
            lists.resize(j);
        }
        return lists[0];
    }
};
