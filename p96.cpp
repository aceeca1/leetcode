#include <cstdint>
using namespace std;

class Solution {
public:
    int numTrees(int n) {
        int64_t ans = 1;
        int m = n + n;
        for (int i = 1; i <= n; ++i) {
            ans *= m + 1 - i;
            ans /= i;
        }
        ans /= n + 1;
        return ans;
    }
};
