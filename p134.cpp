class Solution {
public:
    int canCompleteCircuit(vector<int>& gas, vector<int>& cost) {
        int sum = 0, minPSum = 0, p = -1;
        for (int i = 0; i < gas.size(); ++i) {
            sum += gas[i] - cost[i];
            if (sum < minPSum) { minPSum = sum; p = i; }
        }
        return sum < 0 ? -1 : (p + 1) % gas.size();
    }
};
