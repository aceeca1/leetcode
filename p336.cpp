#include <vector>
#include <string>
#include <algorithm>
#include <unordered_map>
#include <cstdint>
using namespace std;

class Solution {
    unordered_map<uint32_t, int> w;
    vector<vector<int>> ans;
    bool rev = false;

    uint32_t h(const string& s) {
        uint32_t ans = 0;
        for (char i: s) ans = ans * 0xdeadbeef + i;
        return ans;
    }

    void addPrefixPalin(int no, const string& s) {
        cout << no << endl;
        vector<int> n(s.size() + 1);
        n[0] = -1;
        for (int i = 1; i <= s.size(); ++i) {
            int j = n[i - 1];
            while (j >= 0 && s[j] != s[i - 1]) j = n[j];
            n[i] = j + 1;
        }
        int k = 0;
        for (int i = s.size() - 1; i >= 0; --i) {
            while (k >= 0 && s[k] != s[i]) k = n[k];
            ++k;
        }
        int p = 0;
        int t = rev ? -1 : 0;
        for (int i = s.size() - 1; i >= t; --i) {
            if (k > i) {
                auto wp = w.find(p);
                if (wp != w.end() && wp->second != no) {
                    if (rev) ans.emplace_back(vector<int>{no, wp->second});
                    else ans.emplace_back(vector<int>{wp->second, no});
                }
                k = n[k];
            }
            if (i >= 0) p = p * 0xdeadbeef + s[i];
        }
    }

    void palinPairs(const vector<string>& words) {
        for (int i = 0; i < words.size(); ++i) w[h(words[i])] = i;
        for (int i = 0; i < words.size(); ++i) addPrefixPalin(i, words[i]);
        w.clear();
    }

public:
    vector<vector<int>> palindromePairs(vector<string>& words) {
        palinPairs(words);
        for (auto& i: words) reverse(i.begin(), i.end());
        rev = true;
        palinPairs(words);
        return move(ans);
    }
};
