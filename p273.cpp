class Solution {
    void a20(string& s, int num) {
        if (!s.empty()) s += ' ';
        switch (num) {
            case 0: s += "Zero"; break;
            case 1: s += "One"; break;
            case 2: s += "Two"; break;
            case 3: s += "Three"; break;
            case 4: s += "Four"; break;
            case 5: s += "Five"; break;
            case 6: s += "Six"; break;
            case 7: s += "Seven"; break;
            case 8: s += "Eight"; break;
            case 9: s += "Nine"; break;
            case 10: s += "Ten"; break;
            case 11: s += "Eleven"; break;
            case 12: s += "Twelve"; break;
            case 13: s += "Thirteen"; break;
            case 14: s += "Fourteen"; break;
            case 15: s += "Fifteen"; break;
            case 16: s += "Sixteen"; break;
            case 17: s += "Seventeen"; break;
            case 18: s += "Eighteen"; break;
            case 19: s += "Nineteen";
        }
    }

    void a100(string& s, int num) {
        if (num < 20) return a20(s, num);
        if (!s.empty()) s += ' ';
        switch (num / 10) {
            case 2: s += "Twenty"; break;
            case 3: s += "Thirty"; break;
            case 4: s += "Forty"; break;
            case 5: s += "Fifty"; break;
            case 6: s += "Sixty"; break;
            case 7: s += "Seventy"; break;
            case 8: s += "Eighty"; break;
            case 9: s += "Ninety"; break;
        }
        num %= 10;
        if (num) a20(s, num);
    }

    void a1000(string& s, int num) {
        if (num < 100) return a100(s, num);
        a20(s, num / 100);
        s += " Hundred";
        num %= 100;
        if (num) a100(s, num);
    }

public:
    string numberToWords(int num) {
        int b = num / 1000000000;
        int m = num / 1000000 % 1000;
        int k = num / 1000 % 1000;
        int n = num % 1000;
        string ans;
        if (b) {
            a1000(ans, b);
            ans += " Billion";
        }
        if (m) {
            a1000(ans, m);
            ans += " Million";
        }
        if (k) {
            a1000(ans, k);
            ans += " Thousand";
        }
        if (n || ans.empty()) a1000(ans, n);
        return move(ans);
    }
};
