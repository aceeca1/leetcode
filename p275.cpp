class Solution {
public:
    int hIndex(vector<int>& citations) {
        int n = citations.size(), s = 0, t = n;
        while (s < t) {
            int m = (s + t) >> 1;
            if (citations[m] >= n - m) t = m;
            else s = m + 1;
        }
        return n - s;
    }
};
