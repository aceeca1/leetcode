class Solution {
public:
    int minDepth(TreeNode* root) {
        if (!root) return 0;
        if (!root->left && !root->right) return 1;
        int ans = 0x7fffffff;
        if (root->left) {
            int r = minDepth(root->left) + 1;
            if (r < ans) ans = r;
        }
        if (root->right) {
            int r = minDepth(root->right) + 1;
            if (r < ans) ans = r;
        }
        return ans;
    }
};
