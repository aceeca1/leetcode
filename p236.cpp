class Solution {
    TreeNode *pu, *qu;
    vector<TreeNode*> pr, pA, qA;

    void preOrder(TreeNode* u) {
        if (!u) return;
        pr.emplace_back(u);
        if (u == pu) pA = pr;
        if (u == qu) qA = pr;
        preOrder(u->left);
        preOrder(u->right);
        pr.pop_back();
    }

public:
    TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
        pu = p;
        qu = q;
        preOrder(root);
        while (pA.size() > qA.size()) pA.pop_back();
        while (pA.size() < qA.size()) qA.pop_back();
        while (pA.back() != qA.back()) {
            pA.pop_back();
            qA.pop_back();
        }
        return pA.back();
    }
};
