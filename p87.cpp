#include <string>
using namespace std;

class Solution {
    string str1, str2;
    int n;
    vector<int> a;
    int m[256];

    bool is(int len, int p1, int p2) {
        int idx = ((len - 1) * n + p1) * n + p2;
        if (a[idx]) return a[idx] == 1;
        if (len == 1) {
            bool b = str1[p1] == str2[p2];
            a[idx] = b ? 1 : -1;
            return b;
        }
        for (int i = 0; i < 256; ++i) m[i] = 0;
        for (int i = 0; i < len; ++i) {
            ++m[str1[p1 + i]];
            --m[str2[p2 + i]];
        }
        for (int i = 0; i < 256; ++i) if (m[i]) {
            a[idx] = -1;
            return false;
        }
        for (int i = 1; i < len; ++i) {
            if (is(i, p1, p2))
                if (is(len - i, p1 + i, p2 + i))
                    return a[idx] = 1;
            if (is(i, p1, p2 + len - i))
                if (is(len - i, p1 + i, p2))
                    return a[idx] = 1;
        }
        a[idx] = -1;
        return false;
    }

public:
    bool isScramble(string s1, string s2) {
        str1 = move(s1);
        str2 = move(s2);
        n = str1.size();
        a.resize(n * n * n);
        return is(n, 0, 0);
    }
};
