class Solution {
public:
    int maxProfit(vector<int>& prices) {
        if (prices.empty()) return 0;
        int p = -prices[0], a = 0, a1 = 0, a2 = 0;
        for (int i = 1; i < prices.size(); ++i) {
            int k = p + prices[i];
            if (k > a) a = k;
            k = a2 - prices[i];
            if (k > p) p = k;
            a2 = a1;
            a1 = a;
        }
        return a;
    }
};
