#include <vector>
using namespace std;

class Solution {
public:
    int maxCoins(vector<int>& nums) {
        nums.insert(nums.begin(), 1);
        nums.emplace_back(1);
        int n = nums.size();
        vector<vector<int>> a(n);
        for (int i = n - 1; i >= 1; --i) {
            a[i].resize(n + 1 - i);
            for (int j = 1; j < n - i; ++j)
                for (int k = 0; k < j; ++k) {
                    int v = a[i][k] + a[i + k + 1][j - k - 1];
                    v += nums[i - 1] * nums[i + k] * nums[i + j];
                    if (v > a[i][j]) a[i][j] = v;
                }
        }
        return a[1][n - 2];
    }
};
