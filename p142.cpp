class Solution {
public:
    ListNode *detectCycle(ListNode *head) {
        if (!head || !head->next) return nullptr;
        int n1 = 0, n2 = 1;
        auto x1 = head, x2 = head->next;
        while (x1 != x2) {
            if (n2 > n1 + n1) { n1 = n2; x1 = x2; }
            ++n2; x2 = x2->next;
            if (!x2) return nullptr;
        }
        n2 -= n1;
        x1 = x2 = head;
        while (n2--) x2 = x2->next;
        while (x1 != x2) {
            x1 = x1->next;
            x2 = x2->next;
        }
        return x1;
    }
};
