#include <vector>
#include <string>
using namespace std;

class Solution {
public:
    string removeDuplicateLetters(string s) {
        vector<int> c(s.size() + 1);
        int a[26]{};
        for (int i = s.size() - 1; i >= 0; --i) c[i] = !a[s[i] - 'a']++;
        c[s.size()] = 1;
        int p = 0;
        for (;;) {
            int q = p;
            while (q < s.size() && (!c[q] || !s[q])) ++q;
            if (q == s.size()) break;
            char min = 0x7f;
            int i_min;
            for (int i = p; i <= q; ++i) {
                if (!s[i]) continue;
                if (s[i] < min) {
                    min = s[i];
                    i_min = i;
                }
            }
            for (int i = p; i < i_min; ++i) s[i] = 0;
            for (int i = i_min + 1; i < s.size(); ++i)
                if (s[i] == min) s[i] = 0;
            p = i_min + 1;
        }
        s.resize(remove(s.begin(), s.end(), 0) - s.begin());
        return move(s);
    }
};
