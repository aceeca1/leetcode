bool isBadVersion(int version);

class Solution {
public:
    int firstBadVersion(int n) {
        int s = 1, t = n;
        while (s < t) {
            int m = s + ((t - s) >> 1);
            if (isBadVersion(m)) t = m;
            else s = m + 1;
        }
        return s;
    }
};
