class Solution {
    vector<vector<int>> a, len;

    int put(int x, int y) {
        if (len[x][y]) return len[x][y];
        int ans = 0;
        if (x && a[x - 1][y] < a[x][y]) {
            int k = put(x - 1, y);
            if (k > ans) ans = k;
        }
        if (x < a.size() - 1 && a[x + 1][y] < a[x][y]) {
            int k = put(x + 1, y);
            if (k > ans) ans = k;
        }
        if (y && a[x][y - 1] < a[x][y]) {
            int k = put(x, y - 1);
            if (k > ans) ans = k;
        }
        if (y < a[0].size() - 1 && a[x][y + 1] < a[x][y]) {
            int k = put(x, y + 1);
            if (k > ans) ans = k;
        }
        return len[x][y] = ans + 1;
    }

public:
    int longestIncreasingPath(vector<vector<int>>& matrix) {
        if (!matrix.size() || !matrix[0].size()) return 0;
        a = move(matrix);
        len.resize(a.size(), vector<int>(a[0].size()));
        int ans = 0;
        for (int i = 0; i < a.size(); ++i)
            for (int j = 0; j < a[0].size(); ++j) {
                int k = put(i, j);
                if (k > ans) ans = k;
            }
        return ans;
    }
};
