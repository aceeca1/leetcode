#include <stack>
#include <cctype>
using namespace std;

class Solution {
public:
    bool isValidSerialization(string preorder) {
        const char *s = preorder.c_str();
        stack<int> st;
        while (*s) {
            if (*s == '#') {
                ++s;
                while (!st.empty() && !st.top()) {
                    st.pop();
                    if (st.empty() || !st.top()) return false;
                    st.pop();
                }
                st.emplace(0);
            } else {
                while (isdigit(*s)) ++s;
                st.emplace(1);
            }
            if (*s == ',') ++s;
        }
        if (st.empty() || st.top()) return false;
        st.pop();
        return st.empty();
    }
};
