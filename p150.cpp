#include <stack>
#include <cctype>
using namespace std;

class Solution {
public:
    int evalRPN(vector<string>& tokens) {
        stack<int> s;
        for (auto& i: tokens) if (isdigit(i.back()))
            s.emplace(stoi(i));
        else {
            int v1 = s.top();
            s.pop();
            int v2 = s.top();
            s.pop();
            switch (i[0]) {
                case '+': s.emplace(v2 + v1); break;
                case '-': s.emplace(v2 - v1); break;
                case '*': s.emplace(v2 * v1); break;
                case '/': s.emplace(v2 / v1);
            }
        }
        return s.top();
    }
};
