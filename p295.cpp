#include <cstdint>
using namespace std;

class MedianFinder {
    struct Node {
        Node *ch[2]{};
        int n = 0;
    };
    Node *root = new Node;

    int findKth(int k) {
        Node *p = root;
        uint32_t ans = 0;
        for (int i = 31; i >= 0; --i)
            if (!p->ch[0]) {
                ans += 1 << i;
                p = p->ch[1];
            } else if (k >= p->ch[0]->n) {
                k -= p->ch[0]->n;
                ans += 1 << i;
                p = p->ch[1];
            } else p = p->ch[0];
        return ans - 0x80000000;
    }

    void dispose(Node* p) {
        if (!p) return;
        dispose(p->ch[0]);
        dispose(p->ch[1]);
        delete p;
    }

public:
    ~MedianFinder() { dispose(root); }

    void addNum(int num) {
        uint32_t n = num + 0x80000000;
        Node *p = root;
        for (int i = 31; ; --i) {
            ++p->n;
            if (i < 0) break;
            auto& p1 = p->ch[(n >> i) & 1];
            if (!p1) p1 = new Node;
            p = p1;
        }
    }

    double findMedian() {
        int k = root->n >> 1;
        if (root->n & 1) return findKth(k);
        return 0.5 * (double(findKth(k - 1)) + findKth(k));
    }
};

int main() {
    MedianFinder s;
    s.addNum(1);
    s.findMedian();
    return 0;
}
