#include <cctype>
#include <string>
#include <vector>
#include <cstdlib>
using namespace std;

class Solution {
    vector<int> ways(const char* s, int z) {
        vector<int> ans;
        bool b = false;
        for (int i = 0; i < z; ++i) if (!isdigit(s[i])) {
            b = true;
            auto w1 = ways(s, i);
            auto w2 = ways(s + i + 1, z - i - 1);
            for (int j: w1)
                for (int k: w2)
                    switch (s[i]) {
                        case '+': ans.emplace_back(j + k); break;
                        case '-': ans.emplace_back(j - k); break;
                        case '*': ans.emplace_back(j * k); break;
                    }
        }
        if (!b) ans.emplace_back(atoi(s));
        return move(ans);
    }

public:
    vector<int> diffWaysToCompute(string input) {
        return ways(input.c_str(), input.size());
    }
};
