#include <algorithm>
using namespace std;

class Solution {
public:
    int lengthOfLIS(vector<int>& nums) {
        vector<int> a{0};
        for (int i: nums) {
            auto p = lower_bound(a.begin() + 1, a.end(), i);
            if (p == a.end()) a.emplace_back(i);
            else *p = i;
        }
        return a.size() - 1;
    }
};
