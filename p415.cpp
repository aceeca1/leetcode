#include <string>
#include <algorithm>
using namespace std;

class Solution {
public:
    string addStrings(string num1, string num2) {
        reverse(num1.begin(), num1.end());
        reverse(num2.begin(), num2.end());
        while (num1.size() < num2.size()) num1 += '0';
        while (num2.size() < num1.size()) num2 += '0';
        string ans;
        int carry = 0;
        for (int i = 0; i < num1.size(); ++i) {
            carry += (num1[i] - '0') + (num2[i] - '0');
            ans += (carry % 10) + '0';
            carry /= 10;
        }
        if (carry) ans += carry + '0';
        reverse(ans.begin(), ans.end());
        return ans;
    }
};
