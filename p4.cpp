class Solution {
public:
    int findKth(int *a, int az, int *b, int bz, int k) {
        if (az > bz) return findKth(b, bz, a, az, k);
        if (!az) return b[k];
        if (!k) return a[0] < b[0] ? a[0] : b[0];
        int pa = k >> 1;
        if (pa > az - 1) pa = az - 1;
        int pb = k - 1 - pa;
        if (a[pa] < b[pb]) return findKth(
            a + pa + 1, az - pa - 1, b, pb + 1, pb);
        if (a[pa] > b[pb]) return findKth(
            a, pa + 1, b + pb + 1, bz - pb - 1, pa);
        return a[pa];
    }

    double findMedianSortedArrays(vector<int>& nums1, vector<int>& nums2) {
        int z = nums1.size() + nums2.size();
        if (z & 1) return findKth(
            &nums1[0], nums1.size(), &nums2[0], nums2.size(), z >> 1);
        int a1 = findKth(
            &nums1[0], nums1.size(), &nums2[0], nums2.size(), z >> 1);
        int a2 = findKth(
            &nums1[0], nums1.size(), &nums2[0], nums2.size(), (z >> 1) - 1);
        return (a1 + a2) * 0.5;
    }
};
