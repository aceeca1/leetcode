#include <stack>
using namespace std;

class NestedIterator {
    stack<NestedInteger*> s;

    void push(vector<NestedInteger>& v) {
        for (int i = v.size() - 1; i >= 0; --i) s.emplace(&v[i]);
    }

    void splitHead() {
        while (!s.empty()) {
            auto sH = s.top();
            if (sH->isInteger()) break;
            s.pop();
            push(sH->getList());
        }
    }

public:
    NestedIterator(vector<NestedInteger> &nestedList) {
        push(nestedList);
        splitHead();
    }

    int next() {
        auto sH = s.top();
        int ans = sH->getInteger();
        s.pop();
        splitHead();
        return ans;
    }

    bool hasNext() { return !s.empty(); }
};
