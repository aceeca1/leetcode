class NumArray {
    vector<int> a;

public:
    NumArray(vector<int> &nums) {
        a = move(nums);
        int s = 0;
        for (int i = 0; i < a.size(); ++i) s = a[i] += s;
    }

    int sumRange(int i, int j) {
        if (i == 0) return a[j];
        return a[j] - a[i - 1];
    }
};
