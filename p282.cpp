#include <cstdint>
#include <vector>
#include <string>
using namespace std;

class Solution {
    string n;
    int m = 1;
    int64_t t;
    vector<string> ans;
    string pr;

    void put(int64_t v, int64_t term, int64_t expr) {
        if (m == n.size()) {
            if (expr + term * v == t) ans.emplace_back(pr);
            return;
        }
        if (v) {
            pr += n[m];
            put(v * 10 + (n[m++] - '0'), term, expr);
            --m;
            pr.pop_back();
        }
        pr += '*';
        pr += n[m];
        put(n[m++] - '0', term * v, expr);
        --m;
        pr.pop_back();
        pr.pop_back();
        for (char i: {'+', '-'}) {
            pr += i;
            pr += n[m];
            put(n[m++] - '0', i == '+' ? 1 : -1, expr + term * v);
            --m;
            pr.pop_back();
            pr.pop_back();
        }
    }

public:
    vector<string> addOperators(string num, int target) {
        if (num.empty()) return {};
        n = move(num);
        t = target;
        pr += n[0];
        put(n[0] - '0', 1, 0);
        return move(ans);
    }
};
