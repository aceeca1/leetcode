#include <vector>
#include <string>
using namespace std;

class Solution {
public:
    const char *a[10]{
        "", "", "abc", "def", "ghi",
        "jkl", "mno", "pqrs", "tuv", "wxyz"
    };

    string d, s;
    vector<string> v;

    void comb() {
        if (s.size() == d.size()) {
            v.emplace_back(s);
            return;
        }
        for (auto i = a[d[s.size()] - '0']; *i; ++i) {
            s.push_back(*i);
            comb();
            s.pop_back();
        }
    }

    vector<string> letterCombinations(string digits) {
        if (digits.empty()) return vector<string>();
        d = digits;
        comb();
        return v;
    }
};
