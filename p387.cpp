class Solution {
public:
    int firstUniqChar(string s) {
        int a[26]{};
        for (int i = 0; s[i]; ++i) ++a[s[i] - 'a'];
        for (int i = 0; s[i]; ++i)
            if (a[s[i] - 'a'] == 1) return i;
        return -1;
    }
};
