#include <vector>
#include <unordered_map>
#include <algorithm>
using namespace std;

class Solution {
public:
    vector<vector<int>> threeSum(vector<int>& nums) {
        unordered_map<int, int> a;
        for (auto ni: nums) ++a[ni];
        vector<vector<int>> ans;
        for (auto i = a.begin(); i != a.end(); ++i) {
            --i->second;
            auto j = i;
            if (!j->second) ++j;
            for (; j != a.end(); ++j) {
                auto iK = i->first;
                auto jK = j->first;
                if (iK > jK) swap(iK, jK);
                int kK = -(iK + jK);
                if (jK > kK) continue;
                --j->second;
                auto ak = a.find(kK);
                if (ak != a.end() && ak->second)
                    ans.emplace_back(vector<int>{iK, jK, kK});
                ++j->second;
            }
            ++i->second;
        }
        return ans;
    }
};
