class Solution {
public:
    ListNode* reverseKGroup(ListNode* head, int k) {
        ListNode h(0), *p = &h;
        h.next = head;
        for (;;) {
            auto q = p;
            for (int i = 0; i < k; ++i) {
                if (!q) break;
                q = q->next;
            }
            if (!q) break;
            q = p;
            auto q1 = q->next;
            for (int i = 0; i < k; ++i) {
                auto q2 = q1->next;
                q1->next = q;
                q = q1;
                q1 = q2;
            }
            auto p1 = p->next;
            p->next = q;
            p1->next = q1;
            p = p1;
        }
        return h.next;
    }
};
