class Solution {
public:
    int majorityElement(vector<int>& nums) {
        int v, c = 0;
        for (int i: nums)
            if (!c) { c = 1; v = i; }
            else if (i == v) ++c;
            else --c;
        return v;
    }
};
