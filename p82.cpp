class Solution {
public:
    ListNode* deleteDuplicates(ListNode* head) {
        if (!head) return nullptr;
        ListNode h(0), *p = &h, *q = head;
        int r = 1;
        for (auto i = head->next; i || r; i && (i = i->next)) {
            if (!i || i->val != q->val) {
                if (r == 1) p = p->next = q;
                else delete q;
                r = bool(i);
            } else { ++r; delete q; }
            q = i;
        }
        p->next = nullptr;
        return h.next;
    }
};
