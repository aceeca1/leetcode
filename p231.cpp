class Solution {
public:
    bool isPowerOfTwo(int n) {
        if (!n || n == 0x80000000) return false;
        return (n & -n) == n;
    }
};
