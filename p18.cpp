#include <vector>
#include <unordered_map>
#include <algorithm>
#include <utility>
using namespace std;

class Solution {
public:
    vector<vector<int>> fourSum(vector<int>& nums, int target) {
        unordered_map<int, int> a;
        for (int ni: nums) ++a[ni];
        unordered_map<int, vector<pair<int, int>>> b;
        for (auto i = a.begin(); i != a.end(); ++i) {
            auto j = i;
            if (j->second == 1) ++j;
            for (; j != a.end(); ++j) {
                if (!j->second) continue;
                int iK = i->first;
                int jK = j->first;
                if (iK > jK) swap(iK, jK);
                b[iK + jK].emplace_back(iK, jK);
            }
        }
        vector<vector<int>> ans;
        for (auto& bi: b) {
            int biK = bi.first;
            auto bj = b.find(target - biK);
            if (bj == b.end()) continue;
            for (auto& biVi: bi.second) {
                for (auto& bjVi: bj->second) {
                    if (biVi.second > bjVi.first) continue;
                    vector<int> c{
                        biVi.first, biVi.second, bjVi.first, bjVi.second
                    };
                    unordered_map<int, int> u;
                    for (int ci: c) ++u[ci];
                    if (any_of(u.begin(), u.end(),
                        [&](const pair<int, int>& ui){
                            return ui.second > a[ui.first];
                        })
                    ) continue;
                    ans.emplace_back(move(c));
                }
            }
        }
        return move(ans);
    }
};
