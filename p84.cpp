#include <stack>
#include <vector>
using namespace std;

class Solution {
public:
    int largestRectangleArea(vector<int>& heights) {
        stack<int> s;
        int ans = 0;
        heights.emplace_back();
        for (int i = 0; i < heights.size(); ++i) {
            while (!s.empty()) {
                int k = s.top();
                if (heights[k] <= heights[i]) break;
                s.pop();
                int k1 = -1;
                if (!s.empty()) k1 = s.top();
                int v = heights[k] * (i - k1 - 1);
                if (v > ans) ans = v;
            }
            s.emplace(i);
        }
        return ans;
    }
};
