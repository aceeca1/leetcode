#include <queue>
using namespace std;

class Solution {
    vector<vector<char>> g;
    queue<pair<int, int>> q;

    void add(int x, int y) {
        if (g[x][y] != '1') return;
        q.emplace(x, y);
        g[x][y] = '0';
    }

    void floodfill(int x, int y) {
        add(x, y);
        while (!q.empty()) {
            auto qH = q.front();
            q.pop();
            int x = qH.first, y = qH.second;
            if (x) add(x - 1, y);
            if (y) add(x, y - 1);
            if (x < g.size() - 1) add(x + 1, y);
            if (y < g[0].size() - 1) add(x, y + 1);
        }
    }

public:
    int numIslands(vector<vector<char>>& grid) {
        g = move(grid);
        if (g.empty() || g[0].empty()) return 0;
        int ans = 0;
        for (int i = 0; i < g.size(); ++i)
            for (int j = 0; j < g[0].size(); ++j) {
                if (g[i][j] != '1') continue;
                ++ans;
                floodfill(i, j);
            }
        return ans;
    }
};
