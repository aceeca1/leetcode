class Solution {
public:
    bool isInterleave(string s1, string s2, string s3) {
        if (s3.size() != s1.size() + s2.size()) return false;
        vector<vector<int>> a(s1.size() + 1, vector<int>(s2.size() + 1));
        for (int i = 0; i <= s1.size(); ++i)
            for (int j = 0; j <= s2.size(); ++j)
                a[i][j] = !i && !j ||
                    i && a[i - 1][j] && s1[i - 1] == s3[i + j - 1] ||
                    j && a[i][j - 1] && s2[j - 1] == s3[i + j - 1];
        return a.back().back();
    }
};
