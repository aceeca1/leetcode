class Solution {
public:
    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
        ListNode *s = new ListNode(0), *p = s;
        int c = 0;
        while (l1 || l2) {
            if (l1) c += l1->val;
            if (l2) c += l2->val;
            int k = c / 10;
            p = p->next = new ListNode(c - k * 10);
            c = k;
            if (l1) l1 = l1->next;
            if (l2) l2 = l2->next;
        }
        if (c) p->next = new ListNode(c);
        p = s->next;
        delete s;
        return p;
    }
};
