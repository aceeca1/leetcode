class Solution {
public:
    int coinChange(vector<int>& coins, int amount) {
        vector<int> a(amount + 1, 0x7ffffffe);
        a[0] = 0;
        for (int i: coins)
            for (int j = i; j <= amount; ++j) {
                int k = a[j - i] + 1;
                if (k < a[j]) a[j] = k;
            }
        return a[amount] == 0x7ffffffe ? -1 : a[amount];
    }
};
