class Solution {
public:
    vector<int> spiralOrder(vector<vector<int>>& matrix) {
        auto& a = matrix;
        vector<int> ans;
        if (a.empty() || a[0].empty()) return move(ans);
        int pS = 0, pT = a.size() - 1, qS = 0, qT = a[0].size() - 1;
        for (;;) {
            for (int i = qS; i <= qT; ++i) ans.emplace_back(a[pS][i]);
            if (++pS > pT) break;
            for (int i = pS; i <= pT; ++i) ans.emplace_back(a[i][qT]);
            if (qS > --qT) break;
            for (int i = qT; i >= qS; --i) ans.emplace_back(a[pT][i]);
            if (pS > --pT) break;
            for (int i = pT; i >= pS; --i) ans.emplace_back(a[i][qS]);
            if (++qS > qT) break;
        }
        return move(ans);
    }
};
