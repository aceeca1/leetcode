#include <algorithm>
using namespace std;

class Solution {
    void fix(vector<Interval>& a, int p) {
        int i = p + 1;
        for (; i < a.size(); ++i) {
            if (a[i].start > a[p].end) break;
            if (a[i].end > a[p].end) a[p].end = a[i].end;
        }
        a.erase(a.begin() + p + 1, a.begin() + i);
    }

public:
    vector<Interval> insert(vector<Interval>& intervals, Interval newInterval) {
        auto& a = intervals;
        auto& n = newInterval;
        int p = upper_bound(a.begin(), a.end(), n,
            [&](const Interval& i1, const Interval& i2) {
                return i1.start < i2.start;
            }) - a.begin();
        if (p && a[p - 1].end >= n.start) {
            if (n.end > a[p - 1].end) a[p - 1].end = n.end;
            fix(a, p - 1);
        } else if (p < a.size() && n.end >= a[p].start) {
            a[p].start = n.start;
            if (n.end > a[p].end) a[p].end = n.end;
            fix(a, p);
        } else a.emplace(a.begin() + p, n);
        return move(a);
    }
};
