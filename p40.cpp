#include <algorithm>
#include <utility>
#include <vector>
using namespace std;

class Solution {
    vector<int> c, t, pf;
    vector<vector<int>> r;
    int k = 0, n;

    void put() {
        if (!n) r.emplace_back(pf);
        if (k >= c.size() || n < c[k]) return;
        ++k, put(), --k;
        for (int i = 1; i <= t[k]; ++i) {
            if (n < c[k]) break;
            pf.emplace_back(c[k]);
            n -= c[k];
            ++k, put(), --k;
        }
        while (pf.size() && pf.back() == c[k]) {
            pf.pop_back();
            n += c[k];
        }
    }


public:
    vector<vector<int>> combinationSum2(vector<int>& candidates, int target) {
        sort(candidates.begin(), candidates.end());
        int a = candidates[0], rep = 1;
        for (int i = 1; i <= candidates.size(); ++i)
            if (i < candidates.size() && candidates[i] == a) ++rep;
            else {
                c.emplace_back(a);
                t.emplace_back(rep);
                a = candidates[i];
                rep = 1;
            }
        n = target;
        put();
        return move(r);
    }
};
