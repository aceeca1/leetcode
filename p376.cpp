class Solution {
public:
    int wiggleMaxLength(vector<int>& nums) {
        if (nums.size() <= 2) return nums.size();
        int p = 0, ans = 0;
        for (int i = 1; i < nums.size(); ++i)
            if (nums[i - 1] < nums[i]) {
                if (p == 0) p = 1;
                else if (p == -1) { ++ans; p = 1; }
            } else if (nums[i - 1] > nums[i]) {
                if (p == 0) p = -1;
                else if (p == 1) { ++ans; p = -1; }
            }
        return ans + 2;
    }
};
