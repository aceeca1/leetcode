#include <string>
#include <vector>
using namespace std;

class Solution {
public:
    string longestPalindrome(string s) {
        vector<int> a((s.size() << 1) + 1);
        int c = 0, ans = 0, i_ans;
        for (int i = 0; i < a.size(); ++i) {
            int t = c + a[c] - i;
            if (t > 0) {
                int t1 = a[c + c - i];
                if (t1 < t) t = t1;
            } else t = 0;
            for (;;) {
                int p = i - 1 - t, q = i + 1 + t;
                if (p < 0 || q >= a.size()) break;
                if (p & 1 && s[p >> 1] != s[q >> 1]) break;
                ++t;
            }
            a[i] = t;
            if (i + a[i] > c + a[c]) c = i;
            if (t > ans) { ans = t; i_ans = i; }
        }
        return s.substr((i_ans + 1 - ans) >> 1, ans);
    }
};
