#include <string>
using namespace std;

class Solution {
public:
    string largestNumber(vector<int>& nums) {
        vector<string> a;
        for (int i: nums) a.emplace_back(to_string(i));
        sort(a.begin(), a.end(), [&](const string& s1, const string& s2) {
            return s1 + s2 > s2 + s1;
        });
        string ans;
        for (auto& i: a) ans += i;
        int k = 0;
        while (k < ans.size() && ans[k] == '0') ++k;
        if (k == ans.size()) --k;
        ans.erase(0, k);
        return ans;
    }
};
