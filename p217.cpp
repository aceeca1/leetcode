#include <unordered_set>
using namespace std;

class Solution {
public:
    bool containsDuplicate(vector<int>& nums) {
        unordered_set<int> u;
        for (int i: nums) {
            auto p = u.emplace(i);
            if (!p.second) return true;
        }
        return false;
    }
};
