#include <string>
#include <deque>
using namespace std;

class Solution {
    string s;
    deque<int> q;

    void addQ(int no) {
        while (!q.empty() && s[q.back()] > s[no]) q.pop_back();
        q.emplace_back(no);
    }

    int removeMin() {
        int no = q.front();
        q.pop_front();
        return no;
    }

public:
    string removeKdigits(string num, int k) {
        s = move(num);
        for (int i = 0; i < k; ++i) addQ(i);
        string ans;
        for (int i = k; i < s.size(); ++i) {
            addQ(i);
            char c = s[removeMin()];
            if (!ans.empty() || c != '0') ans += c;
        }
        if (ans.empty()) ans += '0';
        return ans;
    }
};
