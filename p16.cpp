#include <vector>
#include <algorithm>
using namespace std;

class Solution {
public:
    int threeSumClosest(vector<int>& nums, int target) {
        sort(nums.begin(), nums.end());
        int d = 0x7fffffff, ans;
        int ni = 0;
        for (int i = 0; i < nums.size(); ++i) {
            swap(nums[i], ni);
            int nj = 0;
            int k = nums.size() - 1;
            for (int j = 1; j < nums.size(); ++j) {
                swap(nums[j], nj);
                if (j <= i) continue;
                int nk = target - ni - nj;
                if (k == j - 1) ++k;
                while (k >= 2 && nums[k] >= nk) --k;
                if (k >= 2) {
                    int nd = nk - nums[k];
                    if (nd < d) {d = nd; ans = target - nd;}
                }
                if (k < nums.size() - 1) {
                    int nd = nums[k + 1] - nk;
                    if (nd < d) {d = nd; ans = target + nd;}
                }
            }
            for (int j = 1; j < nums.size(); ++j) nums[j] = nums[j + 1];
            nums[nums.size() - 1] = nj;
        }
        return ans;
    }
};
