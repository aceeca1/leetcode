#include <string>
#include <cstdio>
#include <cctype>
using namespace std;

class Solution {
    string str;
    int k = 0;

    int parseInt() {
        while (k < str.size() && isblank(str[k])) ++k;
        int ans = 0;
        while (k < str.size() && isdigit(str[k]))
            ans = ans * 10 + (str[k++] - '0');
        return ans;
    }

    int parseTerm() {
        int ans = parseInt();
        for (;;) {
            while (k < str.size() && str[k] == ' ') ++k;
            char op = k < str.size() ? str[k] : 0;
            if (op != '*' && op != '/') return ans;
            ++k;
            int v = parseInt();
            switch (op) {
                case '*': ans *= v; break;
                case '/': ans /= v;
            }
        }
    }

    int parseExpr() {
        int ans = parseTerm();
        for (;;) {
            while (k < str.size() && str[k] == ' ') ++k;
            char op = k < str.size() ? str[k] : 0;
            if (op != '+' && op != '-') return ans;
            ++k;
            int v = parseTerm();
            switch (op) {
                case '+': ans += v; break;
                case '-': ans -= v;
            }
        }
    }

public:
    int calculate(string s) {
        str = move(s);
        return parseExpr();
    }
};
