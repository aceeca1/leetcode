#include <set>
#include <vector>
using namespace std;

class Solution {
    int ku, ans = 0x80000000;

    void maxSumSub(vector<int>& a) {
        set<int> s;
        for (int i = a.size() - 1; i >= 0; --i) {
            s.emplace(a[i]);
            int k = i ? a[i - 1] : 0;
            auto u = s.upper_bound(k + ku);
            if (u == s.begin()) continue;
            int t = *--u - k;
            if (t > ans) ans = t;
        }
    }

public:
    int maxSumSubmatrix(vector<vector<int>>& matrix, int k) {
        ku = k;
        for (int i = 0; i < matrix.size(); ++i)
            for (int j = 0; j < matrix[0].size(); ++j) {
                int v1 = i && j ? matrix[i - 1][j - 1] : 0;
                int v2 = i ? matrix[i - 1][j] : 0;
                int v3 = j ? matrix[i][j - 1] : 0;
                matrix[i][j] += v2 + v3 - v1;
            }
        if (matrix.size() < matrix[0].size()) {
            for (int i = 0; i < matrix.size(); ++i)
                for (int j = i + 1; j <= matrix.size(); ++j) {
                    vector<int> a(matrix[0].size());
                    for (int k = 0; k < matrix[0].size(); ++k)
                        a[k] = matrix[j - 1][k] - (i ? matrix[i - 1][k] : 0);
                    maxSumSub(a);
                }
        } else {
            for (int i = 0; i < matrix[0].size(); ++i)
                for (int j = i + 1; j <= matrix[0].size(); ++j) {
                    vector<int> a(matrix.size());
                    for (int k = 0; k < matrix.size(); ++k)
                        a[k] = matrix[k][j - 1] - (i ? matrix[k][i - 1] : 0);
                    maxSumSub(a);
                }
        }
        return ans;
    }
};
