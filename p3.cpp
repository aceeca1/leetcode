class Solution {
public:
    int lengthOfLongestSubstring(string s) {
        bool b[256]{};
        int j = 0, ans = 0;
        for (int i = 0; i < s.size(); ++i) {
            while (j < s.size()) {
                int sj = s[j];
                if (b[sj]) break;
                b[sj] = true;
                ++j;
            }
            int k = j - i;
            if (k > ans) ans = k;
            b[s[i]] = false;
        }
        return ans;
    }
};
