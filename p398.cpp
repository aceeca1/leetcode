#include <random>
using namespace std;

class Solution {
    vector<int> a;
    default_random_engine rand;

public:
    Solution(vector<int> nums) {
        a = move(nums);
    }

    int pick(int target) {
        int total = 0;
        for (int i = 0; i < a.size(); ++i) if (a[i] == target) ++total;
        total = uniform_int_distribution<int>(0, total - 1)(rand);
        for (int i = 0; i < a.size(); ++i) if (a[i] == target) {
            if (!total) return i;
            --total;
        }
        return -1;
    }
};
