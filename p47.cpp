#include <vector>
#include <algorithm>
using namespace std;

class Solution {
public:
    vector<vector<int>> permuteUnique(vector<int>& nums) {
        vector<vector<int>> ans;
        sort(nums.begin(), nums.end());
        for (;;) {
            ans.emplace_back(nums);
            if (!next_permutation(nums.begin(), nums.end())) break;
        }
        return ans;
    }
};
