class Solution {
    vector<vector<int>> ans;
    vector<int> pr;
    int s;

    void put(TreeNode* p) {
        if (!p) return;
        if (!p->left && !p->right) {
            if (p->val != s) return;
            ans.emplace_back(pr);
            ans.back().emplace_back(s);
            return;
        }
        pr.emplace_back(p->val);
        s -= p->val;
        put(p->left);
        put(p->right);
        s += p->val;
        pr.pop_back();
    }

public:
    vector<vector<int>> pathSum(TreeNode* root, int sum) {
        s = sum;
        put(root);
        return move(ans);
    }
};
