class Solution {
    vector<int> a, pr;
    vector<vector<int>> ans;
    int m = 0;

    void put() {
        if (m == a.size()) {
            ans.emplace_back(pr);
            return;
        }
        pr.emplace_back(a[m++]);
        put();
        pr.pop_back();
        put();
        --m;
    }

public:
    vector<vector<int>> subsets(vector<int>& nums) {
        a = move(nums);
        put();
        return move(ans);
    }
};
