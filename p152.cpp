#include <utility>
using namespace std;

class Solution {
public:
    int maxProduct(vector<int>& nums) {
        int ans = nums[0], min = ans, max = ans;
        for (int i = 1; i < nums.size(); ++i) {
            min *= nums[i];
            max *= nums[i];
            if (nums[i] < 0) swap(min, max);
            if (nums[i] < min) min = nums[i];
            if (nums[i] > max) max = nums[i];
            if (max > ans) ans = max;
        }
        return ans;
    }
};
