#include <algorithm>
#include <vector>
using namespace std;

class Solution {
public:
    bool searchMatrix(vector<vector<int>>& matrix, int target) {
        int k = upper_bound(matrix.begin(), matrix.end(), target,
            [&](int t, const vector<int>& v) {
                return t < v[0];
            }) - matrix.begin() - 1;
        if (k < 0) return false;
        auto k1 = lower_bound(matrix[k].begin(), matrix[k].end(), target);
        if (k1 == matrix[k].end()) return false;
        return *k1 == target;
    }
};
