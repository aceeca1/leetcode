#include <vector>
#include <cstdint>
using namespace std;

class Solution {
public:
    int splitArray(vector<int>& nums, int m) {
        vector<int64_t> pSum(nums.size() + 1);
        for (int i = 0; i < nums.size(); ++i)
            pSum[i + 1] = pSum[i] + nums[i];
        vector<int64_t> a0(pSum), a1(pSum);
        for (int i = 2; i <= m; ++i) {
            int k = 0;
            for (int j = 1; j <= nums.size(); ++j) {
                while (a1[k] < pSum[j] - pSum[k]) ++k;
                a0[j] = min(a1[k], pSum[j] - pSum[k - 1]);
            }
            swap(a0, a1);
        }
        return a1[nums.size()];
    }
};
