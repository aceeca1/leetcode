class Solution {
    struct Segment {
        int x1, x2, y;
    };

    bool test(const Segment& s1, const Segment& s2) {
        return (
            s2.x1 <= s1.y && s1.y <= s2.x2 ||
            s2.x2 <= s1.y && s1.y <= s2.x1
        ) && (
            s1.x1 <= s2.y && s2.y <= s1.x2 ||
            s1.x2 <= s2.y && s2.y <= s1.x1);
    }

public:
    bool isSelfCrossing(vector<int>& x) {
        Segment s0, s1{0, 0, 0}, s2, s3, s4, s5;
        for (int i = 0; i < x.size(); ++i) {
            s0.x1 = s1.y;
            s0.y = s1.x2;
            if (i & 2) s0.x2 = s0.x1 - x[i];
            else s0.x2 = s0.x1 + x[i];
            if (i >= 3 && test(s0, s3)) return true;
            if (i >= 4 && test(s0, s5)) return true;
            s5 = s4; s4 = s3; s3 = s2;
            s2 = s1; s1 = s0;
        }
        return false;
    }
};
