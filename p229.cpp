class Solution {
public:
    vector<int> majorityElement(vector<int>& nums) {
        int v1, c1 = 0, v2, c2 = 0;
        for (int i: nums)
            if (c1 && v1 == i) ++c1;
            else if (c2 && v2 == i) ++c2;
            else if (!c1) { c1 = 1; v1 = i; }
            else if (!c2) { c2 = 1; v2 = i; }
            else --c1, --c2;
        c1 = c2 = 0;
        for (int i: nums)
            if (i == v1) ++c1;
            else if (i == v2) ++c2;
        vector<int> ans;
        if (c1 > nums.size() / 3) ans.emplace_back(v1);
        if (c2 > nums.size() / 3) ans.emplace_back(v2);
        return move(ans);
    }
};
