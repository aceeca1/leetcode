class Solution {
public:
    int minDistance(string word1, string word2) {
        vector<int> a0(word2.size() + 1), a1(a0);
        for (int i = 0; i <= word1.size(); ++i) {
            for (int j = 0; j <= word2.size(); ++j) {
                 int k = 0x7fffffff;
                 if (!i && !j) k = 0;
                 if (i) {
                    int p = a1[j] + 1;
                    if (p < k) k = p;
                 }
                 if (j) {
                    int p = a0[j - 1] + 1;
                    if (p < k) k = p;
                 }
                 if (i && j) {
                    int p = a1[j - 1];
                    if (word1[i - 1] != word2[j - 1]) ++p;
                    if (p < k) k = p;
                 }
                 a0[j] = k;
            }
            swap(a0, a1);
        }
        return a1.back();
    }
};
