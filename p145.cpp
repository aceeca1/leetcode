class Solution {
    vector<int> ans;

    TreeNode* reverse(TreeNode* p, bool b = false) {
        TreeNode *q = nullptr;
        while (p) {
            if (b) ans.emplace_back(p->val);
            auto p1 = p->right;
            p->right = q;
            q = p;
            p = p1;
        }
        return q;
    }

public:
    vector<int> postorderTraversal(TreeNode* root) {
        TreeNode s(0);
        s.left = root;
        root = &s;
        while (root) {
            auto p = root->left;
            if (!p) {
                root = root->right;
                continue;
            }
            while (p->right && p->right != root) p = p->right;
            if (p->right) {
                p->right = nullptr;
                reverse(reverse(root->left), true);
                root = root->right;
                continue;
            }
            p->right = root;
            root = root->left;
        }
        return move(ans);
    }
};
