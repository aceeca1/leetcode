#include <utility>
using namespace std;

class Solution {
    pair<int, int> robP(TreeNode* p) {
        if (!p) return {0, 0};
        auto rob1 = robP(p->left), rob2 = robP(p->right);
        int v1 = rob1.second + rob2.second;
        int v2 = p->val + rob1.first + rob2.first;
        if (v1 > v2) v2 = v1;
        return {v1, v2};
    }

public:
    int rob(TreeNode* root) { return robP(root).second; }
};
