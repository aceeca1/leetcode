#include <stack>
using namespace std;

class Queue {
    stack<int> s1, s2;

public:
    void push(int x) {
        while (!s1.empty()) {
            int sH = s1.top();
            s1.pop();
            s2.emplace(sH);
        }
        s1.emplace(x);
        while (!s2.empty()) {
            int sH = s2.top();
            s2.pop();
            s1.emplace(sH);
        }
    }

    void pop(void) { s1.pop(); }

    int peek(void) { return s1.top(); }

    bool empty(void) { return s1.empty(); }
};
