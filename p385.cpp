class Solution {
    const char *p;

    NestedInteger parseNI() {
        if (p[0] != '[') {
            char *pN;
            int ret = strtol(p, &pN, 10);
            p = pN;
            return ret;
        }
        ++p; // '['
        NestedInteger ret;
        if (p[0] != ']') {
            ret.add(parseNI());
            while (p[0] == ',') {
                ++p; // ','
                ret.add(parseNI());
            }
        }
        ++p; // ']'
        return ret;
    }

public:
    NestedInteger deserialize(string s) {
        p = s.c_str();
        return parseNI();
    }
};
