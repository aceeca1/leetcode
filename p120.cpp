class Solution {
public:
    int minimumTotal(vector<vector<int>>& triangle) {
        auto a = triangle.back();
        for (int i = triangle.size() - 2; i >= 0; --i) {
            for (int j = 0; j < a.size() - 1; ++j) {
                if (a[j + 1] < a[j]) a[j] = a[j + 1];
                a[j] += triangle[i][j];
            }
            a.pop_back();
        }
        return a[0];
    }
};
