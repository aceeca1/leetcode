#include <cctype>
using namespace std;

class Solution {
public:
    bool isPalindrome(string s) {
        int p = 0, q = s.size() - 1;
        for (;;) {
            while (p < q && !isalnum(s[p])) ++p;
            while (p < q && !isalnum(s[q])) --q;
            if (p >= q) return true;
            char sp = tolower(s[p]), sq = tolower(s[q]);
            if (sp != sq) return false;
            ++p;
            --q;
        }
    }
};
