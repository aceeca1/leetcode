class Solution {
    ListNode *h;

    ListNode* sortList(int z) {
        if (z == 1) {
            auto h0 = h;
            h = h0->next;
            h0->next = nullptr;
            return h0;
        }
        int z0 = z >> 1;
        auto h1 = sortList(z0);
        auto h2 = sortList(z - z0);
        ListNode h0(0), *p = &h0;
        while (h1 && h2)
            if (h1->val < h2->val) { p = p->next = h1; h1 = h1->next; }
            else { p = p->next = h2; h2 = h2->next; }
        while (h1) { p = p->next = h1; h1 = h1->next; }
        while (h2) { p = p->next = h2; h2 = h2->next; }
        return h0.next;
    }

public:
    ListNode* sortList(ListNode* head) {
        if (!head) return nullptr;
        int z = 0;
        for (auto i = head; i; i = i->next) ++z;
        h = head;
        return sortList(z);
    }
};
