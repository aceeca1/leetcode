#include <vector>
#include <string>
using namespace std;

class Solution {
public:
    string convert(string s, int numRows) {
        if (numRows == 1) return s;
        vector<string> a(numRows);
        int j = 0, d = 1;
        for (int i = 0; i < s.size(); ++i) {
            a[j] += s[i];
            if (j == numRows - 1) d = -1;
            else if (j == 0) d = 1;
            j += d;
        }
        for (int i = 1; i < a.size(); ++i) a[0] += a[i];
        return a[0];
    }
};
