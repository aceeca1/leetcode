class Solution {
public:
    int nu, k = 0;
    vector<string> v;
    string s;

    void put() {
        if (s.size() == nu) {
            if (k == 0) v.emplace_back(s);
            return;
        }
        if (nu - s.size() < k) return;
        s += '(', ++k, put(), --k, s.pop_back();
        if (k) s += ')', --k, put(), ++k, s.pop_back();
    }

    vector<string> generateParenthesis(int n) {
        nu = n << 1;
        put();
        return v;
    }
};
