class Solution {
public:
    int superPow(int a, vector<int>& b) {
        int e = 0;
        for (int i = 0; i < b.size(); ++i) e = (e * 10 + b[i]) % 1140;
        if (e == 0) e = 1140;
        a %= 1337;
        int ans = 1;
        for (; e; e >>= 1) {
            if (e & 1) ans = ans * a % 1337;
            a = a * a % 1337;
        }
        return ans;
    }
};
