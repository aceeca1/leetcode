#include <vector>
#include <string>
#include <unordered_map>
using namespace std;

struct Edge {
    int t;
    double c;
};

struct Vertex {
    vector<Edge> e;
};

struct Graph {
    vector<Vertex> v;
};

template <class T>
struct GraphBuilder {
    Graph g;
    unordered_map<T, int> u;

    int addVertex() {
        g.v.emplace_back();
        return g.v.size() - 1;
    }

    int addVertex(const T& v) {
        auto r = u.emplace(v, g.v.size());
        if (r.second) return addVertex();
        return r.first->second;
    }
};

class Solution {
    GraphBuilder<string> gb;
    vector<int> rel;
    vector<double> rate;

    void visit(int no) {
        for (Edge& i: gb.g.v[no].e) if (rel[i.t] == -1) {
            rel[i.t] = rel[no];
            rate[i.t] = rate[no] * i.c;
            visit(i.t);
        }
    }

public:
    vector<double> calcEquation(
        vector<pair<string, string>> equations,
        vector<double>& values,
        vector<pair<string, string>> queries) {

        // Build Graph
        for (int i = 0; i < values.size(); ++i) {
            int v1 = gb.addVertex(equations[i].first);
            int v2 = gb.addVertex(equations[i].second);
            double c = values[i];
            gb.g.v[v1].e.emplace_back(Edge{v2, 1.0 / c});
            gb.g.v[v2].e.emplace_back(Edge{v1, c});
        }

        // DFS
        rel.resize(gb.g.v.size(), -1);
        rate.resize(gb.g.v.size());
        for (int i = 0; i < gb.g.v.size(); ++i) if (rel[i] == -1) {
            rel[i] = i;
            rate[i] = 1.0;
            visit(i);
        }

        // Build Answer
        vector<double> ret;
        for (auto& i: queries) {
            int v1 = gb.addVertex(i.first);
            int v2 = gb.addVertex(i.second);
            if (v1 >= rel.size() || v2 >= rel.size() ||
                v1 == v2 || rel[v1] != rel[v2]) ret.emplace_back(-1.0);
            else ret.emplace_back(rate[v1] / rate[v2]);
        }
        return ret;
    }
};
