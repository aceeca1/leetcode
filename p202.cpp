class Solution {
    void f(int& n) {
        int n1 = 0;
        for (char i :to_string(n)) {
            int k = i - '0';
            n1 += k * k;
        }
        n = n1;
    }

public:
    bool isHappy(int n) {
        int n1 = 0, n2 = 1;
        int x1 = n, x2 = n; f(x2);
        if (x1 == 1 || x2 == 1) return true;
        while (x1 != x2) {
            if (n1 + n1 < n2) { n1 = n2; x1 = x2; }
            ++n2; f(x2);
            if (x2 == 1) return true;
        }
        return false;
    }
};
