#include <vector>
using namespace std;

class Solution {
public:
    void merge(vector<int>& nums1, int m, vector<int>& nums2, int n) {
        nums1.resize(m + n);
        int z = m + n - 1, z1 = m - 1, z2 = n - 1;
        while (z2 >= 0) {
            if (z1 >= 0 && nums1[z1] > nums2[z2]) nums1[z--] = nums1[z1--];
            else nums1[z--] = nums2[z2--];
        }
    }
};
