class Solution {
public:
    bool hasCycle(ListNode *head) {
        if (!head || !head->next) return false;
        int n1 = 0, n2 = 1;
        auto x1 = head, x2 = head->next;
        while (x1 != x2) {
            if (n2 > n1 + n1) { n1 = n2; x1 = x2; }
            ++n2; x2 = x2->next;
            if (!x2) return false;
        }
        return true;
    }
};
