#include <string>
#include <vector>
using namespace std;

class Solution {
public:
    string multiply(string num1, string num2) {
        if (num1 == "0") return num1;
        if (num2 == "0") return num2;
        vector<int> v(num1.size() + num2.size());
        for (int i = 0; i < num1.size(); ++i)
            for (int j = 0; j < num2.size(); ++j) {
                int prod = (num1[i] - '0') * (num2[j] - '0');
                v[(num1.size() - 1 - i) + (num2.size() - 1 - j)] += prod;
            }
        int carry = 0;
        for (int i = 0; i < v.size(); ++i) {
            v[i] += carry;
            carry = v[i] / 10;
            v[i] %= 10;
        }
        int k = v.size() - 1;
        if (!v[k]) --k;
        string ans;
        for (int i = k; i >= 0; --i) ans += v[i] + '0';
        return ans;
    }
};
