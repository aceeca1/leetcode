#include <vector>
#include <climits>
#include <queue>
using namespace std;

struct Node {
    int x, y, v;

    bool operator<(const Node& that) {
        return v < that.v;
    }
};

class Solution {
    vector<vector<int>> a;
    vector<vector<bool>> visited;

    int floodfill(int x, int y, int oldv, int newv) {
        int ans = INT_MAX;
        queue<Node> q;
        q.emplace(Node{x, y, 0});
        while (!q.empty()) {
            int qHX = q.front().x;
            int qHY = q.front().y;
            q.pop();
            if (a[qHX][qHY] != oldv) continue;
            a[qHX][qHY] = newv;
            visited[qHX][qHY] = true;
            if (
                qHX == 0 || qHX == a.size() - 1 ||
                qHY == 0 || qHY == a[0].size() - 1
            ) ans = 0;
            auto go = [&](int x, int y) {
                if (a[x][y] == oldv) q.emplace(Node{x, y, 0});
                else if (a[x][y] != -1 && a[x][y] < ans) ans = a[x][y];
            };
            if (0 < qHX)                go(qHX - 1, qHY);
            if (qHX < a.size() - 1)     go(qHX + 1, qHY);
            if (0 < qHY)                go(qHX, qHY - 1);
            if (qHY < a[0].size() - 1)  go(qHX, qHY + 1);
        }
        return ans;
    }

public:
    int trapRainWater(vector<vector<int>>& heightMap) {
        a = move(heightMap);
        if (!a.size() || !a[0].size()) return 0;
        int ans = 0;
        vector<Node> v;
        for (int i = 0; i < a.size(); ++i)
            for (int j = 0; j < a[0].size(); ++j) {
                v.emplace_back(Node{i, j, a[i][j]});
                ans -= a[i][j];
            }
        sort(v.begin(), v.end());
        vector<bool> visitedI(a[0].size());
        visited = vector<vector<bool>>(a.size(), visitedI);
        for (int i = 0; i < v.size(); ++i) {
            if (visited[v[i].x][v[i].y]) continue;
            int lowest = floodfill(v[i].x, v[i].y, v[i].v, -1);
            if (lowest > v[i].v) v[i].v = lowest;
            floodfill(v[i].x, v[i].y, -1, v[i].v);
        }
        for (int i = 0; i < a.size(); ++i)
            for (int j = 0; j < a[0].size(); ++j) ans += a[i][j];
        return ans;
    }
};
