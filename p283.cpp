class Solution {
public:
    void moveZeroes(vector<int>& nums) {
        int z = 0;
        for (int i: nums) if (i) nums[z++] = i;
        while (z < nums.size()) nums[z++] = 0;
    }
};
