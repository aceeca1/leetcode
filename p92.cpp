class Solution {
public:
    ListNode* reverseBetween(ListNode* head, int m, int n) {
        ListNode h(0), *p = &h;
        h.next = head;
        for (int i = 1; i < m; ++i) p = p->next;
        auto h1 = p->next, t1 = h1, q = h1->next;
        for (int i = m; i < n; ++i) {
            auto q0 = q->next;
            q->next = h1;
            h1 = q;
            q = q0;
        }
        p->next = h1;
        t1->next = q;
        return h.next;
    }
};
