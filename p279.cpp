class Solution {
public:
    int numSquares(int n) {
        vector<int> a(n + 1, 0x7fffffff);
        a[0] = 0;
        for (int i = 1; i <= n; ++i) {
            for (int j = 1; j * j <= i; ++j) {
                int k = a[i - j * j] + 1;
                if (k < a[i]) a[i] = k;
            }
        }
        return a[n];
    }
};
