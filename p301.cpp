#include <string>
#include <vector>
#include <unordered_set>
using namespace std;

class Solution {
    string str;
    int k = 0;
    unordered_set<string> med, ans;
    string pr;

    void put() {
        int k0 = k, d = 0;
        while (d >= 0 && k < str.size()) {
            if (str[k] == '(') ++d;
            else if (str[k] == ')') --d;
            pr += str[k++];
        }
        if (d < 0) {
            for (int i = 0; i < pr.size(); ++i) {
                if (pr[i] != ')') continue;
                pr.erase(pr.begin() + i);
                put();
                pr.insert(pr.begin() + i, ')');
            }
        } else med.emplace(pr);
        while (k > k0) --k, pr.pop_back();
    }

    void putR() {
        int k0 = k, d = 0;
        while (d >= 0 && k >= 0) {
            if (str[k] == ')') ++d;
            else if (str[k] == '(') --d;
            pr += str[k--];
        }
        if (d < 0) {
            ++d;
            for (int i = 0; i < pr.size(); ++i) {
                if (pr[i] != '(') continue;
                pr.erase(pr.begin() + i);
                putR();
                pr.insert(pr.begin() + i, '(');
            }
            --d;
        } else ans.emplace(pr.rbegin(), pr.rend());
        while (k < k0) ++k, pr.pop_back();
    }

public:
    vector<string> removeInvalidParentheses(string s) {
        str = move(s);
        put();
        for (auto& s: med) {
            str = move(s);
            k = str.size() - 1;
            putR();
        }
        return vector<string>(ans.begin(), ans.end());
    }
};
