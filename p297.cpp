#include <queue>
using namespace std;

class Codec {
public:
    string serialize(TreeNode* root) {
        if (!root) return "";
        queue<TreeNode*> q;
        q.emplace(root);
        string ans = to_string(root->val);
        while (!q.empty()) {
            auto qH = q.front();
            q.pop();
            ans += ',';
            if (qH->left) {
                ans += to_string(qH->left->val);
                q.emplace(qH->left);
            }
            ans += ',';
            if (qH->right) {
                ans += to_string(qH->right->val);
                q.emplace(qH->right);
            }
        }
        while (ans.back() == ',') ans.pop_back();
        return move(ans);
    }

    int scanInt(const char*& s) {
        if (*s == '-') return -scanInt(++s);
        int ans = 0;
        while (isdigit(*s)) ans = ans * 10 + (*s++ - '0');
        return ans;
    }

    TreeNode* deserialize(string data) {
        if (data.empty()) return nullptr;
        const char *p = data.c_str();
        auto root = new TreeNode(0);
        root->val = scanInt(p);
        queue<TreeNode*> q;
        q.emplace(root);
        while (!q.empty()) {
            auto qH = q.front();
            q.pop();
            if (!*++p) break;
            if (*p != ',') {
                qH->left = new TreeNode(scanInt(p));
                q.emplace(qH->left);
            }
            if (!*++p) break;
            if (*p != ',') {
                qH->right = new TreeNode(scanInt(p));
                q.emplace(qH->right);
            }
        }
        return root;
    }
};
