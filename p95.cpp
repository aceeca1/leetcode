class Solution {
    vector<TreeNode*> generateTrees(int s, int t) {
        if (s > t) return vector<TreeNode*>{nullptr};
        vector<TreeNode*> ans;
        for (int i = s; i <= t; ++i) {
            auto v1 = generateTrees(s, i - 1);
            auto v2 = generateTrees(i + 1, t);
            for (auto v1i: v1)
                for (auto v2i: v2) {
                    auto p = new TreeNode(i);
                    p->left = v1i;
                    p->right = v2i;
                    ans.emplace_back(p);
                }
        }
        return move(ans);
    }

public:
    vector<TreeNode*> generateTrees(int n) {
        if (!n) return vector<TreeNode*>();
        return generateTrees(1, n);
    }
};
