#include <algorithm>
#include <vector>
using namespace std;

class Solution {
public:
    vector<int> largestDivisibleSubset(vector<int>& nums) {
        if (nums.empty()) return {};
        sort(nums.begin(), nums.end());
        vector<int> len(nums.size()), prev(nums.size());
        int ans = 0, p;
        for (int i = 0; i < nums.size(); ++i) {
            len[i] = 1;
            prev[i] = -1;
            for (int j = 0; j < i; ++j) if (!(nums[i] % nums[j])) {
                int k = len[j] + 1;
                if (k <= len[i]) continue;
                len[i] = k;
                prev[i] = j;
            }
            if (len[i] > ans) { ans = len[i]; p = i; }
        }
        vector<int> a;
        for (; p != -1; p = prev[p]) a.emplace_back(nums[p]);
        reverse(a.begin(), a.end());
        return move(a);
    }
};
