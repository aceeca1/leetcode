#include <vector>
#include <algorithm>
using namespace std;

class Solution {
    vector<int> a, pr;
    vector<vector<int>> ans;
    int m = 0;

    void put() {
        if (m == a.size()) {
            ans.emplace_back(pr);
            return;
        }
        pr.emplace_back(a[m++]);
        while (m < a.size() && a[m] == pr.back())
            pr.emplace_back(a[m++]);
        put();
        while (pr.size() >= 2 && pr.back() == pr[pr.size() - 2]) {
            pr.pop_back();
            --m;
        }
        pr.pop_back();
        put();
        --m;
    }

public:
    vector<vector<int>> subsetsWithDup(vector<int>& nums) {
        a = move(nums);
        sort(a.begin(), a.end());
        put();
        return move(ans);
    }
};
