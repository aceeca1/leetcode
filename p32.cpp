#include <stack>
using namespace std;

class Solution {
public:
    int longestValidParentheses(string s) {
        stack<int> st;
        st.emplace(-1);
        int ans = 0;
        for (int i = 0; i < s.size(); ++i) {
            if (s[i] == '(') {
                st.emplace(i);
                continue;
            }
            st.pop();
            if (st.empty()) st.push(i);
            else {
                int a = i - st.top();
                if (a > ans) ans = a;
            }
        }
        return ans;
    }
};
