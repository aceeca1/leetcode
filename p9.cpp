#include <cstdint>
using namespace std;

class Solution {
public:
    bool isPalindrome(int x) {
        if (x < 0) return false;
        int64_t r = 0, k = x;
        while (k) {
            r = r * 10 + k % 10;
            k /= 10;
        }
        return r == x;
    }
};
