class Solution {
public:
    int singleNumber(vector<int>& nums) {
        int ans1 = 0, ans2 = 0;
        for (int i: nums) {
            ans2 ^= ans1 & i;
            ans1 ^= i;
            int b = ans1 & ans2;
            ans1 ^= b;
            ans2 ^= b;
        }
        return ans1;
    }
};
