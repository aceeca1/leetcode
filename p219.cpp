#include <unordered_set>
using namespace std;

class Solution {
public:
    bool containsNearbyDuplicate(vector<int>& nums, int k) {
        unordered_set<int> u;
        for (int i = 0; i < nums.size(); ++i) {
            if (i > k) u.erase(nums[i - k - 1]);
            auto p = u.emplace(nums[i]);
            if (!p.second) return true;
        }
        return false;
    }
};
