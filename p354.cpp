#include <algorithm>
#include <vector>
#include <utility>
using namespace std;

class Solution {
public:
    int maxEnvelopes(vector<pair<int, int>>& envelopes) {
        sort(envelopes.begin(), envelopes.end(),
            [&](const pair<int, int>& p1, const pair<int, int>& p2) {
                return p1.first < p2.first ||
                    p1.first == p2.first && p1.second > p2.second;
            });
        vector<int> a(envelopes.size());
        for (int i = 0; i < envelopes.size(); ++i)
            a[i] = envelopes[i].second;
        vector<int> b = a;
        sort(b.begin(), b.end());
        b.resize(unique(b.begin(), b.end()) - b.begin());
        for (int i = 0; i < a.size(); ++i)
            a[i] = lower_bound(b.begin(), b.end(), a[i]) - b.begin();
        int m = 1 << (ilogb(b.size() + 2) + 1);
        vector<int> c(m + m);
        for (int i: a) {
            int p = 0;
            for (int k = i + m; k > 1; k >>= 1) {
                if (!(k & 1)) continue;
                int k1 = c[k - 1];
                if (k1 > p) p = k1;
            }
            ++p;
            for (int k = i + m; k; k >>= 1)
                if (p > c[k]) c[k] = p;
        }
        return c[1];
    }
};
