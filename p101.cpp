#include <stack>
using namespace std;

class Solution {
public:
    bool isSymmetric(TreeNode* root) {
        if (!root) return true;
        stack<pair<TreeNode*, TreeNode*>> s;
        s.emplace(root, root);
        while (!s.empty()) {
            auto p = s.top();
            s.pop();
            auto p1 = p.first, p2 = p.second;
            if (p1->val != p2->val) return false;
            if (p1->left)
                if (p2->right) s.emplace(p1->left, p2->right);
                else return false;
            else if (p2->right) return false;
            if (p1->right)
                if (p2->left) s.emplace(p1->right, p2->left);
                else return false;
            else if (p2->left) return false;
        }
        return true;
    }
};
