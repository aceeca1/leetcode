class Solution {
public:
    ListNode* removeNthFromEnd(ListNode* head, int n) {
        ListNode h(0), *p = &h, *q = p;
        h.next = head;
        for (int i = 0; i <= n; ++i) q = q->next;
        while (q) {
            p = p->next;
            q = q->next;
        }
        q = p->next;
        p->next = q->next;
        delete q;
        return h.next;
    }
};
