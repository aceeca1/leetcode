class Solution {
public:
    int countPrimes(int n) {
        vector<int> p(n);
        int u = 0, ans = 0;
        for (int i = 2; i < n; ++i) {
            int v = p[i];
            if (!v) {
                u = p[u] = v = i;
                ++ans;
            }
            for (int w = 2; i * w < n; w = p[w]) {
                p[i * w] = w;
                if (w >= v) break;
            }
        }
        return ans;
    }
};
