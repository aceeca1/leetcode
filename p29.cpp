#include <cstdint>
using namespace std;

class Solution {
public:
    int divide(int dividend, int divisor) {
        if (dividend == 0x80000000 && divisor == -1) return 0x7fffffff;
        int64_t a = dividend, b = divisor, c = 1;
        if (a < 0) {a = -a; c = -c;}
        if (b < 0) {b = -b; c = -c;}
        int64_t ans = 0, k = 1, kd = b;
        while (kd <= a) {k <<= 1; kd <<= 1;}
        while (k >>= 1) {
            kd >>= 1;
            if (a >= kd) {a -= kd; ans += k;}
        }
        return c == 1 ? ans : -ans;
    }
};
