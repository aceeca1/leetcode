#include <queue>
#include <algorithm>
#include <cstdint>
using namespace std;

class Solution {
public:
    int nthSuperUglyNumber(int n, vector<int>& primes) {
        vector<queue<int64_t>> a(primes.size());
        for (int i = 0; i < primes.size(); ++i) a[i].emplace(primes[i]);
        int64_t k = 1;
        while (--n) {
            k = 0x7fffffff;
            int i_min;
            for (int i = 0; i < primes.size(); ++i) {
                if (a[i].front() >= k) continue;
                k = a[i].front();
                i_min = i;
            }
            a[i_min].pop();
            for (int i = 0; i < primes.size(); ++i) {
                a[i].emplace(k * primes[i]);
                if (!(k % primes[i])) break;
            }
        }
        return k;
    }
};
