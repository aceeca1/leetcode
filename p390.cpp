class Solution {
public:
    int lastRemaining(int n) {
        int a = 1, b = 0;
        for (;;) {
            int c = (n - b) / a;
            if (c == 1) break;
            a <<= 1;
            c = (n - b) / a;
            if (c == 1) break;
            if (!(c & 1)) b -= a;
            a <<= 1;
        }
        return a + b;
    }
};
