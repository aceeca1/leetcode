#include <algorithm>
#include <vector>
using namespace std;

class Solution {
public:
    int removeDuplicates(vector<int>& nums) {
        if (nums.empty()) return 0;
        int c = nums[0], j = 1;
        for (int i = 1; i < nums.size(); ++i) {
            if (nums[i] != c) c = nums[j++] = nums[i];
        }
        return j;
    }
};
