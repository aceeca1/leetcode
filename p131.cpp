class Solution {
    string str;
    vector<vector<int>> pa;
    vector<vector<string>> ans;
    vector<string> pr;
    int m = 0;

    void put() {
        if (m == str.size()) {
            ans.emplace_back(pr);
            return;
        }
        int m0 = m;
        for (++m; m <= str.size(); ++m) {
            if (!pa[m0][m]) continue;
            pr.emplace_back(str.substr(m0, m - m0));
            put();
            pr.pop_back();
        }
        m = m0;
    }

public:
    vector<vector<string>> partition(string s) {
        str = move(s);
        pa.resize(str.size(), vector<int>(str.size() + 1));
        for (int i = str.size() - 1; i >= 0; --i)
            for (int j = i; j <= str.size(); ++j)
                pa[i][j] = i == j || i + 1 == j ||
                    pa[i + 1][j - 1] && str[i] == str[j - 1];
        put();
        return move(ans);
    }
};
