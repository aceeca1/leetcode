class Solution {
public:
    vector<int> preorderTraversal(TreeNode* root) {
        vector<int> ans;
        while (root) {
            auto p = root->left;
            if (!p) {
                ans.emplace_back(root->val);
                root = root->right;
                continue;
            }
            while (p->right && p->right != root) p = p->right;
            if (p->right) {
                p->right = nullptr;
                root = root->right;
                continue;
            }
            p->right = root;
            ans.emplace_back(root->val);
            root = root->left;
        }
        return ans;
    }
};
