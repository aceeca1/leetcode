class Solution {
    int hash(string s) {
        int ans = 0;
        for (int i = 0; i < s.size(); ++i) ans = ans * 0xdeadbeef + s[i];
        return ans;
    }

public:
    bool wordBreak(string s, unordered_set<string>& wordDict) {
        vector<vector<int>> e(s.size() + 1);
        unordered_set<int> w;
        for (auto& i: wordDict) w.emplace(hash(i));
        for (int i = 0; i < s.size(); ++i) {
            int h = 0;
            for (int j = i + 1; j <= s.size(); ++j) {
                h = h * 0xdeadbeef + s[j - 1];
                if (w.count(h)) e[i].emplace_back(j);
            }
        }
        vector<int> a(s.size() + 1);
        a[s.size()] = true;
        for (int i = s.size() - 1; i >= 0; --i)
            for (auto j: e[i]) a[i] |= a[j];
        return a[0];
    }
};
