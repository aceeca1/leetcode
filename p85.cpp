#include <vector>
#include <utility>
using namespace std;

class Solution {
public:
    int maximalRectangle(vector<vector<char>>& matrix) {
        int m = matrix.size();
        if (!m) return 0;
        int n = matrix[0].size();
        if (!n) return 0;
        int ans = 0;
        vector<int> u(n), u1(n), l(n), l1(n), r(n), r1(n);
        for (int i = 0; i < m; ++i) {
            for (int j = 0; j < n; ++j) {
                if (matrix[i][j] == '0') u[j] = 0;
                else if (i) u[j] = u1[j] + 1;
                else u[j] = 1;
                if (matrix[i][j] == '0') l[j] = 0;
                else if (j) l[j] = l[j - 1] + 1;
                else l[j] = 1;
            }
            for (int j = n - 1; j >= 0; --j)
                if (matrix[i][j] == '0') r[j] = 0;
                else if (j < n - 1) r[j] = r[j + 1] + 1;
                else r[j] = 1;
            for (int j = 0; j < n; ++j) {
                if (!u[j]) continue;
                if (u[j] > 1) {
                    if (l1[j] < l[j]) l[j] = l1[j];
                    if (r1[j] < r[j]) r[j] = r1[j];
                }
                int v = (l[j] + r[j] - 1) * u[j];
                if (v > ans) ans = v;
            }
            swap(u, u1);
            swap(l, l1);
            swap(r, r1);
        }
        return ans;
    }
};
