class Solution {
public:
    int romanToInt(string s) {
        int ans = 0;
        const char *p = s.c_str();
        while (p[0] == 'M') {++p; ans += 1000;}
        if (p[0] == 'C' && p[1] == 'M') {p += 2; ans += 900;}
        else if (p[0] == 'D') {++p; ans += 500;}
        else if (p[0] == 'C' && p[1] == 'D') {p += 2; ans += 400;}
        while (p[0] == 'C') {++p; ans += 100;}
        if (p[0] == 'X' && p[1] == 'C') {p += 2; ans += 90;}
        else if (p[0] == 'L') {++p; ans += 50;}
        else if (p[0] == 'X' && p[1] == 'L') {p += 2; ans += 40;}
        while (p[0] == 'X') {++p; ans += 10;}
        if (p[0] == 'I' && p[1] == 'X') {p += 2; ans += 9;}
        else if (p[0] == 'V') {++p; ans += 5;}
        else if (p[0] == 'I' && p[1] == 'V') {++p; ans += 4;}
        while (p[0] == 'I') {++p; ++ans;}
        return ans;
    }
};
