#include <vector>
#include <string>
#include <unordered_map>
#include <algorithm>
#include <stack>
using namespace std;

class Solution {
    vector<string> iToS;
    unordered_map<string, int> sToI;
    vector<vector<int>> e;

    int addV(string s) {
        auto ss = sToI.emplace(s, iToS.size());
        if (!ss.second) return ss.first->second;
        iToS.emplace_back(move(s));
        e.emplace_back();
        return iToS.size() - 1;
    }

public:
    vector<string> findItinerary(vector<pair<string, string>> tickets) {
        for (auto i: tickets) {
            int v1 = addV(i.first);
            int v2 = addV(i.second);
            e[v1].emplace_back(v2);
        }
        for (auto& ei: e) sort(ei.begin(), ei.end(),
            [&](int e1, int e2) { return iToS[e1] < iToS[e2]; });
        vector<int> eS(e.size());
        vector<string> ans;
        stack<int> s;
        s.emplace(sToI["JFK"]);
        while (!s.empty()) {
            int sH = s.top();
            if (eS[sH] < e[sH].size()) s.emplace(e[sH][eS[sH]++]);
            else { s.pop(); ans.emplace_back(iToS[sH]); }
        }
        reverse(ans.begin(), ans.end());
        return ans;
    }
};
