#include <cstdint>
using namespace std;

class Solution {
public:
    int myAtoi(string str) {
        const char *p = str.c_str();
        while (*p == ' ' || *p == '\t') ++p;
        bool negative = false;
        switch (*p) {
            case '+': ++p; break;
            case '-': ++p; negative = true;
        }
        if (*p < '0' || '9' < *p) return 0;
        int64_t v = *p++ - '0', m = negative ? 0x80000000ll : 0x7fffffffll;
        while ('0' <= *p && *p <= '9') {
            v = v * 10 + (*p++ - '0');
            if (v > m) return m;
        }
        return negative ? -v : v;
    }
};
