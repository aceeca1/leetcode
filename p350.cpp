#include <unordered_map>
#include <vector>
using namespace std;

class Solution {
public:
    vector<int> intersect(vector<int>& nums1, vector<int>& nums2) {
        unordered_map<int, int> u1;
        for (int i: nums1) ++u1[i];
        nums1.clear();
        for (int i: nums2) {
            auto u1i = u1.find(i);
            if (u1i == u1.end()) continue;
            if (--u1i->second >= 0) nums1.emplace_back(i);
        }
        return move(nums1);
    }
};
