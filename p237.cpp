class Solution {
public:
    void deleteNode(ListNode* node) {
        auto n1 = node->next;
        node->val = n1->val;
        node->next = n1->next;
        delete n1;
    }
};
