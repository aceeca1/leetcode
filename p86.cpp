class Solution {
public:
    ListNode* partition(ListNode* head, int x) {
        ListNode h1(0), h2(0), *p1 = &h1, *p2 = &h2;
        for (auto p = head; p; p = p->next)
            if (p->val < x) p1 = p1->next = p;
            else p2 = p2->next = p;
        p1->next = h2.next;
        p2->next = nullptr;
        return h1.next;
    }
};
