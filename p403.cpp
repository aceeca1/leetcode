#include <vector>
#include <unordered_map>
#include <unordered_set>
using namespace std;

struct PIIHash {
    size_t operator()(const pair<int, int>& a) const {
        return (size_t)a.first * 0xdeadbeef + a.second;
    }
};

class Solution {
    vector<int> s;
    unordered_map<int, int> u;
    unordered_set<pair<int, int>, PIIHash> b;

    bool put(int c, int p) {
        auto w = b.emplace(c, p);
        if (!w.second) return false;
        if (c == (int)s.size() - 1) return true;
        int k = s[c] - s[p];
        for (int k1 = k - 1; k1 <= k + 1; ++k1) {
            auto it = u.find(s[c] + k1);
            if (it == u.end()) continue;
            if (it->second == c) continue;
            if (put(it->second, c)) return true;
        }
        return false;
    }

public:
    bool canCross(vector<int>& stones) {
        if (stones[1] != 1) return false;
        s = move(stones);
        for (int i = 0; i < (int)s.size(); ++i) u[s[i]] = i;
        return put(1, 0);
    }
};
