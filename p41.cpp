#include <vector>
#include <cmath>
using namespace std;

class Solution {
public:
    int firstMissingPositive(vector<int>& nums) {
        int n = nums.size();
        for (int i = 0; i < n; ++i)
            if (nums[i] <= 0) nums[i] = n + 1;
        for (int i = 0; i < n; ++i) {
            int k = abs(nums[i]) - 1;
            if (0 <= k && k < n && nums[k] > 0) nums[k] = -nums[k];
        }
        for (int i = 0; i < n; ++i)
            if (nums[i] > 0) return i + 1;
        return n + 1;
    }
};
