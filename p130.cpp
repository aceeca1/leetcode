#include <queue>
using namespace std;

class Solution {
    vector<vector<char>> b;
    queue<pair<int, int>> q;

    void add(int x, int y) {
        if (b[x][y] != 'O') return;
        q.emplace(x, y);
        b[x][y] = 'M';
    }

public:
    void solve(vector<vector<char>>& board) {
        b = move(board);
        if (b.empty() || b[0].empty()) return;
        for (int i = 0; i < b.size(); ++i) {
            add(i, 0);
            add(i, b[0].size() - 1);
        }
        for (int i = 0; i < b[0].size(); ++i) {
            add(0, i);
            add(b.size() - 1, i);
        }
        while (!q.empty()) {
            auto qH = q.front();
            q.pop();
            int x = qH.first, y = qH.second;
            if (x) add(x - 1, y);
            if (y) add(x, y - 1);
            if (x < b.size() - 1) add(x + 1, y);
            if (y < b[0].size() - 1) add(x, y + 1);
        }
        for (int i = 0; i < b.size(); ++i)
            for (int j = 0; j < b[0].size(); ++j)
                switch (b[i][j]) {
                    case 'O': b[i][j] = 'X'; break;
                    case 'M': b[i][j] = 'O';
                }
        board = move(b);
    }
};
