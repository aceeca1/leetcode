class Solution {
public:
    ListNode* deleteDuplicates(ListNode* head) {
        if (!head) return nullptr;
        ListNode *p = head;
        int c = head->val, r = 1;
        for (auto i = head->next; i;)
            if (i->val != c) {
                c = i->val;
                r = 1;
                p = p->next = i;
                i = i->next;
            } else {
                ++r;
                auto i0 = i;
                i = i->next;
                delete i0;
            }
        p->next = nullptr;
        return head;
    }
};
