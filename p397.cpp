#include <climits>
using namespace std;

class Solution {
    void repl(int& n1, int& n2) {
        if (n1 == 1) {
            n1 = 0; --n2;
            return;
        }
        int k1 = n1 >> 1;
        int k2 = (n2 + 1) >> 1;
        repl(k1, k2);
        if (n1 & 1) n1 = min(k1, k2) + 2;
        else n1 = k1 + 1;
        if (n2 & 1) n2 = min(k1, k2) + 2;
        else n2 = k2 + 1;
    }

public:
    int integerReplacement(int n) {
        if (n == INT_MAX) return 32;
        int n1 = n, n2 = n;
        repl(n1, n2);
        return n1;
    }
};
