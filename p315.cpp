#include <algorithm>
#include <vector>
#include <cmath>
using namespace std;

class Solution {
public:
    vector<int> countSmaller(vector<int>& nums) {
        vector<int> a = nums;
        sort(a.begin(), a.end());
        a.resize(unique(a.begin(), a.end()) - a.begin());
        for (int i = 0; i < nums.size(); ++i)
            nums[i] = lower_bound(a.begin(), a.end(), nums[i]) - a.begin();
        int m = 1 << (ilogb(a.size() + 2) + 1);
        a.resize(m + m);
        for (int i = 0; i < a.size(); ++i) a[i] = 0;
        for (int i = nums.size() - 1; i >= 0; --i) {
            int ans = 0;
            for (int k = nums[i] + m; k > 1; k >>= 1) {
                if (k & 1) ans += a[k - 1];
                ++a[k];
            }
            nums[i] = ans;
        }
        return move(nums);
    }
};
