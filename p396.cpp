#include <vector>
#include <climits>
using namespace std;

class Solution {
public:
    int maxRotateFunction(vector<int>& a) {
        int s = 0, w = 0;
        for (int i = 0; i < a.size(); ++i) {
            s += a[i];
            w += a[i] * i;
        }
        int ans = w;
        for (int i = a.size() - 1; i >= 0; --i) {
            w = w + s - a.size() * a[i];
            if (w > ans) ans = w;
        }
        return ans;
    }
};
