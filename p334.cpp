class Solution {
public:
    bool increasingTriplet(vector<int>& nums) {
        int a1 = 0x7fffffff, a2 = 0x7fffffff;
        for (int i: nums) if (i < a1) a1 = i;
        else if (a1 < i && i < a2) a2 = i;
        else if (a2 < i) return true;
        return false;
    }
};
