#include <vector>
#include <utility>
#include <algorithm>
using namespace std;

class Solution {
public:
    vector<pair<int, int>> reconstructQueue(vector<pair<int, int>>& people) {
        vector<pair<int, int>> ans;
        sort(people.begin(), people.end(), []
            (const pair<int, int>& p1, const pair<int, int>& p2) {
            return p1.first > p2.first ||
                p1.first == p2.first && p1.second < p2.second;
        });
        for (int i = 0; i < people.size(); ++i) {
            ans.emplace(ans.begin() + people[i].second, move(people[i]));
        }
        return ans;
    }
};
