#include <vector>
#include <unordered_map>
using namespace std;

class Solution {
public:
    vector<vector<string>> groupAnagrams(vector<string>& strs) {
        unordered_map<size_t, vector<string>> m;
        for (auto& s: strs) {
            string t{"."};
            size_t k = 0;
            for (auto si: s) {
                t[0] = si;
                k += hash<string>()(t);
            }
            m[k].emplace_back(move(s));
        }
        vector<vector<string>> ans;
        for (auto mi: m)
            ans.emplace_back(move(mi.second));
        return move(ans);
    }
};
