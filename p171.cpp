class Solution {
public:
    int titleToNumber(string s) {
        int ans = 0;
        for (char si: s)
            ans = ans * 26 + (si - 'A' + 1);
        return ans;
    }
};
