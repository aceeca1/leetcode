class Solution {
public:
    bool isPowerOfFour(int num) {
        return num > 0 && num == (num & -num & 0x55555555);
    }
};
