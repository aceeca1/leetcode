#include <string>
#include <cstdint>
#include <algorithm>
using namespace std;

class Solution {
public:
    int countDigitOne(int n) {
        if (n <= 0) return 0;
        auto s = to_string(n + 1);
        int64_t ans = 0, p = 0, q = 1, r = count(s.begin(), s.end(), '1');
        while (!s.empty()) {
            int k = s.back();
            if (k > '1') ans += q;
            if (k == '1') --r;
            ans += int64_t(k - '0') * (q * r + p * (q / 10));
            ++p;
            q *= 10;
            s.pop_back();
        }
        return ans;
    }
};
