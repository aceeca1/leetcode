class Solution {
public:
    void connect(TreeLinkNode *root) {
        while (root) {
            TreeLinkNode h(0), *t = &h;
            for (auto i = root; i; i = i->next) {
                if (i->left) t = t->next = i->left;
                if (i->right) t = t->next = i->right;
            }
            root = h.next;
        }
    }
};
