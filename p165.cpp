#include <cctype>
#include <string>
using namespace std;

class Solution {
    int scanInt(const char*& s) {
        int ans = 0;
        while (isdigit(*s)) ans = ans * 10 + (*s++ - '0');
        return ans;
    }

public:
    int compareVersion(string version1, string version2) {
        const char *c1 = version1.c_str(), *c2 = version2.c_str();
        for (;;) {
            if (!*c1 && !*c2) return 0;
            int n1 = scanInt(c1), n2 = scanInt(c2);
            if (n1 < n2) return -1;
            if (n1 > n2) return 1;
            if (*c1 == '.') ++c1;
            if (*c2 == '.') ++c2;
        }
    }
};
