#include <vector>
#include <algorithm>
#include <bitset>
using namespace std;

class Solution {
public:
    vector<vector<int>> e{81};

    void fix(vector<int>& a, int k) {
        for (int eki: e[k]) {
            if (a[eki] == a[k]) a[eki] = 0;
            if (!a[eki]) return;
            if ((a[eki] & -a[eki]) == a[eki]) continue;
            a[eki] &= ~a[k];
            if ((a[eki] & -a[eki]) == a[eki]) fix(a, eki);
        }
    }

    bool solve(vector<int>& a) {
        int k = -1, bck = 0x7fffffff;
        for (int i = 0; i < 81; ++i) {
            int bc = bitset<32>(a[i]).count();
            if (!bc) return false;
            if (bc == 1) continue;
            if (bc < bck) {
                bck = bc;
                k = i;
            }
        }
        if (k == -1) return true;
        for (;;) {
            int lb = a[k] & -a[k];
            if (lb == a[k]) {
                fix(a, k);
                return solve(a);
            } else {
                a[k] &= ~lb;
                auto b = a;
                b[k] = lb;
                fix(b, k);
                if (solve(b)) {
                    swap(a, b);
                    return true;
                }
            }
        }
    }

    void solveSudoku(vector<vector<char>>& board) {
        for (int i1 = 0; i1 < 3; ++i1)
            for (int i2 = 0; i2 < 3; ++i2)
                for (int i3 = 0; i3 < 3; ++i3)
                    for (int i4 = 0; i4 < 3; ++i4) {
                        int v1 = i1 * 27 + i2 * 9 + i3 * 3 + i4;
                        for (int i5 = 0; i5 < 3; ++i5)
                            for (int i6 = 0; i6 < 3; ++i6) {
                                int v2 = i1 * 27 + i2 * 9 + i5 * 3 + i6;
                                if (v1 != v2) e[v1].emplace_back(v2);
                                int v3 = i5 * 27 + i6 * 9 + i3 * 3 + i4;
                                if (v1 != v3) e[v1].emplace_back(v3);
                                int v4 = i1 * 27 + i5 * 9 + i3 * 3 + i6;
                                if (v1 != v4) e[v1].emplace_back(v4);
                            }
                        sort(e[v1].begin(), e[v1].end());
                        auto p = unique(e[v1].begin(), e[v1].end());
                        e[v1].resize(p - e[v1].begin());
                    }
        vector<int> a(81, 1022);
        int k = -1;
        for (int i = 0; i < 9; ++i)
            for (int j = 0; j < 9; ++j) {
                ++k;
                if (board[i][j] == '.') continue;
                a[k] = 1 << (board[i][j] - '0');
                fix(a, k);
            }
        solve(a);
        k = -1;
        for (int i = 0; i < 9; ++i)
            for (int j = 0; j < 9; ++j) {
                ++k;
                if (board[i][j] != '.') continue;
                for (int p = 1; p <= 9; ++p) {
                    if (a[k] != 1 << p) continue;
                    board[i][j] = '0' + p;
                    break;
                }
            }
    }
};
