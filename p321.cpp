#include <algorithm>
#include <vector>
using namespace std;

class Solution {
    vector<int> maxNumber(const vector<int>& nums, int k) {
        if (k == 0) return {};
        deque<int> dq;
        for (int i = 0; i < nums.size() - (k - 1); ++i) {
            while (!dq.empty() && nums[dq.back()] < nums[i]) dq.pop_back();
            dq.emplace_back(i);
        }
        vector<int> ans;
        for (;;) {
            int dqH = dq.front();
            dq.pop_front();
            ans.emplace_back(nums[dqH]);
            if (!--k) break;
            int i = nums.size() - k;
            while (!dq.empty() && nums[dq.back()] < nums[i]) dq.pop_back();
            dq.emplace_back(i);
        }
        return move(ans);
    }

    vector<int> merge(const vector<int>& v1, const vector<int>& v2) {
        vector<int> ans;
        auto p1 = v1.begin(), p2 = v2.begin();
        while (p1 != v1.end() || p2 != v2.end()) {
            if (lexicographical_compare(p1, v1.end(), p2, v2.end()))
                ans.emplace_back(*p2++);
            else ans.emplace_back(*p1++);
        }
        return move(ans);
    }

public:
    vector<int> maxNumber(vector<int>& nums1, vector<int>& nums2, int k) {
        vector<int> ans;
        for (int i = 0; i <= k; ++i) {
            if (i > nums1.size() || k - i > nums2.size()) continue;
            auto v1 = maxNumber(nums1, i);
            auto v2 = maxNumber(nums2, k - i);
            auto v = merge(v1, v2);
            if (v > ans) ans = move(v);
        }
        return move(ans);
    }
};
