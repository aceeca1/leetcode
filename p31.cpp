#include <algorithm>
using namespace std;

class Solution {
public:
    void nextPermutation(vector<int>& nums) {
        int k = nums.size() - 1;
        while (k > 0 && nums[k - 1] >= nums[k]) --k;
        if (!k) {
            reverse(nums.begin(), nums.end());
            return;
        }
        int k1 = -1, k1v = 0x7fffffff;
        for (int i = k; i < nums.size(); ++i)
            if (nums[i] > nums[k - 1] && nums[i] <= k1v) {
                k1v = nums[i];
                k1 = i;
            }
        swap(nums[k - 1], nums[k1]);
        reverse(nums.begin() + k, nums.end());
    }
};
