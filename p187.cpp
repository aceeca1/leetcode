#include <unordered_map>
#include <vector>
#include <string>
using namespace std;

class Solution {
public:
    vector<string> findRepeatedDnaSequences(string s) {
        if (s.size() < 10) return {};
        unordered_map<int, int> a;
        int c = 0, m = 1;
        for (int i = 0; i < 10; ++i) {
            c = c * 0xdeadbeef + s[i];
            if (i) m *= 0xdeadbeef;
        }
        vector<string> ans;
        for (int i = 10; ; ++i) {
            if (++a[c] == 2) ans.emplace_back(s.substr(i - 10, 10));
            if (i >= s.size()) break;
            c = (c - m * s[i - 10]) * 0xdeadbeef + s[i];
        }
        return ans;
    }
};
