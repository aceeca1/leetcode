class Solution {
public:
    ListNode* reverseList(ListNode* head) {
        if (!head || !head->next) return head;
        auto h1 = reverseList(head->next);
        head->next->next = head;
        head->next = nullptr;
        return h1;
    }
};
