class Solution {
public:
    vector<int> searchRange(vector<int>& nums, int target) {
        if (nums.empty()) return vector<int>{-1, -1};
        int s = 0, t = nums.size() - 1;
        while (s < t) {
            int m = (s + t) >> 1;
            if (nums[m] < target) s = m + 1; else t = m;
        }
        int lb = s;
        if (nums[lb] != target) return vector<int>{-1, -1};
        s = 0, t = nums.size() - 1;
        while (s < t) {
            int m = (s + t + 1) >> 1;
            if (nums[m] > target) t = m - 1; else s = m;
        }
        int ub = s;
        return vector<int>{lb, ub};
    }
};
